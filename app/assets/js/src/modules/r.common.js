(function ($) {

    r.common = r.common || {};

    r.common = (function() {

        //change viewstate for groups of buttons
        //takes two arguments
        //1: [$]        the element in question
        //2: [string]   the general class for the views on that page (e.g. .js-types-views)
        function changeViewstate(element, views) {
            //variables
            var viewstate;
            //get viewstate from data-tag
            viewstate = element.attr('data-viewstate');
            //hide current view
            $(views).each(function() {
                $(this).removeClass('visible');
            });
            //show new view
            $(views + '[data-view=' + viewstate + ']').addClass('visible');
        }

        //show the user how that task went
        //takes two arguments
        //1: [string]   the element to use as status
        //2: [string]   the page to redirect to (optional)
        function showStatus(status, page) {
            //redirect to new page if specified
            if (typeof page !== 'undefined') {
                window.location.href = page;
            }
            //show status message
            else {
                //variables
                var $status;
                //set status
                $status = status;
                //make visible
                $status.addClass('active');
                //begin timeout
                setTimeout(function() {
                    $status.removeClass('active');
                }, 8000);
            }
        }

        //ensure the hidden input collection list has the right ids
        //takes one argument
        //1: [string]   the direct parent for the full list items (tr)
        function realignHiddenList(listName, parent) {



            //variables
            var $listItems, nameValue, number;

            //set list items
            if (parent) {
                $listItems = $(parent + ' input[name*="' + listName + '"]');
            }
            else {
                $listItems = $('input[name*="' + listName + '"]');
            }


            //iterate through list items and realign values ascending
            if ($listItems != undefined) {
                for (var i = 0; i < $listItems.length; i++) {
                    nameValue = $listItems[i].getAttribute('name');
                    number = nameValue.match(/\d+/g);
                    $listItems[i].setAttribute('name', nameValue.replace(number, i));
                }
            }
        }

        //adds content from input to a list (table)
        //takes two arguments
        //1: [string]   the input element
        //2: [string]   the direct parent for the list items
        function addToList(input, list, listName, inputName) {

            //helper: generate list row
            function generateListRow(value, listName, inputName) {
                return '<tr><td>' + value + '<input name="' + listName + '[0].' + inputName + '" type="hidden" value="' + value + '"></td><td><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
            }

            //variables
            var $input, value, listRow, $listRowWrapper;

            //set input
            $input = $(input);

            //get value to add
            value = $input.val();

            //generate html for row
            listRow = generateListRow(value, listName, inputName);

            //set the wrapper
            $listRowWrapper = $(list).find('tbody');

            //add the list row to the wrapper if value is not empty
            if (!(value === '')) {
                $listRowWrapper.append(listRow);
            }

            //realign hidden list
            r.common.realignHiddenList(listName);

            //clear the input
            $input.val('');
        }

        //removes list item from list (event)
        //takes one argument
        //1: [$]        the remove button on the list item in question
        //2: [string]   the direct parent for the list items
        function removeFromList(removeBtn) {

            //variables
            var $listItem;

            //set the listitem
            $listItem = removeBtn.closest('tr');

            //remove the list item
            $listItem.remove();
        }

        function menuCtrl() {

            function getParentPage(string) {
                var shortPath;
                var subPage;
                var noHttp = string.replace('http://', '');
                var slashIndex = noHttp.indexOf('/');
                var longPath = noHttp.substring(slashIndex + 1);
                slashIndex = longPath.indexOf('/');
                if (slashIndex !== -1) {
                    shortPath = longPath.substring(0, slashIndex);
                    subPage = longPath.substring(slashIndex + 1, longPath.length);
                }
                else {
                    shortPath = longPath;
                    subPage = 'root';
                }
                return [shortPath, subPage];
            }

            var url = window.location.href;
            var ctrlClass = getParentPage(url);

            $('body').addClass(ctrlClass[0]).addClass(ctrlClass[1]);
        }

        function init() {
            menuCtrl();
        }

        return {
            changeViewstate: changeViewstate,
            showStatus: showStatus,
            realignHiddenList: realignHiddenList,
            addToList: addToList,
            removeFromList: removeFromList,
            init: init
        };
    })();



    r.common.events = (function() {

        //the events common to the solution
        function init() {
            //closes (hides) the current status (event)
            $(document).on('click', '.js-alert', function(e) {
                e.preventDefault();
                $(this).parent().removeClass('active');
            });
        }

        return {
            init: init
        };
    })();

})(jQuery);
(function ($) {

    r.types = (function() {

        function changeViewstate() {
            $(document).on('click', '.js-types-viewstate button', function(e) {
                e.preventDefault();
                r.common.changeViewstate($(this), '.js-types-view');
            });
        }

        function createType() {
            $(document).on('click', '.js-types-create', function(e) {
                e.preventDefault();

                var type, name, url;

                type = $('.js-types-create-type').val();
                name = $('.js-types-create-name').val();
                url = $(this).attr('href');

                $.post(url, { name: name, type: type }, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        $('.js-types-create-name').val('');
                        $('.js-types-create-name').focus();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
            //bind enter key
            $(document).on('keypress', '.js-types-create-name', function(e) {
                if (e.keyCode === 13) {
                $('.js-types-create').click();
                }
            });
        }

        function editType() {
            $(document).on('click', '.js-types-edit', function(e) {
                e.preventDefault();
                var $btn, $listItem;
                $btn = $(this);
                $listItem = $(this).closest('tr');
                //enable edit in textbox
                $listItem.find('input[name=name]').removeAttr('disabled');
                //hide current button
                $btn.removeClass('visible');
                //show other options
                $listItem.find('.js-types-delete').addClass('visible');
                $listItem.find('.js-types-update').addClass('visible');
            });


        }

        function updateType() {
            $(document).on('click', '.js-types-update', function(e) {
                e.preventDefault();

                var $btn, $listItem, name, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                name = $listItem.find('input[name=name]').val();
                url = $btn.attr('href') + '&name=' + name;

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //add success class
                        $listItem.addClass('success');
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        function deleteType() {
            $('.js-types-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Vær opmærksom på at hvis du sletter denne type, vil den blive fjernet fra alle steder den bruges.<br>Er du sikker på at du vil slette typen?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet typen!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //disable edit in textbox
                            $listItem.find('input[name=name]').attr('disabled', 'disabled');
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-types-delete').remove();
                            $listItem.find('.js-types-update').remove();
                            $listItem.find('.js-types-edit').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        var init = function() {
            changeViewstate();
            createType();
            editType();
            updateType();
            deleteType();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);
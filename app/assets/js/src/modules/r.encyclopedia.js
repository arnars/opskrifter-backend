(function ($) {

    r.encyclopedia = (function() {

        //Ingredients
        function autocompleteIngredients() {
            $('.js-encyclopedia-ingredients-search').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-encyclopedia-ingredients-search');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-encyclopedia-ingredients-search');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToIngredientsList() {
            //special add for season
            $(document).on('click', '.js-encyclopedia-ingredients-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Ingredients[0].Id" type="hidden" value="' + id + '"><input name="Ingredients[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-ingredients-search');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-ingredients-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-ingredients-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-ingredients-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-ingredients-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-ingredients-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-ingredients-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-ingredients-search', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-encyclopedia-ingredients-add').click();
                    }
                }
            });
        }

        //Tags
        function autocompleteTags() {
            $('.js-encyclopedia-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-encyclopedia-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-encyclopedia-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-encyclopedia-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-tags-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-tags-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-encyclopedia-tag-add').click();
                    }
                }
            });
        }

        //ItemNumbers
        function formatItemnoInput() {
            $('.js-encyclopedia-itemno-input').formatter({
                'pattern': '{{999999}}'
            });
        }

        function addToItemnoList() {

            //helper: get name by number
            function getName(value) {
                var dfd = new $.Deferred();
                $.ajax({
                    type: 'POST',
                    url: '/item/getnamebyno',
                    data: { no: value },
                    success: function (data) {
                        dfd.resolve(data);
                    },
                    error: function (error) {
                        dfd.resolve('Intet navn');
                    }
                });
                return dfd.promise();
            }

            $('.js-encyclopedia-itemno-list td input').each(function() {
                var $input = $(this);
                var $inputhtml = $(this).prop('outerHTML');
                var itemNo = $(this).val();
                $.when(getName(itemNo)).then(function(data) {
                    $input.parent().html(data + ' (' + itemNo + ')' + $inputhtml);
                });
            });

            $(document).on('click', '.js-encyclopedia-itemno-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + ' (' + id + ')' + '<input name="ItemNumbers[0].ItemNo" type="hidden" value="' + id + '"></td><td><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-itemno-input');

                //set value
                nameVal = $name.val();

                $.when(getName(nameVal)).then(function(data) {
                    //generate html for row
                    listRow = generateListRow(nameVal, data);
                    //set the wrapper
                    $listRowWrapper = $('.js-encyclopedia-itemno-list').find('tbody');

                    //add the list row to the wrapper if value is not empty
                    if (!(nameVal === '')) {
                        $listRowWrapper.append(listRow);

                        //clear the input
                        $name.val('');
                        $name.attr('data-value-set', false);

                        //focus on name
                        $name.focus();
                    }
                    //realign hidden lists
                    r.common.realignHiddenList('.ItemNumbers', '.js-encyclopedia-itemno-list');
                    r.common.realignHiddenList('.ItemNo', '.js-encyclopedia-itemno-list');
                });
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-itemno-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('ItemNo');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-itemno-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $('.js-encyclopedia-itemno-add').click();
                    return false;
                }
            });
        }

        //Season
        function formatSeasonInput() {
            $('.js-encyclopedia-season-input').formatter({
                'pattern': '{{9999}}-{{9999}}'
            });
        }

        function addToSeasonList() {
            //special add for season
            $(document).on('click', '.js-encyclopedia-season-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(value, value1, value2) {
                    return '<tr><td>' + value + '</td><td><input name="Seasons[0].Id" type="hidden" value="00000000-0000-0000-0000-000000000000"><input name="Seasons[0].StartDate" type="hidden" value="' + value1 + '-2000 00:00:00"><input name="Seasons[0].EndDate" type="hidden" value="' + value2 + '-2000 00:00:00"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
                }

                //variables
                var $input, value, value1, value2, listRow, $listRowWrapper;

                //set input
                $input = $('.js-encyclopedia-season-input');

                //get value to add
                value = $input.val();
                value1 = value.substring(0,2) + '-' + value.substring(2,4);
                value2 = value.substring(5,7) + '-' + value.substring(7,9);

                //generate html for row
                listRow = generateListRow(value, value1, value2);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-season-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(value === '')) {
                    $listRowWrapper.append(listRow);
                }

                //clear the input
                $input.val('');

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.StartDate', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.EndDate', '.js-encyclopedia-season-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-season-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.StartDate', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.EndDate', '.js-encyclopedia-season-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-season-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $('.js-encyclopedia-season-add').click();
                    return false;
                }
            });
        }

        //The rest
        function imageInEncyclopedia() {
            $('.js-encyclopedia-fileupload').customFileInput({
                button_position : 'right',
                feedback_text : 'Ingen fil er valgt...',
                button_text : 'Browse',
                button_change_text : 'Vælg ny',
                callback: function() {
                    var fileInput = $('#file')[0];
                    if (fileInput.files && fileInput.files[0]) {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                           $('.js-encyclopedia-image').attr('src', e.target.result);
                        }
                        fileReader.readAsDataURL(fileInput.files[0]);
                    }
                }
            });
        }

        function deleteEncyclopediaEntry() {

            $('.js-encyclopedia-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette opslaget?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet opslaget!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-encyclopedia-delete').remove();
                            $listItem.find('.js-encyclopedia-edit').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function saveEncyclopediaEntry() {
            $(document).on('keydown', 'form.recipe', function(e) {
                if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                    e.preventDefault();
                    $('.js-encyclopedia-save').click();
                }
            });
            $('form.encyclopedia').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            $('#Id').val(data.Model.Id);
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }


        var init = function() {
            autocompleteIngredients();
            addToIngredientsList();
            autocompleteTags();
            addToTagsList();
            formatItemnoInput();
            addToItemnoList();
            formatSeasonInput();
            addToSeasonList();
            imageInEncyclopedia();
            deleteEncyclopediaEntry();
            saveEncyclopediaEntry();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);
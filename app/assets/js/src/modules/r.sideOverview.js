(function ($) {

    r.sideOverview = (function() {

        var thisMeal = {};
        var isSnapperPopulated = false;

        function getThisMeal() {

            var itemNo = $('#itemno :selected').val();
            var key = $('#mealboxkey :selected').val();
            var weekNo = $('input[name=mealboxweekno]').val();

            $.ajax({
                type: 'GET',
                url: '/mealbox/ExportAsJson',
                //data: { itemNo: '115037', key: 'KvikKassen', weekNo: '1614' },
                data: { itemNo: itemNo, key: key, weekNo: weekNo },
                dataType: 'json',
                success: function (data) {
                    thisMeal = data;
                    if (isSnapperPopulated === false) {
                        populateSnapper();
                    }
                    else {
                        populateContents();
                    }
                    $('.js-remember').hide();
                },
                error: function (error) {
                    $('.js-title-details').hide();
                    $('.js-list-bom').hide();
                    $('.js-list-days').hide();
                    $('.js-remember').show();
                }
            });
        }

        function populateContents() {
            function popTitle() {
                $('.js-title-details .js-title').html(thisMeal.Name);
                $('.js-title-details .js-week').html($('input[name=mealboxweekno]').val());
                $('.js-title-details .js-subtitle').html(thisMeal.Name2);
                $('.js-title-details .js-day').html($('select[name=MealBoxDay]').val());
                $('.js-title-details').show();
            }

            function popBom() {
                //get the list wrapper
                var $ul = $('.js-list-bom ul');
                //clear contents of wrapper
                $ul.find('li').remove();
                //declare bomArray based on thisMeal
                var bomArray = thisMeal.Bom;
                //run through array
                for (var i = 0; i < bomArray.length; i++) {
                    $ul.append('<li>' + bomArray[i].Quantity + ' ' + bomArray[i].UnitCode + ' ' + bomArray[i].Name + '</li>');
                };
                //show the list
                $('.js-list-bom').show();
            }
            function popDays() {
                //helper: render ingredients
                function renderIngredients(array) {
                    for (var j = 0; j < array.length; j++) {
                        return '<li>' + array[j].Amount + ' ' + array[j].Name + '</li>';
                    };
                }
                //get the list wrapper
                var $div = $('.js-list-days');
                //clear contents of lists
                $div.find('li').remove();
                //declare mealboxArray based on thisMeal
                var mealboxArray = thisMeal.MealBox;
                //run through mealboxArray
                for (var i = 0; i < mealboxArray.length; i++) {
                    //define the ingredientsArray in the current mealbox
                    var ingredientsArray = mealboxArray[i].Ingredients;
                    //find the current ul wrapper
                    $ulCurrent = $('ul[data-day=' + i + ']');
                    //fill the current ul based on ingredients array
                    for (var j = 0; j < ingredientsArray.length; j++) {
                        $ulCurrent.append('<li>' + ingredientsArray[j].Amount + ' ' + ingredientsArray[j].Name + '</li>');
                    };
                };
            }

            function init() {
                popTitle();
                popBom();
                popDays();
            }
            init();

        }

        function populateSnapper() {
            function popTitle() {
                $('.js-title-details .js-title').html(thisMeal.Name);
                $('.js-title-details .js-week').html($('input[name=mealboxweekno]').val());
                $('.js-title-details .js-subtitle').html(thisMeal.Name2);
                $('.js-title-details .js-day').html($('select[name=MealBoxDay]').val());
                $('.js-title-details').show();
            }

            function popBom() {
                //get the list wrapper
                var $ul = $('.js-list-bom ul');
                //clear contents of wrapper
                $ul.find('li').remove();
                //declare bomArray based on thisMeal
                var bomArray = thisMeal.Bom;
                //run through array
                for (var i = 0; i < bomArray.length; i++) {
                    $ul.append('<li>' + bomArray[i].Quantity + ' ' + bomArray[i].UnitCode + ' ' + bomArray[i].Name + '</li>');
                };
                //show the list
                $('.js-list-bom').show();
            }

            function popDays() {
                //helper: render ingredients
                function renderIngredients(array) {
                    for (var j = 0; j < array.length; j++) {
                        return '<li>' + array[j].Amount + ' ' + array[j].Name + '</li>';
                    };
                }
                //get the list wrapper
                var $div = $('.js-list-days');
                //clear contents of wrapper
                $div.find('> div').not('h3').remove();
                //declare mealboxArray based on thisMeal
                var mealboxArray = thisMeal.MealBox;
                //run through mealboxArray
                for (var i = 0; i < mealboxArray.length; i++) {
                    //append the main structure
                    $div.append('<div class="day"><a href="#">' + mealboxArray[i].MealBoxDay + '. ' + mealboxArray[i].Name + '</a><ul data-day="' + i + '"></ul></div>');
                    //define the ingredientsArray in the current mealbox
                    var ingredientsArray = mealboxArray[i].Ingredients;
                    //find the current ul wrapper
                    $ulCurrent = $('ul[data-day=' + i + ']');
                    //fill the current ul based on ingredients array
                    for (var j = 0; j < ingredientsArray.length; j++) {
                        $ulCurrent.append('<li>' + ingredientsArray[j].Amount + ' ' + ingredientsArray[j].Name + '</li>');
                    };
                };
                //show the list
                $('.js-list-days').show();
            }



            function init() {
                popTitle();
                popBom();
                popDays();
                isSnapperPopulated = true;
            }
            init();
        }

        function toggleOverview(element) {
            if (element.prop('checked')) {
                $('.js-overview-toggle').addClass('visible');
            }
            else {
                $('.js-overview-toggle').removeClass('visible');
            }
        }

        function toggleSnapper() {
            if (snapper.state().state === 'left' ) {
                snapper.close();
            }
            else {
                snapper.open('left');
                getThisMeal();
            }
        }

        function sideOverviewEvents() {
            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleOverview($(this));
            });

            $(document).on('click', '.js-overview-toggle', function(e) {
                e.preventDefault();
                toggleSnapper();
            });

            $(document).on('keydown', 'html', function(e) {
                if (e.keyCode === 81 && e.ctrlKey) {
                    if ($('.js-recipe-mealbox-toggle').prop('checked')) {
                        toggleSnapper();
                    }
                }
            });

            $(document).on('click', '.ingredient-list-days .day a', function(e) {
                e.preventDefault();
                $(this).next('ul').toggle();
            });


            $(document).on('click', '.js-update-list', function(e) {
                e.preventDefault();
                $.when( getThisMeal() ).done(function() {
                    populateSnapper();
                });
            });

        }

        function snapper() {
            snapper = new Snap({
                element: document.getElementById('snap'),
                tapToClose: false,
                touchToDrag: false
            });
        }

        var init = function() {
            snapper();
            sideOverviewEvents();
            toggleOverview($('.js-recipe-mealbox-toggle'));

        };

        return {
            init: init,
            thisMeal: thisMeal
        };

    })();

})(jQuery);
(function ($) {

    r.userimages = (function() {

        function approveImage() {
            $(document).on('click', '.js-userimages-approve', function(e) {
                e.preventDefault();

                var $btn, $listItem, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                url = $btn.attr('href');

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //add success class
                        $listItem.addClass('success');
                        $btn.remove();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        function deleteImage() {
            $(document).on('click', '.js-userimages-delete', function(e) {
                e.preventDefault();

                var $btn, $listItem, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                url = $btn.attr('href');

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //remove row
                        $listItem.remove();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        var init = function() {
            approveImage();
            deleteImage();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);
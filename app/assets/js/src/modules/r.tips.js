(function ($) {

    r.tips = (function() {

        function deleteTip() {

            $('.js-tip-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette tippet?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet tippet!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-recipe-delete').remove();
                            $listItem.find('.js-recipe-edit').remove();
                            $listItem.find('.js-recipe-duplicate').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function saveTip() {
            $('form.tip').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            $('#Id').val(data.Model.Id);
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }

        function addToTipsList() {
            //special add for season
            $(document).on('click', '.js-tip-week-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(weekYear, MealBoxKey) {
                    return '<tr><td>' + weekYear + '</td><td>' + MealBoxKey + '</td><td><input name="Tags[0].WeekYear" type="hidden" value="' + weekYear + '"><input name="Tags[0].MealBoxKey" type="hidden" value="' + MealBoxKey + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $weekYear, $MealBoxKey, $listRowWrapper;

                //set input
                $weekYear = $('.js-tip-week-input');
                $MealBoxKey = $('#mealboxkey');

                //set value
                weekYearVal = $weekYear.val();
                MealBoxKeyVal = $MealBoxKey.val();

                //generate html for row
                listRow = generateListRow(weekYearVal, MealBoxKeyVal);

                //set the wrapper
                $listRowWrapper = $('.js-tip-week-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(weekYearVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $weekYear.val('');
                    $MealBoxKey.val('');

                    //focus on name
                    $weekYear.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.WeekYear');
                r.common.realignHiddenList('.MealBoxKey');
            });

            //removes list items from list
            $(document).on('click', '.js-tip-week-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.WeekYear');
                r.common.realignHiddenList('.MealBoxKey');
            });
        }

        //Tags
        function autocompleteTags() {
            $('.js-tip-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-tip-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-tip-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-tip-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-tip-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-tip-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-tip-tags-list');
                r.common.realignHiddenList('.Name', '.js-tip-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-tip-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-tip-tags-list');
                r.common.realignHiddenList('.Name', '.js-tip-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-tip-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-tip-tag-add').click();
                    }
                }
            });
        }


        var init = function() {
            deleteTip();
            saveTip();
            addToTipsList();
            addToTagsList();
            autocompleteTags();
        };

        return {
            init: init
        };

    })();

})(jQuery);
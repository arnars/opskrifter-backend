var variantUpdate, variantUrl;

(function ($) {

    r.recipe = (function() {

        //Variants
        function updateAllVariants() {
            $('.js-update-variants').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil opdatere alle varianter?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - opdatér!',
                confirmCallback: function (target) {
                    variantUpdate = true;
                    variantUrl = $(target).attr('href');
                    $('.js-recipe-save').click();
                }
            });
        }

        function UpdateVariants(url) {
            $.ajax({
                url: url,
                type: 'POST',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        $('#Id').val(data.Model.Id);
                        $('#UrlName').val(data.Model.UrlName).trigger('change');
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                    $('html, body').animate({
                        scrollTop: 0
                    }, 500);
                },
                error: function () {
                    r.common.showStatus($('.js-alert-error'));
                }
            });
        }

        //Ingredients
        function autocompleteIngredients() {
            $('.js-recipe-ingredients-search').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            //add show all option 666
                            result[10] = {
                                Id: '666',
                                ItemType: 0,
                                Name: 'VIS ALLE >',
                                TypeDescription: 'Ingredienser'
                            };
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    if (ui.item.id === "666") {
                        $('.js-recipe-ingredients-showall').click();
                    }
                    else {
                        $list = $('.js-recipe-ingredients-search');
                        $list.attr('data-id', ui.item.id);
                        $list.attr('data-value-set', true);
                    }
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-search');
                        $list.val('');
                        $list.focus();
                    }
                }
            });

            //in edit page
            $('.js-recipe-ingredients-showall').confirmModal({
                confirmTitle: 'Find Ingrediens',
                confirmMessage: '<ul class="ajaxlist"></ul>',
                showButtons: false,
                confirmPreOpen: function(target, modal) {
                    //generer indhold til ajaxlist baseret på val
                    searchTerm = $('.js-recipe-ingredients-search').val();
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/listall',
                        data: 'term=' + searchTerm,
                        success: function(result) {
                            $list = $('.ajaxlist');
                            $.map(result, function(item) {
                                $list.append('<li><a class="js-pick-ingredient" href="#" data-ingredient="' + item.Name + '" data-id="' + item.Id + '">' + item.Name + '</a></li>');
                            });
                            $(document).on('click', '.js-pick-ingredient', function(e) {
                                e.preventDefault();
                                ingredient = $(this).attr('data-ingredient');
                                id = $(this).attr('data-id');
                                $list = $('.js-recipe-ingredients-search');
                                $list.val(ingredient);
                                $list.attr('data-id', id);
                                $list.attr('data-value-set', true);
                                modal.modal('hide');
                            });
                        },
                        error: function (error) {
                        }
                    });
                },
                confirmCallback: function (target, modal) {
                }

            });
        }

        function autocompleteUnits() {
            $('.js-recipe-ingredients-unit').attr('data-value-set', 0);
            $(document).on('focus', '.js-recipe-ingredients-unit', function(e) {
                $(this).attr('data-value-set', false);
            });
            $(document).on('blur', '.js-recipe-ingredients-unit', function(e) {
                 if ($(this).attr('data-value-set') !== 'true' && $(this).attr('data-value-set') !== '0') {
                    $(this).attr('data-value-set', 0);
                }
            });
            $('.js-recipe-ingredients-unit').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/type/autocompleteunit',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item,
                                    label: item,
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-ingredients-unit');
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-unit');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToIngredientsList() {
            //special add for season
            $(document).on('click', '.js-recipe-ingredients-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name, amount, unit, note) {
                    return '<tr class="ingredient"><td colspan="3"><input name="Ingredients[0].RecipeIngredientId" type="hidden" value="00000000-0000-0000-0000-000000000000"><input name="Ingredients[0].Id" type="hidden" value="' + id + '"><input name="Ingredients[0].Name" type="hidden" value="' + name + '"><input name="Ingredients[0].Amount" type="hidden" value="' + amount + '"><input name="Ingredients[0].Unit" type="hidden" value="' + unit + '"><input name="Ingredients[0].Note" type="hidden" value="' + note + '"><input name="Ingredients[0].Excluded" type="hidden" value="False"><span class="amount">' + amount + '</span> <span class="unit">' + unit + '</span> <span class="ingredient">' + name + '</span><span class="note">' + note + '</span></td><td><a class="btn center pull-right btn-info btn-mini js-ingredient-edit">?</a><a class="btn center pull-right btn-info btn-mini js-delete" style="display: none;">x</a></td></tr>'
                }

                //variables
                var $id, $name, $amount, $unit, $note, $listRowWrapper;

                //set input
                $name = $('.js-recipe-ingredients-search');
                $amount = $('.js-recipe-ingredients-amount');
                $unit = $('.js-recipe-ingredients-unit');
                $note = $('.js-recipe-ingredients-note');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();
                amountVal = $amount.val();
                unitVal = $unit.val();
                noteVal = $note.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal, amountVal, unitVal, noteVal);

                //set the wrapper
                $listRowWrapper = $('.js-recipe-ingredients-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $amount.val('');
                    $unit.val('');
                    $note.val('');
                    $name.attr('data-value-set', false);
                    $unit.attr('data-value-set', 0);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id');
                r.common.realignHiddenList('.Name');
                r.common.realignHiddenList('.Unit');
                r.common.realignHiddenList('.Amount');
                r.common.realignHiddenList('.Note');
                r.common.realignHiddenList('.RecipeIngredientId');
                r.common.realignHiddenList('.Excluded');

                editIngredient();
            });

            //removes list items from list
            $(document).on('click', '.js-recipe-ingredients-list .js-delete', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id');
                r.common.realignHiddenList('.Name');
                r.common.realignHiddenList('.Unit');
                r.common.realignHiddenList('.Amount');
                r.common.realignHiddenList('.Note');
                r.common.realignHiddenList('.RecipeIngredientId');
                r.common.realignHiddenList('.Excluded');
            });

            //bind enter key
            $(document).on('keypress', '.js-recipe-ingredients-search, .js-recipe-ingredients-amount, .js-recipe-ingredients-unit, .js-recipe-ingredients-note', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    //check is value = set on ingredients
                    if ($('.js-recipe-ingredients-search').attr('data-value-set') === 'true') {
                        //check if value set or empty at units
                        if ($('.js-recipe-ingredients-unit').attr('data-value-set') === 'true' || $('.js-recipe-ingredients-unit').attr('data-value-set') === '0') {
                            $('.js-recipe-ingredients-add').click();
                        }
                    }
                }
            });
        }

        function sortIngredientList() {
            $('.js-recipe-ingredients-list tbody').sortable({
                onDrop: function (item, placeholder, container) {
                    //defaults
                    item.removeClass('dragged').removeAttr('style');
                    $('body').removeClass('dragging');
                    //realign hidden lists
                    r.common.realignHiddenList('.Id');
                    r.common.realignHiddenList('.Name');
                    r.common.realignHiddenList('.Unit');
                    r.common.realignHiddenList('.Amount');
                    r.common.realignHiddenList('.Note');
                    r.common.realignHiddenList('.RecipeIngredientId');
                    r.common.realignHiddenList('.Excluded');
                }
            })
        }

        function autocompleteUnitsEdit() {
            $('.js-recipe-ingredients-unit-edit').attr('data-value-set', 0);
            $(document).on('focus', '.js-recipe-ingredients-unit-edit', function(e) {
                $(this).attr('data-value-set', false);
            });
            $(document).on('blur', '.js-recipe-ingredients-unit-edit', function(e) {
                 if ($(this).attr('data-value-set') !== 'true' && $(this).attr('data-value-set') !== '0') {
                    $(this).attr('data-value-set', 0);
                }
            });
            $('.js-recipe-ingredients-unit-edit').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/type/autocompleteunit',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item,
                                    label: item,
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-ingredients-unit-edit');
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-unit-edit');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }


        function editIngredient() {

            //unbind previous button
            $('.js-ingredient-edit').unbind();

            var msg, $row, name, amount, unit, note, excluded;

            msg += '<h4>Ingrediens: <span class="miName">' + name + '</span></h4>';
            msg += '<div class="row">';
            msg += '<div class="span5"><label>Mængde</label><input placeholder="Mængde" type="text" class="miAmount input-block-level js-recipe-ingredients-amount" tabindex="1"></div>';
            msg += '<div class="span5"><label>Enhed <small>(<a href="/type" target="_blank">Mangler enheden?</a>)</small></label><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input placeholder="Enhed" type="text" class="input-block-level js-recipe-ingredients-unit ui-autocomplete-input-edit miUnit" tabindex="2" autocomplete="off"></div>';
            msg += '<div class="span5"><label>Ingrediensnote (max 50 tegn)</label><input placeholder="Note" type="text" class="input-block-level js-recipe-ingredients-note miNote" tabindex="3"></div>';
            msg += '</div>';
            msg += '<div class="row">';
            msg += '<div class="span5"><label class="checkbox">Tjek-at-du-selv-har?<input class="miExcluded" id="tjekatduselvhar" name="tjekatduselvhar" tabindex="310" type="checkbox" value="true"><input name="tjekatduselvhar" type="hidden" value="false"></label></div>';
            msg += '</div>';
            msg += '<div class="row">';
            msg += '<div class="span5"><hr /><a class="btn center btn-danger js-ingredient-delete">Slet ingrediens</a></div>';
            msg += '</div>';

            //in edit page
            $('.js-ingredient-edit').confirmModal({
                confirmTitle: 'Redigér Ingrediens',
                confirmMessage: msg,
                confirmStyle: 'primary',
                confirmOk: 'Opdater',
                confirmPreOpen: function(target, modal) {
                    modalId = modal.attr('id');

                    $row = $(target).closest('tr');
                    name = $row.find('input[name*="Name"]').val();
                    amount = $row.find('input[name*="Amount"]').val();
                    unit = $row.find('input[name*="Unit"]').val();
                    note = $row.find('input[name*="Note"]').val();
                    excluded = $row.find('[id*="Excluded"]').val();


                    $('#' + modalId + ' .miName').text(name);
                    $('#' + modalId + ' .miAmount').val(amount);
                    $('#' + modalId + ' .miUnit').val(unit);
                    $('#' + modalId + ' .miNote').val(note);
                    if (parseInt(excluded, 10) == "true") {
                        $('.miExcluded').prop('checked', true)
                    }

                    $('.modal-body > p').remove();

                    autocompleteUnits();

                    //removes list items from list
                    $(document).on('click', '.js-ingredient-delete', function(e) {
                        e.preventDefault();
                        $row.find('.js-delete').click();
                        modal.modal('hide');
                    });
                },
                confirmCallback: function (target, modal) {
                    modalId = modal.attr('id');

                    newAmount = $('#' + modalId + ' .miAmount').val();
                    newUnit = $('#' + modalId + ' .miUnit').val();
                    newNote = $('#' + modalId + ' .miNote').val();
                    if ($('#' + modalId + ' .miExcluded').prop('checked') === true) {
                        newExcluded = "True";
                    }
                    else {
                        newExcluded = "False";
                    }

                    $row = $(target).closest('tr');

                    amountVal = $row.find('input[name*="Amount"]');
                    unitVal = $row.find('input[name*="Unit"]');
                    noteVal = $row.find('input[name*="Note"]');
                    excludedVal = $row.find('input[name*="Excluded"]');

                    amount = $row.find('span.amount');
                    unit = $row.find('span.unit');
                    note = $row.find('span.note');

                    amountVal.val(newAmount);
                    unitVal.val(newUnit);
                    noteVal.val(newNote);
                    excludedVal.val(newExcluded);

                    amount.html(newAmount);
                    unit.html(newUnit);
                    note.html(newNote);

                    if (newExcluded == "True") {
                        $row.addClass('error');
                        $row.insertAfter($row.parent().find('tr').last());
                        $('.js-recipe-ingredients-list tbody').sortable('refresh');

                        r.common.realignHiddenList('.Id');
                        r.common.realignHiddenList('.Name');
                        r.common.realignHiddenList('.Unit');
                        r.common.realignHiddenList('.Amount');
                        r.common.realignHiddenList('.Note');
                        r.common.realignHiddenList('.RecipeIngredientId');
                        r.common.realignHiddenList('.Excluded');
                    }
                    else {
                        $row.removeClass('error');
                    }

                }

            });

        }

        //Tags
        function autocompleteTags() {
            $('.js-recipe-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-recipe-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-recipe-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-recipe-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-recipe-tags-list');
                r.common.realignHiddenList('.Name', '.js-recipe-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-recipe-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-recipe-tags-list');
                r.common.realignHiddenList('.Name', '.js-recipe-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-recipe-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-recipe-tag-add').click();
                    }
                }
            });
        }

        //The rest
        function imageInRecipe() {
            $('.js-recipe-fileupload').customFileInput({
                button_position : 'right',
                feedback_text : 'Ingen fil er valgt...',
                button_text : 'Browse',
                button_change_text : 'Vælg ny',
                callback: function() {
                    var fileInput = $('#file')[0];

                    if (fileInput.files && fileInput.files[0]) {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            $('.js-recipe-image').attr('src', e.target.result);
                        }
                        if (fileInput.files[0].size > 2999999) {
                            $('.wFileUpload').addClass('large-file');
                        }
                        else {
                            fileReader.readAsDataURL(fileInput.files[0]);
                            $('.wFileUpload').removeClass('large-file');
                        }
                    }
                }
            });
        }

        function formatPersonInput() {
            $('.js-recipe-persons-input').formatter({
                'pattern': '{{9}}'
            });
        }

        function toggleMealbox() {
            $check = $('.js-recipe-mealbox-toggle');
            $wrapper = $('.js-recipe-mealbox-wrapper');

            function resetMealboxInputs() {
                $wrapper.find('input').val('');
                $wrapper.find('select').prop('selectedIndex', 0);
            }

            function toggleInput() {
                $mealboxweekno = $('input[name="mealboxweekno"]');
                $mealboxkey = $('select[name="mealboxkey"]');
                $mealboxvariant = $('select[name="mealboxvariant"]');
                $mealboxday = $('select[name="MealBoxDay"]');
                if ($check.prop('checked')) {
                    $wrapper.show();
                }
                else {
                    $wrapper.hide();
                    resetMealboxInputs();
                }
            }

            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleInput();
            });
            toggleInput();
        }

        function deleteRecipe() {

            $('.js-recipe-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette opskriften?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet opskriften!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-recipe-delete').remove();
                            $listItem.find('.js-recipe-edit').remove();
                            $listItem.find('.js-recipe-duplicate').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function duplicateRecipe() {

            //in edit page
            $('.js-duplicate-recipe').confirmModal({
                confirmTitle: 'Duplikér opskrift',
                confirmMessage: '<h4>Omdøb kopi</h4><p>Indtast evt. ny titel til opskriften.</p><p><input style="width: 98%;" type="text" id="confirmModalInput" placeholder="Ny titel"></p>',
                confirmStyle: 'primary',
                confirmOk: 'Duplikér!',
                confirmPreOpen: function() {
                    $('#confirmModalInput').val($('#Name').val()).focus();
                },
                confirmCallback: function (target) {
                    $titleInput = $('#confirmModalInput');
                    newTitle = $titleInput.val();
                    $titleInput.val('');

                    var $btn, url;

                    $btn = target;
                    url = $btn.attr('href');

                    $.post(url + '?name=' + newTitle, function (data) {
                        if (data.Result == true) {
                            var recipeUrl = data.Model.UrlName;
                            var urlToGo = '/recipe/edit/' + recipeUrl;
                            window.location.href = urlToGo;
                            r.common.showStatus($('.js-alert-success'));
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }

            });

        }

        function saveRecipe() {
            $(document).on('keydown', 'form.recipe', function(e) {
                if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                    e.preventDefault();
                    $('.js-recipe-save').click();
                }
            });
            if ($('#Description').length > 0) {
                $(document).on('keydown', '.froala-element', function(e) {
                    if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                        e.preventDefault();
                        $('.js-recipe-save').click();
                        $('.js-encyclopedia-save').click();
                    }
                });
            }
            $('form.recipe').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            if (variantUpdate !== true) {
                                r.common.showStatus($('.js-alert-success'));
                            }
                            $('#Id').val(data.Model.Id);
                            $('#UrlName').val(data.Model.UrlName).trigger('change');
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                        if (variantUpdate === true) {
                            UpdateVariants(variantUrl);
                            variantUpdate = false;
                        }
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }

        function getMealBoxVariants() {

            $(document).on('change', '#mealboxkey', function(e) {
                // e.preventDefault();

                var mealboxkey = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: '/mealbox/GetMealBoxVariants',
                    data: { key: mealboxkey },
                    success: function(data) {

                        $('#itemno').empty();

                        $.each(data, function (index, item) {
                            $('#itemno')
                                .append($("<option></option>")
                                .attr("value", item.DataValueKey)
                                .text(item.DataTextValue));
                        });

                    }
                });

            });
        }

        function countChars() {
            $charCount = $('.chars-left');
            function theCount(len) {
                var max = 1000;
                var char = max - len;
                if ($('.js-recipe-mealbox-toggle').prop('checked')) {
                    $charCount.show();
                    $charCount.text(char + ' tegn tilbage (vejledende)');
                }
                else {
                    $charCount.hide();
                }
            }
            if ($('#Description').length > 0) {
                $(document).on('keyup', '.froala-element', function(e) {
                    theCount($('.froala-element').text().length);
                });
                theCount($('.froala-element').text().length)
            }
        }


        function toggleSaveUpdate() {
            function toggleOverview(element) {
                if (element.prop('checked')) {
                    $('.js-update-variants').addClass('visible');
                }
                else {
                    $('.js-update-variants').removeClass('visible');
                }
            }
            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleOverview($(this));
            });

            toggleOverview($('.js-recipe-mealbox-toggle'));
        }

        function toggleDuplicate() {
            function isUrlName() {
                if ($('#UrlName').length) {
                    if ($('#UrlName').val().length > 0) {
                        return true
                    }
                    else {
                        return false
                    }
                }
            }

            function toggleButton() {
                if (isUrlName() === true) {
                    $('.js-duplicate-recipe').addClass('visible');
                }
                else {
                    $('.js-duplicate-recipe').removeClass('visible');
                }
            }
            $(document).on('change', '#UrlName', function() {
                toggleButton();
            });
            toggleButton();

        }

        function toggleDays() {
            var mealdata = {};
            var daysData = [];

            var itemNo = $('#itemno :selected').val();
            var key = $('#mealboxkey :selected').val();
            var weekNo = $('input[name=mealboxweekno]').val();

            function eliminateDays(days) {
                $selects = $('select[name="MealBoxDay"]');
                $.each(days, function(index, value) {
                    $selects.find('option:eq(' + value + ')').attr('disabled','disabled');
                });
            }

            $.ajax({
                type: 'GET',
                url: '/mealbox/ExportAsJson',
                data: { itemNo: itemNo, key: key, weekNo: weekNo },
                dataType: 'json',
                success: function (data) {
                    mealdata = data;
                    for (var i = 0; i < mealdata.MealBox.length; i++) {
                        daysData.push(mealdata.MealBox[i].MealBoxDay);
                    };
                    eliminateDays(daysData);
                },
                error: function (error) {

                }
            });
        }


        var init = function() {
            updateAllVariants();
            autocompleteIngredients();
            autocompleteUnits();
            autocompleteTags();
            addToTagsList();
            imageInRecipe();
            addToIngredientsList();
            editIngredient();
            formatPersonInput();
            toggleMealbox();
            sortIngredientList();
            deleteRecipe();
            duplicateRecipe();
            saveRecipe();
            getMealBoxVariants();
            countChars();
            toggleSaveUpdate();
            toggleDuplicate();
            toggleDays();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);
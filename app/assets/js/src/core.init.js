 (function ($) {

    //inits on document.ready
    r.config               = (function() {

        var configuration = function() {

            //Add full link functionality to touch platforms
            $(document).on('touchend', '.touch a', function() {
                $(this).mouseout();
            });

            $('#Description.no-image').editable({
                inlineMode: false,
                minHeight: 500,
                buttons: ['bold', 'italic', 'html'],
                plainPaste: true,
                imageUploadParam: 'file',
                imageUploadURL: '/editorimage/upload'
            });

            $('#Description.image').editable({
                inlineMode: false,
                minHeight: 500,
                buttons: ['bold', 'italic', 'insertImage', 'html'],
                plainPaste: true,
                imageUploadParam: 'file',
                imageUploadURL: '/editorimage/upload'
            });

            $('table').stupidtable();

            //Fast clicking for touch devices
            if (Modernizr.touch) {
               if (window.addEventListener) {
                    window.addEventListener('load', function() {
                        new FastClick(document.body);
                    }, false);
                }
            }
        };

        var init = function() {
            configuration();
            r.common.init();
            r.common.events.init();
            r.types.init();
            r.encyclopedia.init();
            r.recipe.init();
            r.userimages.init();
            r.sideOverview.init();
            r.tips.init();
        };

        return {
            init: init
        };
    })();


    //inits on ajax postback
    r.reInit               = (function() {

        var init = function() {

        };

        return {
            init: init
        };
    })();

    //---------------------------------------------------
    //    EXECUTE
    //---------------------------------------------------

    $(document).ready(function () {
        r.config.init();
    });


})(jQuery);



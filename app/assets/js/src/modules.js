(function ($) {

    r.common = r.common || {};

    r.common = (function() {

        //change viewstate for groups of buttons
        //takes two arguments
        //1: [$]        the element in question
        //2: [string]   the general class for the views on that page (e.g. .js-types-views)
        function changeViewstate(element, views) {
            //variables
            var viewstate;
            //get viewstate from data-tag
            viewstate = element.attr('data-viewstate');
            //hide current view
            $(views).each(function() {
                $(this).removeClass('visible');
            });
            //show new view
            $(views + '[data-view=' + viewstate + ']').addClass('visible');
        }

        //show the user how that task went
        //takes two arguments
        //1: [string]   the element to use as status
        //2: [string]   the page to redirect to (optional)
        function showStatus(status, page) {
            //redirect to new page if specified
            if (typeof page !== 'undefined') {
                window.location.href = page;
            }
            //show status message
            else {
                //variables
                var $status;
                //set status
                $status = status;
                //make visible
                $status.addClass('active');
                //begin timeout
                setTimeout(function() {
                    $status.removeClass('active');
                }, 8000);
            }
        }

        //ensure the hidden input collection list has the right ids
        //takes one argument
        //1: [string]   the direct parent for the full list items (tr)
        function realignHiddenList(listName, parent) {



            //variables
            var $listItems, nameValue, number;

            //set list items
            if (parent) {
                $listItems = $(parent + ' input[name*="' + listName + '"]');
            }
            else {
                $listItems = $('input[name*="' + listName + '"]');
            }


            //iterate through list items and realign values ascending
            if ($listItems != undefined) {
                for (var i = 0; i < $listItems.length; i++) {
                    nameValue = $listItems[i].getAttribute('name');
                    number = nameValue.match(/\d+/g);
                    $listItems[i].setAttribute('name', nameValue.replace(number, i));
                }
            }
        }

        //adds content from input to a list (table)
        //takes two arguments
        //1: [string]   the input element
        //2: [string]   the direct parent for the list items
        function addToList(input, list, listName, inputName) {

            //helper: generate list row
            function generateListRow(value, listName, inputName) {
                return '<tr><td>' + value + '<input name="' + listName + '[0].' + inputName + '" type="hidden" value="' + value + '"></td><td><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
            }

            //variables
            var $input, value, listRow, $listRowWrapper;

            //set input
            $input = $(input);

            //get value to add
            value = $input.val();

            //generate html for row
            listRow = generateListRow(value, listName, inputName);

            //set the wrapper
            $listRowWrapper = $(list).find('tbody');

            //add the list row to the wrapper if value is not empty
            if (!(value === '')) {
                $listRowWrapper.append(listRow);
            }

            //realign hidden list
            r.common.realignHiddenList(listName);

            //clear the input
            $input.val('');
        }

        //removes list item from list (event)
        //takes one argument
        //1: [$]        the remove button on the list item in question
        //2: [string]   the direct parent for the list items
        function removeFromList(removeBtn) {

            //variables
            var $listItem;

            //set the listitem
            $listItem = removeBtn.closest('tr');

            //remove the list item
            $listItem.remove();
        }

        function menuCtrl() {

            function getParentPage(string) {
                var shortPath;
                var subPage;
                var noHttp = string.replace('http://', '');
                var slashIndex = noHttp.indexOf('/');
                var longPath = noHttp.substring(slashIndex + 1);
                slashIndex = longPath.indexOf('/');
                if (slashIndex !== -1) {
                    shortPath = longPath.substring(0, slashIndex);
                    subPage = longPath.substring(slashIndex + 1, longPath.length);
                }
                else {
                    shortPath = longPath;
                    subPage = 'root';
                }
                return [shortPath, subPage];
            }

            var url = window.location.href;
            var ctrlClass = getParentPage(url);

            $('body').addClass(ctrlClass[0]).addClass(ctrlClass[1]);
        }

        function init() {
            menuCtrl();
        }

        return {
            changeViewstate: changeViewstate,
            showStatus: showStatus,
            realignHiddenList: realignHiddenList,
            addToList: addToList,
            removeFromList: removeFromList,
            init: init
        };
    })();



    r.common.events = (function() {

        //the events common to the solution
        function init() {
            //closes (hides) the current status (event)
            $(document).on('click', '.js-alert', function(e) {
                e.preventDefault();
                $(this).parent().removeClass('active');
            });
        }

        return {
            init: init
        };
    })();

})(jQuery);(function ($) {

    r.encyclopedia = (function() {

        //Ingredients
        function autocompleteIngredients() {
            $('.js-encyclopedia-ingredients-search').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-encyclopedia-ingredients-search');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-encyclopedia-ingredients-search');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToIngredientsList() {
            //special add for season
            $(document).on('click', '.js-encyclopedia-ingredients-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Ingredients[0].Id" type="hidden" value="' + id + '"><input name="Ingredients[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-ingredients-search');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-ingredients-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-ingredients-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-ingredients-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-ingredients-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-ingredients-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-ingredients-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-ingredients-search', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-encyclopedia-ingredients-add').click();
                    }
                }
            });
        }

        //Tags
        function autocompleteTags() {
            $('.js-encyclopedia-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-encyclopedia-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-encyclopedia-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-encyclopedia-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-tags-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-tags-list');
                r.common.realignHiddenList('.Name', '.js-encyclopedia-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-encyclopedia-tag-add').click();
                    }
                }
            });
        }

        //ItemNumbers
        function formatItemnoInput() {
            $('.js-encyclopedia-itemno-input').formatter({
                'pattern': '{{999999}}'
            });
        }

        function addToItemnoList() {

            //helper: get name by number
            function getName(value) {
                var dfd = new $.Deferred();
                $.ajax({
                    type: 'POST',
                    url: '/item/getnamebyno',
                    data: { no: value },
                    success: function (data) {
                        dfd.resolve(data);
                    },
                    error: function (error) {
                        dfd.resolve('Intet navn');
                    }
                });
                return dfd.promise();
            }

            $('.js-encyclopedia-itemno-list td input').each(function() {
                var $input = $(this);
                var $inputhtml = $(this).prop('outerHTML');
                var itemNo = $(this).val();
                $.when(getName(itemNo)).then(function(data) {
                    $input.parent().html(data + ' (' + itemNo + ')' + $inputhtml);
                });
            });

            $(document).on('click', '.js-encyclopedia-itemno-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + ' (' + id + ')' + '<input name="ItemNumbers[0].ItemNo" type="hidden" value="' + id + '"></td><td><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-encyclopedia-itemno-input');

                //set value
                nameVal = $name.val();

                $.when(getName(nameVal)).then(function(data) {
                    //generate html for row
                    listRow = generateListRow(nameVal, data);
                    //set the wrapper
                    $listRowWrapper = $('.js-encyclopedia-itemno-list').find('tbody');

                    //add the list row to the wrapper if value is not empty
                    if (!(nameVal === '')) {
                        $listRowWrapper.append(listRow);

                        //clear the input
                        $name.val('');
                        $name.attr('data-value-set', false);

                        //focus on name
                        $name.focus();
                    }
                    //realign hidden lists
                    r.common.realignHiddenList('.ItemNumbers', '.js-encyclopedia-itemno-list');
                    r.common.realignHiddenList('.ItemNo', '.js-encyclopedia-itemno-list');
                });
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-itemno-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('ItemNo');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-itemno-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $('.js-encyclopedia-itemno-add').click();
                    return false;
                }
            });
        }

        //Season
        function formatSeasonInput() {
            $('.js-encyclopedia-season-input').formatter({
                'pattern': '{{9999}}-{{9999}}'
            });
        }

        function addToSeasonList() {
            //special add for season
            $(document).on('click', '.js-encyclopedia-season-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(value, value1, value2) {
                    return '<tr><td>' + value + '</td><td><input name="Seasons[0].Id" type="hidden" value="00000000-0000-0000-0000-000000000000"><input name="Seasons[0].StartDate" type="hidden" value="' + value1 + '-2000 00:00:00"><input name="Seasons[0].EndDate" type="hidden" value="' + value2 + '-2000 00:00:00"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
                }

                //variables
                var $input, value, value1, value2, listRow, $listRowWrapper;

                //set input
                $input = $('.js-encyclopedia-season-input');

                //get value to add
                value = $input.val();
                value1 = value.substring(0,2) + '-' + value.substring(2,4);
                value2 = value.substring(5,7) + '-' + value.substring(7,9);

                //generate html for row
                listRow = generateListRow(value, value1, value2);

                //set the wrapper
                $listRowWrapper = $('.js-encyclopedia-season-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(value === '')) {
                    $listRowWrapper.append(listRow);
                }

                //clear the input
                $input.val('');

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.StartDate', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.EndDate', '.js-encyclopedia-season-list');
            });

            //removes list items from list
            $(document).on('click', '.js-encyclopedia-season-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.StartDate', '.js-encyclopedia-season-list');
                r.common.realignHiddenList('.EndDate', '.js-encyclopedia-season-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-encyclopedia-season-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $('.js-encyclopedia-season-add').click();
                    return false;
                }
            });
        }

        //The rest
        function imageInEncyclopedia() {
            $('.js-encyclopedia-fileupload').customFileInput({
                button_position : 'right',
                feedback_text : 'Ingen fil er valgt...',
                button_text : 'Browse',
                button_change_text : 'Vælg ny',
                callback: function() {
                    var fileInput = $('#file')[0];
                    if (fileInput.files && fileInput.files[0]) {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                           $('.js-encyclopedia-image').attr('src', e.target.result);
                        }
                        fileReader.readAsDataURL(fileInput.files[0]);
                    }
                }
            });
        }

        function deleteEncyclopediaEntry() {

            $('.js-encyclopedia-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette opslaget?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet opslaget!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-encyclopedia-delete').remove();
                            $listItem.find('.js-encyclopedia-edit').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function saveEncyclopediaEntry() {
            $(document).on('keydown', 'form.recipe', function(e) {
                if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                    e.preventDefault();
                    $('.js-encyclopedia-save').click();
                }
            });
            $('form.encyclopedia').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            $('#Id').val(data.Model.Id);
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }


        var init = function() {
            autocompleteIngredients();
            addToIngredientsList();
            autocompleteTags();
            addToTagsList();
            formatItemnoInput();
            addToItemnoList();
            formatSeasonInput();
            addToSeasonList();
            imageInEncyclopedia();
            deleteEncyclopediaEntry();
            saveEncyclopediaEntry();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);var variantUpdate, variantUrl;

(function ($) {

    r.recipe = (function() {

        //Variants
        function updateAllVariants() {
            $('.js-update-variants').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil opdatere alle varianter?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - opdatér!',
                confirmCallback: function (target) {
                    variantUpdate = true;
                    variantUrl = $(target).attr('href');
                    $('.js-recipe-save').click();
                }
            });
        }

        function UpdateVariants(url) {
            $.ajax({
                url: url,
                type: 'POST',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        $('#Id').val(data.Model.Id);
                        $('#UrlName').val(data.Model.UrlName).trigger('change');
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                    $('html, body').animate({
                        scrollTop: 0
                    }, 500);
                },
                error: function () {
                    r.common.showStatus($('.js-alert-error'));
                }
            });
        }

        //Ingredients
        function autocompleteIngredients() {
            $('.js-recipe-ingredients-search').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            //add show all option 666
                            result[10] = {
                                Id: '666',
                                ItemType: 0,
                                Name: 'VIS ALLE >',
                                TypeDescription: 'Ingredienser'
                            };
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    if (ui.item.id === "666") {
                        $('.js-recipe-ingredients-showall').click();
                    }
                    else {
                        $list = $('.js-recipe-ingredients-search');
                        $list.attr('data-id', ui.item.id);
                        $list.attr('data-value-set', true);
                    }
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-search');
                        $list.val('');
                        $list.focus();
                    }
                }
            });

            //in edit page
            $('.js-recipe-ingredients-showall').confirmModal({
                confirmTitle: 'Find Ingrediens',
                confirmMessage: '<ul class="ajaxlist"></ul>',
                showButtons: false,
                confirmPreOpen: function(target, modal) {
                    //generer indhold til ajaxlist baseret på val
                    searchTerm = $('.js-recipe-ingredients-search').val();
                    $.ajax({
                        type: 'POST',
                        url: '/ingredient/listall',
                        data: 'term=' + searchTerm,
                        success: function(result) {
                            $list = $('.ajaxlist');
                            $.map(result, function(item) {
                                $list.append('<li><a class="js-pick-ingredient" href="#" data-ingredient="' + item.Name + '" data-id="' + item.Id + '">' + item.Name + '</a></li>');
                            });
                            $(document).on('click', '.js-pick-ingredient', function(e) {
                                e.preventDefault();
                                ingredient = $(this).attr('data-ingredient');
                                id = $(this).attr('data-id');
                                $list = $('.js-recipe-ingredients-search');
                                $list.val(ingredient);
                                $list.attr('data-id', id);
                                $list.attr('data-value-set', true);
                                modal.modal('hide');
                            });
                        },
                        error: function (error) {
                        }
                    });
                },
                confirmCallback: function (target, modal) {
                }

            });
        }

        function autocompleteUnits() {
            $('.js-recipe-ingredients-unit').attr('data-value-set', 0);
            $(document).on('focus', '.js-recipe-ingredients-unit', function(e) {
                $(this).attr('data-value-set', false);
            });
            $(document).on('blur', '.js-recipe-ingredients-unit', function(e) {
                 if ($(this).attr('data-value-set') !== 'true' && $(this).attr('data-value-set') !== '0') {
                    $(this).attr('data-value-set', 0);
                }
            });
            $('.js-recipe-ingredients-unit').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/type/autocompleteunit',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item,
                                    label: item,
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-ingredients-unit');
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-unit');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToIngredientsList() {
            //special add for season
            $(document).on('click', '.js-recipe-ingredients-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name, amount, unit, note) {
                    return '<tr class="ingredient"><td colspan="3"><input name="Ingredients[0].RecipeIngredientId" type="hidden" value="00000000-0000-0000-0000-000000000000"><input name="Ingredients[0].Id" type="hidden" value="' + id + '"><input name="Ingredients[0].Name" type="hidden" value="' + name + '"><input name="Ingredients[0].Amount" type="hidden" value="' + amount + '"><input name="Ingredients[0].Unit" type="hidden" value="' + unit + '"><input name="Ingredients[0].Note" type="hidden" value="' + note + '"><input name="Ingredients[0].Excluded" type="hidden" value="False"><span class="amount">' + amount + '</span> <span class="unit">' + unit + '</span> <span class="ingredient">' + name + '</span><span class="note">' + note + '</span></td><td><a class="btn center pull-right btn-info btn-mini js-ingredient-edit">?</a><a class="btn center pull-right btn-info btn-mini js-delete" style="display: none;">x</a></td></tr>'
                }

                //variables
                var $id, $name, $amount, $unit, $note, $listRowWrapper;

                //set input
                $name = $('.js-recipe-ingredients-search');
                $amount = $('.js-recipe-ingredients-amount');
                $unit = $('.js-recipe-ingredients-unit');
                $note = $('.js-recipe-ingredients-note');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();
                amountVal = $amount.val();
                unitVal = $unit.val();
                noteVal = $note.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal, amountVal, unitVal, noteVal);

                //set the wrapper
                $listRowWrapper = $('.js-recipe-ingredients-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $amount.val('');
                    $unit.val('');
                    $note.val('');
                    $name.attr('data-value-set', false);
                    $unit.attr('data-value-set', 0);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id');
                r.common.realignHiddenList('.Name');
                r.common.realignHiddenList('.Unit');
                r.common.realignHiddenList('.Amount');
                r.common.realignHiddenList('.Note');
                r.common.realignHiddenList('.RecipeIngredientId');
                r.common.realignHiddenList('.Excluded');

                editIngredient();
            });

            //removes list items from list
            $(document).on('click', '.js-recipe-ingredients-list .js-delete', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id');
                r.common.realignHiddenList('.Name');
                r.common.realignHiddenList('.Unit');
                r.common.realignHiddenList('.Amount');
                r.common.realignHiddenList('.Note');
                r.common.realignHiddenList('.RecipeIngredientId');
                r.common.realignHiddenList('.Excluded');
            });

            //bind enter key
            $(document).on('keypress', '.js-recipe-ingredients-search, .js-recipe-ingredients-amount, .js-recipe-ingredients-unit, .js-recipe-ingredients-note', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    //check is value = set on ingredients
                    if ($('.js-recipe-ingredients-search').attr('data-value-set') === 'true') {
                        //check if value set or empty at units
                        if ($('.js-recipe-ingredients-unit').attr('data-value-set') === 'true' || $('.js-recipe-ingredients-unit').attr('data-value-set') === '0') {
                            $('.js-recipe-ingredients-add').click();
                        }
                    }
                }
            });
        }

        function sortIngredientList() {
            $('.js-recipe-ingredients-list tbody').sortable({
                onDrop: function (item, placeholder, container) {
                    //defaults
                    item.removeClass('dragged').removeAttr('style');
                    $('body').removeClass('dragging');
                    //realign hidden lists
                    r.common.realignHiddenList('.Id');
                    r.common.realignHiddenList('.Name');
                    r.common.realignHiddenList('.Unit');
                    r.common.realignHiddenList('.Amount');
                    r.common.realignHiddenList('.Note');
                    r.common.realignHiddenList('.RecipeIngredientId');
                    r.common.realignHiddenList('.Excluded');
                }
            })
        }

        function autocompleteUnitsEdit() {
            $('.js-recipe-ingredients-unit-edit').attr('data-value-set', 0);
            $(document).on('focus', '.js-recipe-ingredients-unit-edit', function(e) {
                $(this).attr('data-value-set', false);
            });
            $(document).on('blur', '.js-recipe-ingredients-unit-edit', function(e) {
                 if ($(this).attr('data-value-set') !== 'true' && $(this).attr('data-value-set') !== '0') {
                    $(this).attr('data-value-set', 0);
                }
            });
            $('.js-recipe-ingredients-unit-edit').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/type/autocompleteunit',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item,
                                    label: item,
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-ingredients-unit-edit');
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-ingredients-unit-edit');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }


        function editIngredient() {

            //unbind previous button
            $('.js-ingredient-edit').unbind();

            var msg, $row, name, amount, unit, note, excluded;

            msg += '<h4>Ingrediens: <span class="miName">' + name + '</span></h4>';
            msg += '<div class="row">';
            msg += '<div class="span5"><label>Mængde</label><input placeholder="Mængde" type="text" class="miAmount input-block-level js-recipe-ingredients-amount" tabindex="1"></div>';
            msg += '<div class="span5"><label>Enhed <small>(<a href="/type" target="_blank">Mangler enheden?</a>)</small></label><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input placeholder="Enhed" type="text" class="input-block-level js-recipe-ingredients-unit ui-autocomplete-input-edit miUnit" tabindex="2" autocomplete="off"></div>';
            msg += '<div class="span5"><label>Ingrediensnote (max 50 tegn)</label><input placeholder="Note" type="text" class="input-block-level js-recipe-ingredients-note miNote" tabindex="3"></div>';
            msg += '</div>';
            msg += '<div class="row">';
            msg += '<div class="span5"><label class="checkbox">Tjek-at-du-selv-har?<input class="miExcluded" id="tjekatduselvhar" name="tjekatduselvhar" tabindex="310" type="checkbox" value="true"><input name="tjekatduselvhar" type="hidden" value="false"></label></div>';
            msg += '</div>';
            msg += '<div class="row">';
            msg += '<div class="span5"><hr /><a class="btn center btn-danger js-ingredient-delete">Slet ingrediens</a></div>';
            msg += '</div>';

            //in edit page
            $('.js-ingredient-edit').confirmModal({
                confirmTitle: 'Redigér Ingrediens',
                confirmMessage: msg,
                confirmStyle: 'primary',
                confirmOk: 'Opdater',
                confirmPreOpen: function(target, modal) {
                    modalId = modal.attr('id');

                    $row = $(target).closest('tr');
                    name = $row.find('input[name*="Name"]').val();
                    amount = $row.find('input[name*="Amount"]').val();
                    unit = $row.find('input[name*="Unit"]').val();
                    note = $row.find('input[name*="Note"]').val();
                    excluded = $row.find('[id*="Excluded"]').val();


                    $('#' + modalId + ' .miName').text(name);
                    $('#' + modalId + ' .miAmount').val(amount);
                    $('#' + modalId + ' .miUnit').val(unit);
                    $('#' + modalId + ' .miNote').val(note);
                    if (parseInt(excluded, 10) == "true") {
                        $('.miExcluded').prop('checked', true)
                    }

                    $('.modal-body > p').remove();

                    autocompleteUnits();

                    //removes list items from list
                    $(document).on('click', '.js-ingredient-delete', function(e) {
                        e.preventDefault();
                        $row.find('.js-delete').click();
                        modal.modal('hide');
                    });
                },
                confirmCallback: function (target, modal) {
                    modalId = modal.attr('id');

                    newAmount = $('#' + modalId + ' .miAmount').val();
                    newUnit = $('#' + modalId + ' .miUnit').val();
                    newNote = $('#' + modalId + ' .miNote').val();
                    if ($('#' + modalId + ' .miExcluded').prop('checked') === true) {
                        newExcluded = "True";
                    }
                    else {
                        newExcluded = "False";
                    }

                    $row = $(target).closest('tr');

                    amountVal = $row.find('input[name*="Amount"]');
                    unitVal = $row.find('input[name*="Unit"]');
                    noteVal = $row.find('input[name*="Note"]');
                    excludedVal = $row.find('input[name*="Excluded"]');

                    amount = $row.find('span.amount');
                    unit = $row.find('span.unit');
                    note = $row.find('span.note');

                    amountVal.val(newAmount);
                    unitVal.val(newUnit);
                    noteVal.val(newNote);
                    excludedVal.val(newExcluded);

                    amount.html(newAmount);
                    unit.html(newUnit);
                    note.html(newNote);

                    if (newExcluded == "True") {
                        $row.addClass('error');
                        $row.insertAfter($row.parent().find('tr').last());
                        $('.js-recipe-ingredients-list tbody').sortable('refresh');

                        r.common.realignHiddenList('.Id');
                        r.common.realignHiddenList('.Name');
                        r.common.realignHiddenList('.Unit');
                        r.common.realignHiddenList('.Amount');
                        r.common.realignHiddenList('.Note');
                        r.common.realignHiddenList('.RecipeIngredientId');
                        r.common.realignHiddenList('.Excluded');
                    }
                    else {
                        $row.removeClass('error');
                    }

                }

            });

        }

        //Tags
        function autocompleteTags() {
            $('.js-recipe-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-recipe-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-recipe-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-recipe-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>';
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-recipe-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-recipe-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-recipe-tags-list');
                r.common.realignHiddenList('.Name', '.js-recipe-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-recipe-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-recipe-tags-list');
                r.common.realignHiddenList('.Name', '.js-recipe-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-recipe-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-recipe-tag-add').click();
                    }
                }
            });
        }

        //The rest
        function imageInRecipe() {
            $('.js-recipe-fileupload').customFileInput({
                button_position : 'right',
                feedback_text : 'Ingen fil er valgt...',
                button_text : 'Browse',
                button_change_text : 'Vælg ny',
                callback: function() {
                    var fileInput = $('#file')[0];

                    if (fileInput.files && fileInput.files[0]) {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            $('.js-recipe-image').attr('src', e.target.result);
                        }
                        if (fileInput.files[0].size > 2999999) {
                            $('.wFileUpload').addClass('large-file');
                        }
                        else {
                            fileReader.readAsDataURL(fileInput.files[0]);
                            $('.wFileUpload').removeClass('large-file');
                        }
                    }
                }
            });
        }

        function formatPersonInput() {
            $('.js-recipe-persons-input').formatter({
                'pattern': '{{9}}'
            });
        }

        function toggleMealbox() {
            $check = $('.js-recipe-mealbox-toggle');
            $wrapper = $('.js-recipe-mealbox-wrapper');

            function resetMealboxInputs() {
                $wrapper.find('input').val('');
                $wrapper.find('select').prop('selectedIndex', 0);
            }

            function toggleInput() {
                $mealboxweekno = $('input[name="mealboxweekno"]');
                $mealboxkey = $('select[name="mealboxkey"]');
                $mealboxvariant = $('select[name="mealboxvariant"]');
                $mealboxday = $('select[name="MealBoxDay"]');
                if ($check.prop('checked')) {
                    $wrapper.show();
                }
                else {
                    $wrapper.hide();
                    resetMealboxInputs();
                }
            }

            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleInput();
            });
            toggleInput();
        }

        function deleteRecipe() {

            $('.js-recipe-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette opskriften?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet opskriften!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-recipe-delete').remove();
                            $listItem.find('.js-recipe-edit').remove();
                            $listItem.find('.js-recipe-duplicate').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function duplicateRecipe() {

            //in edit page
            $('.js-duplicate-recipe').confirmModal({
                confirmTitle: 'Duplikér opskrift',
                confirmMessage: '<h4>Omdøb kopi</h4><p>Indtast evt. ny titel til opskriften.</p><p><input style="width: 98%;" type="text" id="confirmModalInput" placeholder="Ny titel"></p>',
                confirmStyle: 'primary',
                confirmOk: 'Duplikér!',
                confirmPreOpen: function() {
                    $('#confirmModalInput').val($('#Name').val()).focus();
                },
                confirmCallback: function (target) {
                    $titleInput = $('#confirmModalInput');
                    newTitle = $titleInput.val();
                    $titleInput.val('');

                    var $btn, url;

                    $btn = target;
                    url = $btn.attr('href');

                    $.post(url + '?name=' + newTitle, function (data) {
                        if (data.Result == true) {
                            var recipeUrl = data.Model.UrlName;
                            var urlToGo = '/recipe/edit/' + recipeUrl;
                            window.location.href = urlToGo;
                            r.common.showStatus($('.js-alert-success'));
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }

            });

        }

        function saveRecipe() {
            $(document).on('keydown', 'form.recipe', function(e) {
                if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                    e.preventDefault();
                    $('.js-recipe-save').click();
                }
            });
            if ($('#Description').length > 0) {
                $(document).on('keydown', '.froala-element', function(e) {
                    if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
                        e.preventDefault();
                        $('.js-recipe-save').click();
                        $('.js-encyclopedia-save').click();
                    }
                });
            }
            $('form.recipe').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            if (variantUpdate !== true) {
                                r.common.showStatus($('.js-alert-success'));
                            }
                            $('#Id').val(data.Model.Id);
                            $('#UrlName').val(data.Model.UrlName).trigger('change');
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                        if (variantUpdate === true) {
                            UpdateVariants(variantUrl);
                            variantUpdate = false;
                        }
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }

        function getMealBoxVariants() {

            $(document).on('change', '#mealboxkey', function(e) {
                // e.preventDefault();

                var mealboxkey = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: '/mealbox/GetMealBoxVariants',
                    data: { key: mealboxkey },
                    success: function(data) {

                        $('#itemno').empty();

                        $.each(data, function (index, item) {
                            $('#itemno')
                                .append($("<option></option>")
                                .attr("value", item.DataValueKey)
                                .text(item.DataTextValue));
                        });

                    }
                });

            });
        }

        function countChars() {
            $charCount = $('.chars-left');
            function theCount(len) {
                var max = 1000;
                var char = max - len;
                if ($('.js-recipe-mealbox-toggle').prop('checked')) {
                    $charCount.show();
                    $charCount.text(char + ' tegn tilbage (vejledende)');
                }
                else {
                    $charCount.hide();
                }
            }
            if ($('#Description').length > 0) {
                $(document).on('keyup', '.froala-element', function(e) {
                    theCount($('.froala-element').text().length);
                });
                theCount($('.froala-element').text().length)
            }
        }


        function toggleSaveUpdate() {
            function toggleOverview(element) {
                if (element.prop('checked')) {
                    $('.js-update-variants').addClass('visible');
                }
                else {
                    $('.js-update-variants').removeClass('visible');
                }
            }
            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleOverview($(this));
            });

            toggleOverview($('.js-recipe-mealbox-toggle'));
        }

        function toggleDuplicate() {
            function isUrlName() {
                if ($('#UrlName').length) {
                    if ($('#UrlName').val().length > 0) {
                        return true
                    }
                    else {
                        return false
                    }
                }
            }

            function toggleButton() {
                if (isUrlName() === true) {
                    $('.js-duplicate-recipe').addClass('visible');
                }
                else {
                    $('.js-duplicate-recipe').removeClass('visible');
                }
            }
            $(document).on('change', '#UrlName', function() {
                toggleButton();
            });
            toggleButton();

        }

        function toggleDays() {
            var mealdata = {};
            var daysData = [];

            var itemNo = $('#itemno :selected').val();
            var key = $('#mealboxkey :selected').val();
            var weekNo = $('input[name=mealboxweekno]').val();

            function eliminateDays(days) {
                $selects = $('select[name="MealBoxDay"]');
                $.each(days, function(index, value) {
                    $selects.find('option:eq(' + value + ')').attr('disabled','disabled');
                });
            }

            $.ajax({
                type: 'GET',
                url: '/mealbox/ExportAsJson',
                data: { itemNo: itemNo, key: key, weekNo: weekNo },
                dataType: 'json',
                success: function (data) {
                    mealdata = data;
                    for (var i = 0; i < mealdata.MealBox.length; i++) {
                        daysData.push(mealdata.MealBox[i].MealBoxDay);
                    };
                    eliminateDays(daysData);
                },
                error: function (error) {

                }
            });
        }


        var init = function() {
            updateAllVariants();
            autocompleteIngredients();
            autocompleteUnits();
            autocompleteTags();
            addToTagsList();
            imageInRecipe();
            addToIngredientsList();
            editIngredient();
            formatPersonInput();
            toggleMealbox();
            sortIngredientList();
            deleteRecipe();
            duplicateRecipe();
            saveRecipe();
            getMealBoxVariants();
            countChars();
            toggleSaveUpdate();
            toggleDuplicate();
            toggleDays();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);(function ($) {

    r.sideOverview = (function() {

        var thisMeal = {};
        var isSnapperPopulated = false;

        function getThisMeal() {

            var itemNo = $('#itemno :selected').val();
            var key = $('#mealboxkey :selected').val();
            var weekNo = $('input[name=mealboxweekno]').val();

            $.ajax({
                type: 'GET',
                url: '/mealbox/ExportAsJson',
                //data: { itemNo: '115037', key: 'KvikKassen', weekNo: '1614' },
                data: { itemNo: itemNo, key: key, weekNo: weekNo },
                dataType: 'json',
                success: function (data) {
                    thisMeal = data;
                    if (isSnapperPopulated === false) {
                        populateSnapper();
                    }
                    else {
                        populateContents();
                    }
                    $('.js-remember').hide();
                },
                error: function (error) {
                    $('.js-title-details').hide();
                    $('.js-list-bom').hide();
                    $('.js-list-days').hide();
                    $('.js-remember').show();
                }
            });
        }

        function populateContents() {
            function popTitle() {
                $('.js-title-details .js-title').html(thisMeal.Name);
                $('.js-title-details .js-week').html($('input[name=mealboxweekno]').val());
                $('.js-title-details .js-subtitle').html(thisMeal.Name2);
                $('.js-title-details .js-day').html($('select[name=MealBoxDay]').val());
                $('.js-title-details').show();
            }

            function popBom() {
                //get the list wrapper
                var $ul = $('.js-list-bom ul');
                //clear contents of wrapper
                $ul.find('li').remove();
                //declare bomArray based on thisMeal
                var bomArray = thisMeal.Bom;
                //run through array
                for (var i = 0; i < bomArray.length; i++) {
                    $ul.append('<li>' + bomArray[i].Quantity + ' ' + bomArray[i].UnitCode + ' ' + bomArray[i].Name + '</li>');
                };
                //show the list
                $('.js-list-bom').show();
            }
            function popDays() {
                //helper: render ingredients
                function renderIngredients(array) {
                    for (var j = 0; j < array.length; j++) {
                        return '<li>' + array[j].Amount + ' ' + array[j].Name + '</li>';
                    };
                }
                //get the list wrapper
                var $div = $('.js-list-days');
                //clear contents of lists
                $div.find('li').remove();
                //declare mealboxArray based on thisMeal
                var mealboxArray = thisMeal.MealBox;
                //run through mealboxArray
                for (var i = 0; i < mealboxArray.length; i++) {
                    //define the ingredientsArray in the current mealbox
                    var ingredientsArray = mealboxArray[i].Ingredients;
                    //find the current ul wrapper
                    $ulCurrent = $('ul[data-day=' + i + ']');
                    //fill the current ul based on ingredients array
                    for (var j = 0; j < ingredientsArray.length; j++) {
                        $ulCurrent.append('<li>' + ingredientsArray[j].Amount + ' ' + ingredientsArray[j].Name + '</li>');
                    };
                };
            }

            function init() {
                popTitle();
                popBom();
                popDays();
            }
            init();

        }

        function populateSnapper() {
            function popTitle() {
                $('.js-title-details .js-title').html(thisMeal.Name);
                $('.js-title-details .js-week').html($('input[name=mealboxweekno]').val());
                $('.js-title-details .js-subtitle').html(thisMeal.Name2);
                $('.js-title-details .js-day').html($('select[name=MealBoxDay]').val());
                $('.js-title-details').show();
            }

            function popBom() {
                //get the list wrapper
                var $ul = $('.js-list-bom ul');
                //clear contents of wrapper
                $ul.find('li').remove();
                //declare bomArray based on thisMeal
                var bomArray = thisMeal.Bom;
                //run through array
                for (var i = 0; i < bomArray.length; i++) {
                    $ul.append('<li>' + bomArray[i].Quantity + ' ' + bomArray[i].UnitCode + ' ' + bomArray[i].Name + '</li>');
                };
                //show the list
                $('.js-list-bom').show();
            }

            function popDays() {
                //helper: render ingredients
                function renderIngredients(array) {
                    for (var j = 0; j < array.length; j++) {
                        return '<li>' + array[j].Amount + ' ' + array[j].Name + '</li>';
                    };
                }
                //get the list wrapper
                var $div = $('.js-list-days');
                //clear contents of wrapper
                $div.find('> div').not('h3').remove();
                //declare mealboxArray based on thisMeal
                var mealboxArray = thisMeal.MealBox;
                //run through mealboxArray
                for (var i = 0; i < mealboxArray.length; i++) {
                    //append the main structure
                    $div.append('<div class="day"><a href="#">' + mealboxArray[i].MealBoxDay + '. ' + mealboxArray[i].Name + '</a><ul data-day="' + i + '"></ul></div>');
                    //define the ingredientsArray in the current mealbox
                    var ingredientsArray = mealboxArray[i].Ingredients;
                    //find the current ul wrapper
                    $ulCurrent = $('ul[data-day=' + i + ']');
                    //fill the current ul based on ingredients array
                    for (var j = 0; j < ingredientsArray.length; j++) {
                        $ulCurrent.append('<li>' + ingredientsArray[j].Amount + ' ' + ingredientsArray[j].Name + '</li>');
                    };
                };
                //show the list
                $('.js-list-days').show();
            }



            function init() {
                popTitle();
                popBom();
                popDays();
                isSnapperPopulated = true;
            }
            init();
        }

        function toggleOverview(element) {
            if (element.prop('checked')) {
                $('.js-overview-toggle').addClass('visible');
            }
            else {
                $('.js-overview-toggle').removeClass('visible');
            }
        }

        function toggleSnapper() {
            if (snapper.state().state === 'left' ) {
                snapper.close();
            }
            else {
                snapper.open('left');
                getThisMeal();
            }
        }

        function sideOverviewEvents() {
            $(document).on('change', '.js-recipe-mealbox-toggle', function(e) {
                e.preventDefault();
                toggleOverview($(this));
            });

            $(document).on('click', '.js-overview-toggle', function(e) {
                e.preventDefault();
                toggleSnapper();
            });

            $(document).on('keydown', 'html', function(e) {
                if (e.keyCode === 81 && e.ctrlKey) {
                    if ($('.js-recipe-mealbox-toggle').prop('checked')) {
                        toggleSnapper();
                    }
                }
            });

            $(document).on('click', '.ingredient-list-days .day a', function(e) {
                e.preventDefault();
                $(this).next('ul').toggle();
            });


            $(document).on('click', '.js-update-list', function(e) {
                e.preventDefault();
                $.when( getThisMeal() ).done(function() {
                    populateSnapper();
                });
            });

        }

        function snapper() {
            snapper = new Snap({
                element: document.getElementById('snap'),
                tapToClose: false,
                touchToDrag: false
            });
        }

        var init = function() {
            snapper();
            sideOverviewEvents();
            toggleOverview($('.js-recipe-mealbox-toggle'));

        };

        return {
            init: init,
            thisMeal: thisMeal
        };

    })();

})(jQuery);(function ($) {

    r.tips = (function() {

        function deleteTip() {

            $('.js-tip-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Er du sikker på at du vil slette tippet?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet tippet!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-recipe-delete').remove();
                            $listItem.find('.js-recipe-edit').remove();
                            $listItem.find('.js-recipe-duplicate').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        function saveTip() {
            $('form.tip').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            $('#Id').val(data.Model.Id);
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    },
                    error: function() {
                        r.common.showStatus($('.js-alert-error'));
                    }
                });
            });
        }

        function addToTipsList() {
            //special add for season
            $(document).on('click', '.js-tip-week-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(weekYear, MealBoxKey) {
                    return '<tr><td>' + weekYear + '</td><td>' + MealBoxKey + '</td><td><input name="Tags[0].WeekYear" type="hidden" value="' + weekYear + '"><input name="Tags[0].MealBoxKey" type="hidden" value="' + MealBoxKey + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $weekYear, $MealBoxKey, $listRowWrapper;

                //set input
                $weekYear = $('.js-tip-week-input');
                $MealBoxKey = $('#mealboxkey');

                //set value
                weekYearVal = $weekYear.val();
                MealBoxKeyVal = $MealBoxKey.val();

                //generate html for row
                listRow = generateListRow(weekYearVal, MealBoxKeyVal);

                //set the wrapper
                $listRowWrapper = $('.js-tip-week-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(weekYearVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $weekYear.val('');
                    $MealBoxKey.val('');

                    //focus on name
                    $weekYear.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.WeekYear');
                r.common.realignHiddenList('.MealBoxKey');
            });

            //removes list items from list
            $(document).on('click', '.js-tip-week-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.WeekYear');
                r.common.realignHiddenList('.MealBoxKey');
            });
        }

        //Tags
        function autocompleteTags() {
            $('.js-tip-tag-input').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        type: 'GET',
                        url: '/tag/autocomplete',
                        data: 'term=' + request.term,
                        success: function(result) {
                            response($.map(result, function(item) {
                                return {
                                    value: item.Name,
                                    label: item.Name,
                                    id: item.Id
                                };
                            }));
                        },
                        error: function (error) {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    });
                },
                select: function(event, ui) {
                    $list = $('.js-tip-tag-input');
                    $list.attr('data-id', ui.item.id);
                    $list.attr('data-value-set', true);
                },
                change: function(event, ui) {
                    if (ui.item === null) {
                        $list = $('.js-tip-tag-input');
                        $list.val('');
                        $list.focus();
                    }
                }
            });
        }

        function addToTagsList() {
            $(document).on('click', '.js-tip-tag-add', function(e) {
                e.preventDefault();

                //helper: generate list row
                function generateListRow(id, name) {
                    return '<tr><td>' + name + '</td><td><input name="Tags[0].Id" type="hidden" value="' + id + '"><input name="Tags[0].Name" type="hidden" value="' + name + '"><a class="btn center pull-right btn-danger btn-mini js-remove">X</a></td></tr>'
                }

                //variables
                var $id, $name, $listRowWrapper;

                //set input
                $name = $('.js-tip-tag-input');

                //set value
                idVal = $name.attr('data-id');
                nameVal = $name.val();

                //generate html for row
                listRow = generateListRow(idVal, nameVal);

                //set the wrapper
                $listRowWrapper = $('.js-tip-tags-list').find('tbody');

                //add the list row to the wrapper if value is not empty
                if (!(nameVal === '')) {
                    $listRowWrapper.append(listRow);

                    //clear the input
                    $name.val('');
                    $name.attr('data-value-set', false);

                    //focus on name
                    $name.focus();
                }

                //realign hidden lists
                r.common.realignHiddenList('.Id', '.js-tip-tags-list');
                r.common.realignHiddenList('.Name', '.js-tip-tags-list');
            });

            //removes list items from list
            $(document).on('click', '.js-tip-tags-list .js-remove', function(e) {
                e.preventDefault();
                r.common.removeFromList($(this));
                r.common.realignHiddenList('.Id', '.js-tip-tags-list');
                r.common.realignHiddenList('.Name', '.js-tip-tags-list');
            });

            //bind enter key
            $(document).on('keypress', '.js-tip-tag-input', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).attr('data-value-set') === 'true') {
                        $('.js-tip-tag-add').click();
                    }
                }
            });
        }


        var init = function() {
            deleteTip();
            saveTip();
            addToTipsList();
            addToTagsList();
            autocompleteTags();
        };

        return {
            init: init
        };

    })();

})(jQuery);(function ($) {

    r.types = (function() {

        function changeViewstate() {
            $(document).on('click', '.js-types-viewstate button', function(e) {
                e.preventDefault();
                r.common.changeViewstate($(this), '.js-types-view');
            });
        }

        function createType() {
            $(document).on('click', '.js-types-create', function(e) {
                e.preventDefault();

                var type, name, url;

                type = $('.js-types-create-type').val();
                name = $('.js-types-create-name').val();
                url = $(this).attr('href');

                $.post(url, { name: name, type: type }, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        $('.js-types-create-name').val('');
                        $('.js-types-create-name').focus();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
            //bind enter key
            $(document).on('keypress', '.js-types-create-name', function(e) {
                if (e.keyCode === 13) {
                $('.js-types-create').click();
                }
            });
        }

        function editType() {
            $(document).on('click', '.js-types-edit', function(e) {
                e.preventDefault();
                var $btn, $listItem;
                $btn = $(this);
                $listItem = $(this).closest('tr');
                //enable edit in textbox
                $listItem.find('input[name=name]').removeAttr('disabled');
                //hide current button
                $btn.removeClass('visible');
                //show other options
                $listItem.find('.js-types-delete').addClass('visible');
                $listItem.find('.js-types-update').addClass('visible');
            });


        }

        function updateType() {
            $(document).on('click', '.js-types-update', function(e) {
                e.preventDefault();

                var $btn, $listItem, name, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                name = $listItem.find('input[name=name]').val();
                url = $btn.attr('href') + '&name=' + name;

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //add success class
                        $listItem.addClass('success');
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        function deleteType() {
            $('.js-types-delete').confirmModal({
                confirmTitle: 'Bekræft venligst',
                confirmMessage: 'Vær opmærksom på at hvis du sletter denne type, vil den blive fjernet fra alle steder den bruges.<br>Er du sikker på at du vil slette typen?',
                confirmStyle: 'danger',
                confirmOk: '<i class="icon-trash icon-white"></i> Jeg er sikker - slet typen!',
                confirmCallback: function (target) {

                    var $btn, url, $listItem;

                    $btn = target;
                    url = $btn.attr('href');
                    $listItem = $btn.closest('tr');

                    $.post(url, function (data) {
                        if (data.Result == true) {
                            r.common.showStatus($('.js-alert-success'));
                            //disable edit in textbox
                            $listItem.find('input[name=name]').attr('disabled', 'disabled');
                            //add deleted class to row
                            $listItem.addClass('error');
                            //remove buttons from row
                            $listItem.find('.js-types-delete').remove();
                            $listItem.find('.js-types-update').remove();
                            $listItem.find('.js-types-edit').remove();
                        }
                        else {
                            r.common.showStatus($('.js-alert-error'));
                        }
                    })
                    .fail(function () {
                        r.common.showStatus($('.js-alert-error'));
                    });

                }
            });
        }

        var init = function() {
            changeViewstate();
            createType();
            editType();
            updateType();
            deleteType();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);(function ($) {

    r.userimages = (function() {

        function approveImage() {
            $(document).on('click', '.js-userimages-approve', function(e) {
                e.preventDefault();

                var $btn, $listItem, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                url = $btn.attr('href');

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //add success class
                        $listItem.addClass('success');
                        $btn.remove();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        function deleteImage() {
            $(document).on('click', '.js-userimages-delete', function(e) {
                e.preventDefault();

                var $btn, $listItem, url;

                $btn = $(this);
                $listItem = $(this).closest('tr');

                url = $btn.attr('href');

                $.post(url, function (data) {
                    if (data.Result == true) {
                        r.common.showStatus($('.js-alert-success'));
                        //remove row
                        $listItem.remove();
                    }
                    else {
                        r.common.showStatus($('.js-alert-error'));
                    }
                })
                .fail(function () {
                    r.common.showStatus($('.js-alert-error'));
                });
            });
        }

        var init = function() {
            approveImage();
            deleteImage();
        };

        var reInit = function() {

        };

        return {
            init: init,
            reInit: reInit
        };
    })();

})(jQuery);
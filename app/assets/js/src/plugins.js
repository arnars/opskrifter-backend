/* ===================================================
 * confirmModal by Maxime AILLOUD
 * https://github.com/mailloud/confirm-bootstrap
 * ===================================================
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENCE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this licence document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENCE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 * ========================================================== */


 (function($) {
    $.fn.confirmModal = function(opts)
    {
        var body = $('body');
        var defaultOptions    = {
            confirmTitle     : 'Please confirm',
            confirmMessage   : 'Are you sure you want to perform this action ?',
            confirmOk        : 'Yes',
            showButtons      : true,
            confirmCancel    : 'Cancel',
            confirmDirection : 'rtl',
            confirmStyle     : 'primary',
            confirmCallback  : defaultCallback,
            confirmPreOpen   : preOpenCallback
        };
        var options = $.extend(defaultOptions, opts);
        var time    = Date.now();

        var headModalTemplate =
            '<div class="modal hide fade" id="#modalId#" tabindex="-1" role="dialog" aria-labelledby="#AriaLabel#" aria-hidden="true">' +
                '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>' +
                    '<h3>#Heading#</h3>' +
                '</div>' +
                '<div class="modal-body">' +
                    '<p>#Body#</p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '#buttonTemplate#' +
                '</div>' +
            '</div>'
            ;

        return this.each(function(index)
        {

            var confirmLink = $(this);
            var targetData  = confirmLink.data();

            var currentOptions = $.extend(options, targetData);

            var modalId = "confirmModal" + parseInt(time + index);
            var modalTemplate = headModalTemplate;
            var buttonTemplate;
            if (options.showButtons === true) {
                buttonTemplate =
                    '<button class="btn" data-dismiss="modal" aria-hidden="true">#Cancel#</button>' +
                    '<button class="btn btn-#Style#" data-dismiss="ok" data-href="' + confirmLink.attr('href') + '">#Ok#</button>'
                ;
                if(options.confirmDirection == 'ltr')
                {
                    buttonTemplate =
                        '<button class="btn btn-#Style#" data-dismiss="ok" data-href="' + confirmLink.attr('href') + '">#Ok#</button>' +
                        '<button class="btn" data-dismiss="modal" aria-hidden="true">#Cancel#</button>'
                    ;
                }
            }
            else {
                buttonTemplate = '';
            }


            modalTemplate = modalTemplate.
                replace('#buttonTemplate#', buttonTemplate).
                replace('#modalId#', modalId).
                replace('#AriaLabel#', options.confirmTitle).
                replace('#Heading#', options.confirmTitle).
                replace('#Body#', options.confirmMessage).
                replace('#Ok#', options.confirmOk).
                replace('#Cancel#', options.confirmCancel).
                replace('#Style#', options.confirmStyle)
            ;

            body.append(modalTemplate);

            var confirmModal = $('#' + modalId);



            confirmLink.on('click', function(modalEvent)
            {
                modalEvent.preventDefault();
                options.confirmPreOpen(confirmLink, confirmModal);
                confirmModal.modal('show');

                $('button[data-dismiss="ok"]', confirmModal).on('click', function(event) {
                    confirmModal.modal('hide');
                    options.confirmCallback(confirmLink, confirmModal);
                });
            });
        });

        function preOpenCallback(target) {

        }

        function defaultCallback(target)
        {
            window.location = $(target).attr('href');
        }
    };
})(jQuery);/* ===================================================
 * bootstrap-transition.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#transitions
 * ===================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  })

}(window.jQuery);
/* =========================================================
 * bootstrap-modal.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#modals
 * =========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

  "use strict"; // jshint ;_;


 /* MODAL CLASS DEFINITION
  * ====================== */

  var Modal = function (element, options) {
    this.options = options
    this.$element = $(element)
      .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
    this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
  }

  Modal.prototype = {

      constructor: Modal

    , toggle: function () {
        return this[!this.isShown ? 'show' : 'hide']()
      }

    , show: function () {
        var that = this
          , e = $.Event('show')

        this.$element.trigger(e)

        if (this.isShown || e.isDefaultPrevented()) return

        this.isShown = true

        this.escape()

        this.backdrop(function () {
          var transition = $.support.transition && that.$element.hasClass('fade')

          if (!that.$element.parent().length) {
            that.$element.appendTo(document.body) //don't move modals dom position
          }

          that.$element.show()

          if (transition) {
            that.$element[0].offsetWidth // force reflow
          }

          that.$element
            .addClass('in')
            .attr('aria-hidden', false)

          that.enforceFocus()

          transition ?
            that.$element.one($.support.transition.end, function () { that.$element.focus().trigger('shown') }) :
            that.$element.focus().trigger('shown')

        })
      }

    , hide: function (e) {
        e && e.preventDefault()

        var that = this

        e = $.Event('hide')

        this.$element.trigger(e)

        if (!this.isShown || e.isDefaultPrevented()) return

        this.isShown = false

        this.escape()

        $(document).off('focusin.modal')

        this.$element
          .removeClass('in')
          .attr('aria-hidden', true)

        $.support.transition && this.$element.hasClass('fade') ?
          this.hideWithTransition() :
          this.hideModal()
      }

    , enforceFocus: function () {
        var that = this
        $(document).on('focusin.modal', function (e) {
          if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
            that.$element.focus()
          }
        })
      }

    , escape: function () {
        var that = this
        if (this.isShown && this.options.keyboard) {
          this.$element.on('keyup.dismiss.modal', function ( e ) {
            e.which == 27 && that.hide()
          })
        } else if (!this.isShown) {
          this.$element.off('keyup.dismiss.modal')
        }
      }

    , hideWithTransition: function () {
        var that = this
          , timeout = setTimeout(function () {
              that.$element.off($.support.transition.end)
              that.hideModal()
            }, 500)

        this.$element.one($.support.transition.end, function () {
          clearTimeout(timeout)
          that.hideModal()
        })
      }

    , hideModal: function () {
        var that = this
        this.$element.hide()
        this.backdrop(function () {
          that.removeBackdrop()
          that.$element.trigger('hidden')
        })
      }

    , removeBackdrop: function () {
        this.$backdrop && this.$backdrop.remove()
        this.$backdrop = null
      }

    , backdrop: function (callback) {
        var that = this
          , animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
          var doAnimate = $.support.transition && animate

          this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
            .appendTo(document.body)

          this.$backdrop.click(
            this.options.backdrop == 'static' ?
              $.proxy(this.$element[0].focus, this.$element[0])
            : $.proxy(this.hide, this)
          )

          if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

          this.$backdrop.addClass('in')

          if (!callback) return

          doAnimate ?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass('in')

          $.support.transition && this.$element.hasClass('fade')?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (callback) {
          callback()
        }
      }
  }


 /* MODAL PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.modal

  $.fn.modal = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('modal')
        , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option]()
      else if (options.show) data.show()
    })
  }

  $.fn.modal.defaults = {
      backdrop: true
    , keyboard: true
    , show: true
  }

  $.fn.modal.Constructor = Modal


 /* MODAL NO CONFLICT
  * ================= */

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


 /* MODAL DATA-API
  * ============== */

  $(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this)
      , href = $this.attr('href')
      , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
      , option = $target.data('modal') ? 'toggle' : $.extend({ remote:!/#/.test(href) && href }, $target.data(), $this.data())

    e.preventDefault()

    $target
      .modal(option)
      .one('hide', function () {
        $this.focus()
      })
  })

}(window.jQuery);

/* ============================================================
 * bootstrap-dropdown.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#dropdowns
 * ============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=dropdown]'
    , Dropdown = function (element) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) {
        if ('ontouchstart' in document.documentElement) {
          // if mobile we we use a backdrop because click events don't delegate
          $('<div class="dropdown-backdrop"/>').insertBefore($(this)).on('click', clearMenus)
        }
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }

  , keydown: function (e) {
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) {
        if (e.which == 27) $parent.find(toggle).focus()
        return $this.click()
      }

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }

  }

  function clearMenus() {
    $('.dropdown-backdrop').remove()
    $(toggle).each(function () {
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = selector && $(selector)

    if (!$parent || !$parent.length) $parent = $this.parent()

    return $parent
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.dropdown

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


 /* DROPDOWN NO CONFLICT
  * ==================== */

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(document)
    .on('click.dropdown.data-api', clearMenus)
    .on('click.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.dropdown.data-api'  , toggle, Dropdown.prototype.toggle)
    .on('keydown.dropdown.data-api', toggle + ', [role=menu]' , Dropdown.prototype.keydown)

}(window.jQuery);

/* =============================================================
 * bootstrap-scrollspy.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#scrollspy
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* SCROLLSPY CLASS DEFINITION
  * ========================== */

  function ScrollSpy(element, options) {
    var process = $.proxy(this.process, this)
      , $element = $(element).is('body') ? $(window) : $(element)
      , href
    this.options = $.extend({}, $.fn.scrollspy.defaults, options)
    this.$scrollElement = $element.on('scroll.scroll-spy.data-api', process)
    this.selector = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.$body = $('body')
    this.refresh()
    this.process()
  }

  ScrollSpy.prototype = {

      constructor: ScrollSpy

    , refresh: function () {
        var self = this
          , $targets

        this.offsets = $([])
        this.targets = $([])

        $targets = this.$body
          .find(this.selector)
          .map(function () {
            var $el = $(this)
              , href = $el.data('target') || $el.attr('href')
              , $href = /^#\w/.test(href) && $(href)
            return ( $href
              && $href.length
              && [[ $href.position().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]] ) || null
          })
          .sort(function (a, b) { return a[0] - b[0] })
          .each(function () {
            self.offsets.push(this[0])
            self.targets.push(this[1])
          })
      }

    , process: function () {
        var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
          , scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
          , maxScroll = scrollHeight - this.$scrollElement.height()
          , offsets = this.offsets
          , targets = this.targets
          , activeTarget = this.activeTarget
          , i

        if (scrollTop >= maxScroll) {
          return activeTarget != (i = targets.last()[0])
            && this.activate ( i )
        }

        for (i = offsets.length; i--;) {
          activeTarget != targets[i]
            && scrollTop >= offsets[i]
            && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
            && this.activate( targets[i] )
        }
      }

    , activate: function (target) {
        var active
          , selector

        this.activeTarget = target

        $(this.selector)
          .parent('.active')
          .removeClass('active')

        selector = this.selector
          + '[data-target="' + target + '"],'
          + this.selector + '[href="' + target + '"]'

        active = $(selector)
          .parent('li')
          .addClass('active')

        if (active.parent('.dropdown-menu').length)  {
          active = active.closest('li.dropdown').addClass('active')
        }

        active.trigger('activate')
      }

  }


 /* SCROLLSPY PLUGIN DEFINITION
  * =========================== */

  var old = $.fn.scrollspy

  $.fn.scrollspy = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('scrollspy')
        , options = typeof option == 'object' && option
      if (!data) $this.data('scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy

  $.fn.scrollspy.defaults = {
    offset: 10
  }


 /* SCROLLSPY NO CONFLICT
  * ===================== */

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


 /* SCROLLSPY DATA-API
  * ================== */

  $(window).on('load', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}(window.jQuery);
/* ========================================================
 * bootstrap-tab.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#tabs
 * ========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TAB CLASS DEFINITION
  * ==================== */

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.prototype = {

    constructor: Tab

  , show: function () {
      var $this = this.element
        , $ul = $this.closest('ul:not(.dropdown-menu)')
        , selector = $this.attr('data-target')
        , previous
        , $target
        , e

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      if ( $this.parent('li').hasClass('active') ) return

      previous = $ul.find('.active:last a')[0]

      e = $.Event('show', {
        relatedTarget: previous
      })

      $this.trigger(e)

      if (e.isDefaultPrevented()) return

      $target = $(selector)

      this.activate($this.parent('li'), $ul)
      this.activate($target, $target.parent(), function () {
        $this.trigger({
          type: 'shown'
        , relatedTarget: previous
        })
      })
    }

  , activate: function ( element, container, callback) {
      var $active = container.find('> .active')
        , transition = callback
            && $.support.transition
            && $active.hasClass('fade')

      function next() {
        $active
          .removeClass('active')
          .find('> .dropdown-menu > .active')
          .removeClass('active')

        element.addClass('active')

        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }

        if ( element.parent('.dropdown-menu') ) {
          element.closest('li.dropdown').addClass('active')
        }

        callback && callback()
      }

      transition ?
        $active.one($.support.transition.end, next) :
        next()

      $active.removeClass('in')
    }
  }


 /* TAB PLUGIN DEFINITION
  * ===================== */

  var old = $.fn.tab

  $.fn.tab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tab')
      if (!data) $this.data('tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tab.Constructor = Tab


 /* TAB NO CONFLICT
  * =============== */

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


 /* TAB DATA-API
  * ============ */

  $(document).on('click.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

}(window.jQuery);
/* ===========================================================
 * bootstrap-tooltip.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#tooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var Tooltip = function (element, options) {
    this.init('tooltip', element, options)
  }

  Tooltip.prototype = {

    constructor: Tooltip

  , init: function (type, element, options) {
      var eventIn
        , eventOut
        , triggers
        , trigger
        , i

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      triggers = this.options.trigger.split(' ')

      for (i = triggers.length; i--;) {
        trigger = triggers[i]
        if (trigger == 'click') {
          this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
        } else if (trigger != 'manual') {
          eventIn = trigger == 'hover' ? 'mouseenter' : 'focus'
          eventOut = trigger == 'hover' ? 'mouseleave' : 'blur'
          this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
          this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
        }
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function (options) {
      options = $.extend({}, $.fn[this.type].defaults, this.$element.data(), options)

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function (e) {
      var defaults = $.fn[this.type].defaults
        , options = {}
        , self

      this._options && $.each(this._options, function (key, value) {
        if (defaults[key] != value) options[key] = value
      }, this)

      self = $(e.currentTarget)[this.type](options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) return self.show()

      clearTimeout(this.timeout)
      self.hoverState = 'in'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'in') self.show()
      }, self.options.delay.show)
    }

  , leave: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (this.timeout) clearTimeout(this.timeout)
      if (!self.options.delay || !self.options.delay.hide) return self.hide()

      self.hoverState = 'out'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'out') self.hide()
      }, self.options.delay.hide)
    }

  , show: function () {
      var $tip
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp
        , e = $.Event('show')

      if (this.hasContent() && this.enabled) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $tip = this.tip()
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        $tip
          .detach()
          .css({ top: 0, left: 0, display: 'block' })

        this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

        pos = this.getPosition()

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        this.applyPlacement(tp, placement)
        this.$element.trigger('shown')
      }
    }

  , applyPlacement: function(offset, placement){
      var $tip = this.tip()
        , width = $tip[0].offsetWidth
        , height = $tip[0].offsetHeight
        , actualWidth
        , actualHeight
        , delta
        , replace

      $tip
        .offset(offset)
        .addClass(placement)
        .addClass('in')

      actualWidth = $tip[0].offsetWidth
      actualHeight = $tip[0].offsetHeight

      if (placement == 'top' && actualHeight != height) {
        offset.top = offset.top + height - actualHeight
        replace = true
      }

      if (placement == 'bottom' || placement == 'top') {
        delta = 0

        if (offset.left < 0){
          delta = offset.left * -2
          offset.left = 0
          $tip.offset(offset)
          actualWidth = $tip[0].offsetWidth
          actualHeight = $tip[0].offsetHeight
        }

        this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
      } else {
        this.replaceArrow(actualHeight - height, actualHeight, 'top')
      }

      if (replace) $tip.offset(offset)
    }

  , replaceArrow: function(delta, dimension, position){
      this
        .arrow()
        .css(position, delta ? (50 * (1 - delta / dimension) + "%") : '')
    }

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()

      $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function () {
      var that = this
        , $tip = this.tip()
        , e = $.Event('hide')

      this.$element.trigger(e)
      if (e.isDefaultPrevented()) return

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).detach()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.detach()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.detach()

      this.$element.trigger('hidden')

      return this
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function () {
      var el = this.$element[0]
      return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
        width: el.offsetWidth
      , height: el.offsetHeight
      }, this.$element.offset())
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , arrow: function(){
      return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function (e) {
      var self = e ? $(e.currentTarget)[this.type](this._options).data(this.type) : this
      self.tip().hasClass('in') ? self.hide() : self.show()
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  var old = $.fn.tooltip

  $.fn.tooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip

  $.fn.tooltip.defaults = {
    animation: true
  , placement: 'top'
  , selector: false
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  , trigger: 'hover focus'
  , title: ''
  , delay: 0
  , html: false
  , container: false
  }


 /* TOOLTIP NO CONFLICT
  * =================== */

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(window.jQuery);

/* ===========================================================
 * bootstrap-popover.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#popovers
 * ===========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* POPOVER PUBLIC CLASS DEFINITION
  * =============================== */

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }


  /* NOTE: POPOVER EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype, {

    constructor: Popover

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()
        , content = this.getContent()

      $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
      $tip.find('.popover-content')[this.options.html ? 'html' : 'text'](content)

      $tip.removeClass('fade top bottom left right in')
    }

  , hasContent: function () {
      return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)
        || $e.attr('data-content')

      return content
    }

  , tip: function () {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  })


 /* POPOVER PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.popover

  $.fn.popover = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popover')
        , options = typeof option == 'object' && option
      if (!data) $this.data('popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popover.Constructor = Popover

  $.fn.popover.defaults = $.extend({} , $.fn.tooltip.defaults, {
    placement: 'right'
  , trigger: 'click'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


 /* POPOVER NO CONFLICT
  * =================== */

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(window.jQuery);

/* ==========================================================
 * bootstrap-affix.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#affix
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* AFFIX CLASS DEFINITION
  * ====================== */

  var Affix = function (element, options) {
    this.options = $.extend({}, $.fn.affix.defaults, options)
    this.$window = $(window)
      .on('scroll.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.affix.data-api',  $.proxy(function () { setTimeout($.proxy(this.checkPosition, this), 1) }, this))
    this.$element = $(element)
    this.checkPosition()
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var scrollHeight = $(document).height()
      , scrollTop = this.$window.scrollTop()
      , position = this.$element.offset()
      , offset = this.options.offset
      , offsetBottom = offset.bottom
      , offsetTop = offset.top
      , reset = 'affix affix-top affix-bottom'
      , affix

    if (typeof offset != 'object') offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function') offsetTop = offset.top()
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom()

    affix = this.unpin != null && (scrollTop + this.unpin <= position.top) ?
      false    : offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ?
      'bottom' : offsetTop != null && scrollTop <= offsetTop ?
      'top'    : false

    if (this.affixed === affix) return

    this.affixed = affix
    this.unpin = affix == 'bottom' ? position.top - scrollTop : null

    this.$element.removeClass(reset).addClass('affix' + (affix ? '-' + affix : ''))
  }


 /* AFFIX PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.affix

  $.fn.affix = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('affix')
        , options = typeof option == 'object' && option
      if (!data) $this.data('affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.affix.Constructor = Affix

  $.fn.affix.defaults = {
    offset: 0
  }


 /* AFFIX NO CONFLICT
  * ================= */

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


 /* AFFIX DATA-API
  * ============== */

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
        , data = $spy.data()

      data.offset = data.offset || {}

      data.offsetBottom && (data.offset.bottom = data.offsetBottom)
      data.offsetTop && (data.offset.top = data.offsetTop)

      $spy.affix(data)
    })
  })


}(window.jQuery);
/* ==========================================================
 * bootstrap-alert.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#alerts
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* ALERT CLASS DEFINITION
  * ====================== */

  var dismiss = '[data-dismiss="alert"]'
    , Alert = function (el) {
        $(el).on('click', dismiss, this.close)
      }

  Alert.prototype.close = function (e) {
    var $this = $(this)
      , selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = $(selector)

    e && e.preventDefault()

    $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

    $parent.trigger(e = $.Event('close'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      $parent
        .trigger('closed')
        .remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent.on($.support.transition.end, removeElement) :
      removeElement()
  }


 /* ALERT PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.alert

  $.fn.alert = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('alert')
      if (!data) $this.data('alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.alert.Constructor = Alert


 /* ALERT NO CONFLICT
  * ================= */

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


 /* ALERT DATA-API
  * ============== */

  $(document).on('click.alert.data-api', dismiss, Alert.prototype.close)

}(window.jQuery);
/* ============================================================
 * bootstrap-button.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#buttons
 * ============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var Button = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.button.defaults, options)
  }

  Button.prototype.setState = function (state) {
    var d = 'disabled'
      , $el = this.$element
      , data = $el.data()
      , val = $el.is('input') ? 'val' : 'html'

    state = state + 'Text'
    data.resetText || $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout(function () {
      state == 'loadingText' ?
        $el.addClass(d).attr(d, d) :
        $el.removeClass(d).removeAttr(d)
    }, 0)
  }

  Button.prototype.toggle = function () {
    var $parent = this.$element.closest('[data-toggle="buttons-radio"]')

    $parent && $parent
      .find('.active')
      .removeClass('active')

    this.$element.toggleClass('active')
  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.button

  $.fn.button = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('button')
        , options = typeof option == 'object' && option
      if (!data) $this.data('button', (data = new Button(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.defaults = {
    loadingText: 'loading...'
  }

  $.fn.button.Constructor = Button


 /* BUTTON NO CONFLICT
  * ================== */

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


 /* BUTTON DATA-API
  * =============== */

  $(document).on('click.button.data-api', '[data-toggle^=button]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.button('toggle')
  })

}(window.jQuery);
/* =============================================================
 * bootstrap-collapse.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#collapse
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* COLLAPSE PUBLIC CLASS DEFINITION
  * ================================ */

  var Collapse = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.collapse.defaults, options)

    if (this.options.parent) {
      this.$parent = $(this.options.parent)
    }

    this.options.toggle && this.toggle()
  }

  Collapse.prototype = {

    constructor: Collapse

  , dimension: function () {
      var hasWidth = this.$element.hasClass('width')
      return hasWidth ? 'width' : 'height'
    }

  , show: function () {
      var dimension
        , scroll
        , actives
        , hasData

      if (this.transitioning || this.$element.hasClass('in')) return

      dimension = this.dimension()
      scroll = $.camelCase(['scroll', dimension].join('-'))
      actives = this.$parent && this.$parent.find('> .accordion-group > .in')

      if (actives && actives.length) {
        hasData = actives.data('collapse')
        if (hasData && hasData.transitioning) return
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
      }

      this.$element[dimension](0)
      this.transition('addClass', $.Event('show'), 'shown')
      $.support.transition && this.$element[dimension](this.$element[0][scroll])
    }

  , hide: function () {
      var dimension
      if (this.transitioning || !this.$element.hasClass('in')) return
      dimension = this.dimension()
      this.reset(this.$element[dimension]())
      this.transition('removeClass', $.Event('hide'), 'hidden')
      this.$element[dimension](0)
    }

  , reset: function (size) {
      var dimension = this.dimension()

      this.$element
        .removeClass('collapse')
        [dimension](size || 'auto')
        [0].offsetWidth

      this.$element[size !== null ? 'addClass' : 'removeClass']('collapse')

      return this
    }

  , transition: function (method, startEvent, completeEvent) {
      var that = this
        , complete = function () {
            if (startEvent.type == 'show') that.reset()
            that.transitioning = 0
            that.$element.trigger(completeEvent)
          }

      this.$element.trigger(startEvent)

      if (startEvent.isDefaultPrevented()) return

      this.transitioning = 1

      this.$element[method]('in')

      $.support.transition && this.$element.hasClass('collapse') ?
        this.$element.one($.support.transition.end, complete) :
        complete()
    }

  , toggle: function () {
      this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }

  }


 /* COLLAPSE PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.collapse

  $.fn.collapse = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('collapse')
        , options = $.extend({}, $.fn.collapse.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.defaults = {
    toggle: true
  }

  $.fn.collapse.Constructor = Collapse


 /* COLLAPSE NO CONFLICT
  * ==================== */

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


 /* COLLAPSE DATA-API
  * ================= */

  $(document).on('click.collapse.data-api', '[data-toggle=collapse]', function (e) {
    var $this = $(this), href
      , target = $this.attr('data-target')
        || e.preventDefault()
        || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
      , option = $(target).data('collapse') ? 'toggle' : $this.data()
    $this[$(target).hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
    $(target).collapse(option)
  })

}(window.jQuery);
/* ==========================================================
 * bootstrap-carousel.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#carousel
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* CAROUSEL CLASS DEFINITION
  * ========================= */

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options = options
    this.options.pause == 'hover' && this.$element
      .on('mouseenter', $.proxy(this.pause, this))
      .on('mouseleave', $.proxy(this.cycle, this))
  }

  Carousel.prototype = {

    cycle: function (e) {
      if (!e) this.paused = false
      if (this.interval) clearInterval(this.interval);
      this.options.interval
        && !this.paused
        && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
      return this
    }

  , getActiveIndex: function () {
      this.$active = this.$element.find('.item.active')
      this.$items = this.$active.parent().children()
      return this.$items.index(this.$active)
    }

  , to: function (pos) {
      var activeIndex = this.getActiveIndex()
        , that = this

      if (pos > (this.$items.length - 1) || pos < 0) return

      if (this.sliding) {
        return this.$element.one('slid', function () {
          that.to(pos)
        })
      }

      if (activeIndex == pos) {
        return this.pause().cycle()
      }

      return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
    }

  , pause: function (e) {
      if (!e) this.paused = true
      if (this.$element.find('.next, .prev').length && $.support.transition.end) {
        this.$element.trigger($.support.transition.end)
        this.cycle(true)
      }
      clearInterval(this.interval)
      this.interval = null
      return this
    }

  , next: function () {
      if (this.sliding) return
      return this.slide('next')
    }

  , prev: function () {
      if (this.sliding) return
      return this.slide('prev')
    }

  , slide: function (type, next) {
      var $active = this.$element.find('.item.active')
        , $next = next || $active[type]()
        , isCycling = this.interval
        , direction = type == 'next' ? 'left' : 'right'
        , fallback  = type == 'next' ? 'first' : 'last'
        , that = this
        , e

      this.sliding = true

      isCycling && this.pause()

      $next = $next.length ? $next : this.$element.find('.item')[fallback]()

      e = $.Event('slide', {
        relatedTarget: $next[0]
      , direction: direction
      })

      if ($next.hasClass('active')) return

      if (this.$indicators.length) {
        this.$indicators.find('.active').removeClass('active')
        this.$element.one('slid', function () {
          var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
          $nextIndicator && $nextIndicator.addClass('active')
        })
      }

      if ($.support.transition && this.$element.hasClass('slide')) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $next.addClass(type)
        $next[0].offsetWidth // force reflow
        $active.addClass(direction)
        $next.addClass(direction)
        this.$element.one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid') }, 0)
        })
      } else {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $active.removeClass('active')
        $next.addClass('active')
        this.sliding = false
        this.$element.trigger('slid')
      }

      isCycling && this.cycle()

      return this
    }

  }


 /* CAROUSEL PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.carousel

  $.fn.carousel = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('carousel')
        , options = $.extend({}, $.fn.carousel.defaults, typeof option == 'object' && option)
        , action = typeof option == 'string' ? option : options.slide
      if (!data) $this.data('carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  $.fn.carousel.defaults = {
    interval: 5000
  , pause: 'hover'
  }

  $.fn.carousel.Constructor = Carousel


 /* CAROUSEL NO CONFLICT
  * ==================== */

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }

 /* CAROUSEL DATA-API
  * ================= */

  $(document).on('click.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
    var $this = $(this), href
      , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      , options = $.extend({}, $target.data(), $this.data())
      , slideIndex

    $target.carousel(options)

    if (slideIndex = $this.attr('data-slide-to')) {
      $target.data('carousel').pause().to(slideIndex).cycle()
    }

    e.preventDefault()
  })

}(window.jQuery);
/* =============================================================
 * bootstrap-typeahead.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#typeahead
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function($){

  "use strict"; // jshint ;_;


 /* TYPEAHEAD PUBLIC CLASS DEFINITION
  * ================================= */

  var Typeahead = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.typeahead.defaults, options)
    this.matcher = this.options.matcher || this.matcher
    this.sorter = this.options.sorter || this.sorter
    this.highlighter = this.options.highlighter || this.highlighter
    this.updater = this.options.updater || this.updater
    this.source = this.options.source
    this.$menu = $(this.options.menu)
    this.shown = false
    this.listen()
  }

  Typeahead.prototype = {

    constructor: Typeahead

  , select: function () {
      var val = this.$menu.find('.active').attr('data-value')
      this.$element
        .val(this.updater(val))
        .change()
      return this.hide()
    }

  , updater: function (item) {
      return item
    }

  , show: function () {
      var pos = $.extend({}, this.$element.position(), {
        height: this.$element[0].offsetHeight
      })

      this.$menu
        .insertAfter(this.$element)
        .css({
          top: pos.top + pos.height
        , left: pos.left
        })
        .show()

      this.shown = true
      return this
    }

  , hide: function () {
      this.$menu.hide()
      this.shown = false
      return this
    }

  , lookup: function (event) {
      var items

      this.query = this.$element.val()

      if (!this.query || this.query.length < this.options.minLength) {
        return this.shown ? this.hide() : this
      }

      items = $.isFunction(this.source) ? this.source(this.query, $.proxy(this.process, this)) : this.source

      return items ? this.process(items) : this
    }

  , process: function (items) {
      var that = this

      items = $.grep(items, function (item) {
        return that.matcher(item)
      })

      items = this.sorter(items)

      if (!items.length) {
        return this.shown ? this.hide() : this
      }

      return this.render(items.slice(0, this.options.items)).show()
    }

  , matcher: function (item) {
      return ~item.toLowerCase().indexOf(this.query.toLowerCase())
    }

  , sorter: function (items) {
      var beginswith = []
        , caseSensitive = []
        , caseInsensitive = []
        , item

      while (item = items.shift()) {
        if (!item.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
        else if (~item.indexOf(this.query)) caseSensitive.push(item)
        else caseInsensitive.push(item)
      }

      return beginswith.concat(caseSensitive, caseInsensitive)
    }

  , highlighter: function (item) {
      var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        return '<strong>' + match + '</strong>'
      })
    }

  , render: function (items) {
      var that = this

      items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item)
        i.find('a').html(that.highlighter(item))
        return i[0]
      })

      items.first().addClass('active')
      this.$menu.html(items)
      return this
    }

  , next: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , next = active.next()

      if (!next.length) {
        next = $(this.$menu.find('li')[0])
      }

      next.addClass('active')
    }

  , prev: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , prev = active.prev()

      if (!prev.length) {
        prev = this.$menu.find('li').last()
      }

      prev.addClass('active')
    }

  , listen: function () {
      this.$element
        .on('focus',    $.proxy(this.focus, this))
        .on('blur',     $.proxy(this.blur, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup',    $.proxy(this.keyup, this))

      if (this.eventSupported('keydown')) {
        this.$element.on('keydown', $.proxy(this.keydown, this))
      }

      this.$menu
        .on('click', $.proxy(this.click, this))
        .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
        .on('mouseleave', 'li', $.proxy(this.mouseleave, this))
    }

  , eventSupported: function(eventName) {
      var isSupported = eventName in this.$element
      if (!isSupported) {
        this.$element.setAttribute(eventName, 'return;')
        isSupported = typeof this.$element[eventName] === 'function'
      }
      return isSupported
    }

  , move: function (e) {
      if (!this.shown) return

      switch(e.keyCode) {
        case 9: // tab
        case 13: // enter
        case 27: // escape
          e.preventDefault()
          break

        case 38: // up arrow
          e.preventDefault()
          this.prev()
          break

        case 40: // down arrow
          e.preventDefault()
          this.next()
          break
      }

      e.stopPropagation()
    }

  , keydown: function (e) {
      this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [40,38,9,13,27])
      this.move(e)
    }

  , keypress: function (e) {
      if (this.suppressKeyPressRepeat) return
      this.move(e)
    }

  , keyup: function (e) {
      switch(e.keyCode) {
        case 40: // down arrow
        case 38: // up arrow
        case 16: // shift
        case 17: // ctrl
        case 18: // alt
          break

        case 9: // tab
        case 13: // enter
          if (!this.shown) return
          this.select()
          break

        case 27: // escape
          if (!this.shown) return
          this.hide()
          break

        default:
          this.lookup()
      }

      e.stopPropagation()
      e.preventDefault()
  }

  , focus: function (e) {
      this.focused = true
    }

  , blur: function (e) {
      this.focused = false
      if (!this.mousedover && this.shown) this.hide()
    }

  , click: function (e) {
      e.stopPropagation()
      e.preventDefault()
      this.select()
      this.$element.focus()
    }

  , mouseenter: function (e) {
      this.mousedover = true
      this.$menu.find('.active').removeClass('active')
      $(e.currentTarget).addClass('active')
    }

  , mouseleave: function (e) {
      this.mousedover = false
      if (!this.focused && this.shown) this.hide()
    }

  }


  /* TYPEAHEAD PLUGIN DEFINITION
   * =========================== */

  var old = $.fn.typeahead

  $.fn.typeahead = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('typeahead')
        , options = typeof option == 'object' && option
      if (!data) $this.data('typeahead', (data = new Typeahead(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.typeahead.defaults = {
    source: []
  , items: 8
  , menu: '<ul class="typeahead dropdown-menu"></ul>'
  , item: '<li><a href="#"></a></li>'
  , minLength: 1
  }

  $.fn.typeahead.Constructor = Typeahead


 /* TYPEAHEAD NO CONFLICT
  * =================== */

  $.fn.typeahead.noConflict = function () {
    $.fn.typeahead = old
    return this
  }


 /* TYPEAHEAD DATA-API
  * ================== */

  $(document).on('focus.typeahead.data-api', '[data-provide="typeahead"]', function (e) {
    var $this = $(this)
    if ($this.data('typeahead')) return
    $this.typeahead($this.data())
  })

}(window.jQuery);
/*
 * EnhanceJS version 1.1 - Test-Driven Progressive Enhancement
 * http://enhancejs.googlecode.com/
 * Copyright (c) 2010 Filament Group, Inc, authors.txt
 * Licensed under MIT (license.txt)
*/(function(a,b,c){function o(){return!!b.cookie}function p(){J(d.testName,"fail");a.location.reload()}function q(){J(d.testName,"pass");a.location.reload()}function r(){L(d.testName);a.location.reload()}function s(){f=b.createElement("body");i.insertBefore(f,i.firstChild);e=f}function t(){i.removeChild(f);e=b.body}function u(){var a=K(d.testName);if(a){if(a==="pass"){y();d.onPass()}else{d.onFail();x()}d.appendToggleLink&&v(function(){w(a)})}else{var b=!0;s();for(var c in d.tests){b=d.tests[c]();if(!b){d.alertOnFailure&&alert(c+" failed");break}}t();a=b?"pass":"fail";J(d.testName,a);if(b){y();d.onPass()}else{d.onFail();x()}d.appendToggleLink&&v(function(){w(a)})}}function v(b){if(g)b();else{var c=a.onload;a.onload=function(){c&&c();b()}}}function w(a){if(!d.appendToggleLink||!enhance.cookiesSupported)return;if(a){var c=b.createElement("a");c.href="#";c.className=d.testName+"_toggleResult";c.innerHTML=a==="pass"?d.forceFailText:d.forcePassText;c.onclick=a==="pass"?enhance.forceFail:enhance.forcePass;b.getElementsByTagName("body")[0].appendChild(c)}}function x(){i.className=i.className.replace(d.testName,"")}function y(){j=!0;d.loadStyles.length&&D();d.loadScripts.length?F():d.onScriptsLoaded()}function z(b,c){if(K(k)&&K(l)){L(k);L(l)}else{J(k,b);J(l,c)}a.location.reload()}function A(a){m.length==2&&(a==m[0]?a=m[1]:a==m[1]&&(a=m[0]));return a}function B(){var a=d.testName+"-incomplete";i.className.indexOf(a)===-1&&(i.className+=" "+a)}function C(a){if(a.constructor===Array){var b=!0;for(var c in a)b&&(b=!!a[c]);return b}return!!a}function D(){var a=-1,e;while(e=d.loadStyles[++a]){var f=b.createElement("link");f.type="text/css";f.rel="stylesheet";f.onerror=d.onLoadError;if(typeof e=="string"){f.href=e;h.appendChild(f)}else{if(e.media){e.media=A(e.media);d.media&&d.media[e.media]!==c&&(e.media=d.media[e.media])}e.excludemedia&&(e.excludemedia=A(e.excludemedia));var g=!0;e.media&&e.media!=="print"&&e.media!=="projection"&&e.media!=="speech"&&e.media!=="aural"&&e.media!=="braille"&&(g=n(e.media));g&&e.excludemedia&&(g=!n(e.excludemedia));g&&e.iecondition&&(g=E(e.iecondition));if(g&&e.ifsupported!==c){g=C(e.ifsupported);if(!g&&e.fallback!==c){e.href=e.fallback;g=!0}}if(g){for(var i in e)i!=="iecondition"&&i!=="excludemedia"&&i!=="ifsupported"&&i!=="fallback"&&f.setAttribute(i,e[i]);h.appendChild(f)}}}}function F(){d.queueLoading?G():H()}function G(){function b(){if(a.length===0)return!1;var c=a.shift(),e=I(c),f=!1;if(!e)return b();e.onload=e.onreadystatechange=function(){if(!f&&(!this.readyState||this.readyState=="loaded"||this.readyState=="complete")){f=!0;b()===!1&&d.onScriptsLoaded();this.onload=this.onreadystatechange=null}};h.insertBefore(e,h.firstChild)}var a=[].concat(d.loadScripts);b()}function H(){var a=-1,b;while(b=d.loadScripts[++a]){var c=I(b);c&&h.insertBefore(c,h.firstChild)}d.onScriptsLoaded()}function I(a){var e=b.createElement("script");e.type="text/javascript";e.onerror=d.onLoadError;if(typeof a=="string"){e.src=a;return e}if(a.media){a.media=A(a.media);d.media&&d.media[a.media]&&(a.media=d.media[a.media])}a.excludemedia&&(a.excludemedia=A(a.excludemedia));var f=!0;a.media&&(f=n(a.media));f&&a.excludemedia&&(f=!n(a.excludemedia));f&&a.iecondition&&(f=E(a.iecondition));if(f&&a.ifsupported!==c){f=C(a.ifsupported);if(!f&&a.fallback!==c){a.src=a.fallback;f=!0}}if(f){for(var g in a)g!=="iecondition"&&g!=="media"&&g!=="excludemedia"&&g!=="ifsupported"&&g!=="fallback"&&e.setAttribute(g,a[g]);return e}return!1}function J(a,c,d){d=d||90;var e=new Date;e.setTime(e.getTime()+d*24*60*60*1e3);var f="; expires="+e.toGMTString();b.cookie=a+"="+c+f+"; path=/"}function K(a){var c=a+"=",d=b.cookie.split(";");for(var e=0;e<d.length;e++){var f=d[e];while(f.charAt(0)==" ")f=f.substring(1,f.length);if(f.indexOf(c)==0)return f.substring(c.length,f.length)}return null}function L(a){J(a,"",-1)}function M(){if(b.readyState==null&&b.addEventListener){b.addEventListener("DOMContentLoaded",function a(){b.removeEventListener("DOMContentLoaded",a,!1);b.readyState="complete"},!1);b.readyState="loading"}}var d,e,f,g,h,i=b.documentElement,j=!1,k,l,m=[];b.getElementsByTagName?h=b.getElementsByTagName("head")[0]||i:h=i;var n=function(){var a={},d=b.createElement("div");d.setAttribute("id","ejs-qtest");return function(f){if(a[f]===c){s();var g=b.createElement("style");g.type="text/css";h.appendChild(g);var i="@media "+f+" { #ejs-qtest { position: absolute; width: 10px; } }";g.styleSheet?g.styleSheet.cssText=i:g.appendChild(b.createTextNode(i));e.appendChild(d);var j=d.offsetWidth;e.removeChild(d);h.removeChild(g);t();a[f]=j==10}return a[f]}}();a.enhance=function(a){a=a||{};d={};for(var b in enhance.defaultSettings){var e=a[b];d[b]=e!==c?e:enhance.defaultSettings[b]}for(var f in a.addTests)d.tests[f]=a.addTests[f];i.className.indexOf(d.testName)===-1&&(i.className+=" "+d.testName);k=d.testName+"-toggledmediaA";l=d.testName+"-toggledmediaB";m=[K(k),K(l)];setTimeout(function(){j||x()},3e3);u();M();v(function(){g=!0})};enhance.query=n;enhance.defaultTests={getById:function(){return!!b.getElementById},getByTagName:function(){return!!b.getElementsByTagName},createEl:function(){return!!b.createElement},boxmodel:function(){var a=b.createElement("div");a.style.cssText="width: 1px; padding: 1px;";e.appendChild(a);var c=a.offsetWidth;e.removeChild(a);return c===3},position:function(){var a=b.createElement("div");a.style.cssText="position: absolute; left: 10px;";e.appendChild(a);var c=a.offsetLeft;e.removeChild(a);return c===10},floatClear:function(){var a=!1,c=b.createElement("div"),d='style="width: 5px; height: 5px; float: left;"';c.innerHTML="<div "+d+"></div><div "+d+"></div>";e.appendChild(c);var f=c.childNodes,g=f[0].offsetTop,h=f[1],i=h.offsetTop;if(g===i){h.style.clear="left";i=h.offsetTop;g!==i&&(a=!0)}e.removeChild(c);return a},heightOverflow:function(){var a=b.createElement("div");a.innerHTML='<div style="height: 10px;"></div>';a.style.cssText="overflow: hidden; height: 0;";e.appendChild(a);var c=a.offsetHeight;e.removeChild(a);return c===0},ajax:function(){var a=!1,b=-1,c,d=[function(){return new XMLHttpRequest},function(){return new ActiveXObject("Msxml2.XMLHTTP")},function(){return new ActiveXObject("Msxml3.XMLHTTP")},function(){return new ActiveXObject("Microsoft.XMLHTTP")}];while(c=d[++b]){try{a=c()}catch(e){continue}break}return!!a},resize:function(){return a.onresize!=0},print:function(){return!!a.print}};enhance.defaultSettings={testName:"enhanced",loadScripts:[],loadStyles:[],queueLoading:!0,appendToggleLink:!0,forcePassText:"View high-bandwidth version",forceFailText:"View low-bandwidth version",tests:enhance.defaultTests,media:{"-ejs-desktop":enhance.query("screen and (max-device-width: 1024px)")?"not screen and (max-device-width: 1024px)":"screen","-ejs-handheld":"screen and (max-device-width: 1024px)"},addTests:{},alertOnFailure:!1,onPass:function(){},onFail:function(){},onLoadError:B,onScriptsLoaded:function(){}};enhance.cookiesSupported=o();enhance.cookiesSupported&&(enhance.forceFail=p);enhance.cookiesSupported&&(enhance.forcePass=q);enhance.cookiesSupported&&(enhance.reTest=r);enhance.toggleMedia=z;var E=function(){var a={},d;return function(e){return!1}}()})(window,document);/**
 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
 *
 * @version 0.4.3
 * @codingstandard ftlabs-jslint
 * @copyright The Financial Times Limited [All Rights Reserved]
 * @license MIT License (see LICENSE.txt)
 */

/*jslint browser:true, node:true*/
/*global define*/


/**
 * Instantiate fast-clicking listeners on the specificed layer.
 *
 * @constructor
 * @param {Element} layer The layer to listen on
 */
function FastClick(layer) {
    'use strict';
    var oldOnClick, self = this;


    /**
     * Whether a click is currently being tracked.
     *
     * @type boolean
     */
    this.trackingClick = false;


    /**
     * Timestamp for when when click tracking started.
     *
     * @type number
     */
    this.trackingClickStart = 0;


    /**
     * The element being tracked for a click.
     *
     * @type Element
     */
    this.targetElement = null;


    /**
     * The FastClick layer.
     *
     * @type Element
     */
    this.layer = layer;

    if (!layer || !layer.nodeType) {
        throw new TypeError('Layer must be a document node');
    }

    // Bind handlers to this instance
    this.onClick = function() { FastClick.prototype.onClick.apply(self, arguments); };
    this.onTouchStart = function() { FastClick.prototype.onTouchStart.apply(self, arguments); };
    this.onTouchMove = function() { FastClick.prototype.onTouchMove.apply(self, arguments); };
    this.onTouchEnd = function() { FastClick.prototype.onTouchEnd.apply(self, arguments); };
    this.onTouchCancel = function() { FastClick.prototype.onTouchCancel.apply(self, arguments); };

    // Devices that don't support touch don't need FastClick
    if (typeof window.ontouchstart === 'undefined') {
        return;
    }

    // Set up event handlers as required
    layer.addEventListener('click', this.onClick, true);
    layer.addEventListener('touchstart', this.onTouchStart, false);
    layer.addEventListener('touchmove', this.onTouchMove, false);
    layer.addEventListener('touchend', this.onTouchEnd, false);
    layer.addEventListener('touchcancel', this.onTouchCancel, false);

    // If a handler is already declared in the element's onclick attribute, it will be fired before
    // FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
    // adding it as listener.
    if (typeof layer.onclick === 'function') {

        // Android browser on at least 3.2 requires a new reference to the function in layer.onclick
        // - the old one won't work if passed to addEventListener directly.
        oldOnClick = layer.onclick;
        layer.addEventListener('click', function(event) {
            oldOnClick(event);
        }, false);
        layer.onclick = null;
    }
}


/**
 * Android requires an exception for labels.
 *
 * @type boolean
 */
FastClick.prototype.deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;


/**
 * Determine whether a given element requires a native click.
 *
 * @param {Element} target Target DOM element
 * @returns {boolean} Returns true if the element needs a native click
 */
FastClick.prototype.needsClick = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {
    case 'label':
    case 'video':
        return true;
    default:
        return (/\bneedsclick\b/).test(target.className);
    }
};


/**
 * Determine whether a given element requires a call to focus to simulate click into element.
 *
 * @param {Element} target Target DOM element
 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
 */
FastClick.prototype.needsFocus = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {
    case 'textarea':
    case 'select':
        return true;
    case 'input':
        switch (target.type) {
        case 'button':
        case 'checkbox':
        case 'file':
        case 'image':
        case 'radio':
        case 'submit':
            return false;
        }
        return true;
    default:
        return (/\bneedsfocus\b/).test(target.className);
    }
};


/**
 * Send a click event to the element if it needs it.
 *
 * @returns {boolean} Whether the click was sent or not
 */
FastClick.prototype.maybeSendClick = function(targetElement, event) {
    'use strict';
    var clickEvent, touch;

    // Prevent the actual click from going though - unless the target node is marked as requiring
    // real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted
    // to open the options list and so the original event is required.
    if (this.needsClick(targetElement)) {
        return false;
    }

    // On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
    if (document.activeElement && document.activeElement !== targetElement) {
        document.activeElement.blur();
    }

    touch = event.changedTouches[0];

    // Synthesise a click event, with an extra attribute so it can be tracked
    clickEvent = document.createEvent('MouseEvents');
    clickEvent.initMouseEvent('click', true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
    clickEvent.forwardedTouchEvent = true;
    targetElement.dispatchEvent(clickEvent);

    return true;
};


/**
 * On touch start, record the position and scroll offset.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onTouchStart = function(event) {
    'use strict';
    var touch = event.targetTouches[0];

    this.trackingClick = true;
    this.trackingClickStart = event.timeStamp;
    this.targetElement = event.target;

    this.touchStartX = touch.pageX;
    this.touchStartY = touch.pageY;

    if (event.timeStamp - this.lastClickTime < 200) {
        event.preventDefault();
    }
    return true;
};


/**
 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.touchHasMoved = function(event) {
    'use strict';
    var touch = event.targetTouches[0];

    if (Math.abs(touch.pageX - this.touchStartX) > 10 || Math.abs(touch.pageY - this.touchStartY) > 10) {
        return true;
    }

    return false;
};


/**
 * Update the last position.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onTouchMove = function(event) {
    'use strict';
    if (!this.trackingClick) {
        return true;
    }

    // If the touch has moved, cancel the click tracking
    if (this.targetElement !== event.target || this.touchHasMoved(event)) {
        this.trackingClick = false;
        this.targetElement = null;
    }

    return true;
};


/**
 * Attempt to find the labelled control for the given label element.
 *
 * @param {HTMLLabelElement} labelElement
 * @returns {HTMLInputElement|null}
 */
FastClick.prototype.findControl = function(labelElement) {
    'use strict';

    // Fast path for newer browsers supporting the HTML5 control attribute
    if (labelElement.control !== undefined) {
        return labelElement.control;
    }

    // All browsers under test that support touch events also support the HTML5 htmlFor attribute
    if (labelElement.htmlFor) {
        return document.getElementById(labelElement.htmlFor);
    }

    // If no for attribute exists, attempt to retrieve the first labellable descendant element
    // the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
    return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
};


/**
 * On touch end, determine whether to send a click event at once.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onTouchEnd = function(event) {
    'use strict';
    var forElement, trackingClickStart, targetElement = this.targetElement;

    if (!this.trackingClick) {
        return true;
    }

    if (event.timeStamp - this.lastClickTime < 200) {
        this.cancelNextClick = true
        return true;
    }

    this.lastClickTime = event.timeStamp

    trackingClickStart = this.trackingClickStart;
    this.trackingClick = false;
    this.trackingClickStart = 0;

    if (targetElement.nodeName.toLowerCase() === 'label') {
        forElement = this.findControl(targetElement);
        if (forElement) {
            targetElement.focus();
            if (this.deviceIsAndroid) {
                return false;
            }

            if (this.maybeSendClick(forElement, event)) {
                event.preventDefault();
            }

            return false;
        }
    } else if (this.needsFocus(targetElement)) {
        // If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.

        // Held clicks between 100ms and 150ms still created this behavior, so I lowered the threshold.
        if ((event.timeStamp - trackingClickStart) > 100) {

            this.targetElement = null;
            return true;
        }

        targetElement.focus();

        // Select elements need the event to go through at least on iOS, otherwise the selector menu won't open.
        if (targetElement.tagName.toLowerCase() !== 'select') {
            event.preventDefault();
        }

        return false;
    }

    if (!this.maybeSendClick(targetElement, event)) {
        return false;
    }

    event.preventDefault();
    return false;
};


/**
 * On touch cancel, stop tracking the click.
 *
 * @returns {void}
 */
FastClick.prototype.onTouchCancel = function() {
    'use strict';
    this.trackingClick = false;
    this.targetElement = null;
};


/**
 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
 * an actual click which should be permitted.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onClick = function(event) {
    'use strict';

    var oldTargetElement;

    if (event.forwardedTouchEvent) {
        return true;
    }

    // If a target element was never set (because a touch event was never fired) allow the click
    if (!this.targetElement) {
        return true;
    }

    oldTargetElement = this.targetElement;
    this.targetElement = null;

    // Programmatically generated events targeting a specific element should be permitted
    if (!event.cancelable) {
        return true;
    }

    // Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
    if (event.target.type === 'submit' && event.detail === 0) {
        return true;
    }

    // Derive and check the target element to see whether the click needs to be permitted;
    // unless explicitly enabled, prevent non-touch click events from triggering actions,
    // to prevent ghost/doubleclicks.
    if (!this.needsClick(oldTargetElement) || this.cancelNextClick) {
        this.cancelNextClick = false
        // Prevent any user-added listeners declared on FastClick element from being fired.
        if (event.stopImmediatePropagation) {
            event.stopImmediatePropagation();
        }

        // Cancel the event
        event.stopPropagation();
        event.preventDefault();

        return false;
    }

    // If clicks are permitted, return true for the action to go through.
    return true;
};


/**
 * Remove all FastClick's event listeners.
 *
 * @returns {void}
 */
FastClick.prototype.destroy = function() {
    'use strict';
    var layer = this.layer;

    layer.removeEventListener('click', this.onClick, true);
    layer.removeEventListener('touchstart', this.onTouchStart, false);
    layer.removeEventListener('touchmove', this.onTouchMove, false);
    layer.removeEventListener('touchend', this.onTouchEnd, false);
    layer.removeEventListener('touchcancel', this.onTouchCancel, false);
};


if (typeof define === 'function' && define.amd) {

    // AMD. Register as an anonymous module.
    define(function() {
        'use strict';
        return FastClick;
    });
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = function(layer) {
        'use strict';
        return new FastClick(layer);
    };

    module.exports.FastClick = FastClick;
}/**
 * --------------------------------------------------------------------
 * jQuery customfileinput plugin
 * Author: Scott Jehl, scott@filamentgroup.com
 * Copyright (c) 2009 Filament Group. Updated 2012.
 * licensed under MIT (filamentgroup.com/examples/mit-license.txt)
 * --------------------------------------------------------------------
 */

/**
 * All credits go to the Author of this file, some additional customization was
 * done for theme compat. purposes.
 *
 * Additional bugfixes/changes by smurfy
 */
!function ($) {

    "use strict"; // jshint ;_;

    /* FILEINPUT CLASS DEFINITION
     * ====================== */

    var CustomFileInput = function (content, options) {
        var self = this;
        this.$element = $(content);

        this.options = $.extend({
            classes	: (this.$element.attr('class') ? this.$element.attr('class') : ''),
        }, options);

        //create custom control container
        this.$upload = $('<div class="input-' + (('right' === this.options.button_position)?'append':'prepend') + ' customfile">');
        //create custom control feedback
        this.$uploadFeedback = $('<input type="text" readonly="readonly" class="customfile-feedback ' + this.options.classes + '" aria-hidden="true" value="' + this.options.feedback_text + '"/>').appendTo(this.$upload);
        //create custom control button
        this.$uploadButton = $('<span class="add-on customfile-button" aria-hidden="true">' + this.options.button_text + '</span>').css({ float : this.options.button_position });

        this.$element
            .addClass('customfile-input') //add class for CSS
            .on('focus', $.proxy(this.onFocus, this))
            .on('blur', $.proxy(this.onBlur, this))
            .on('disable', $.proxy(this.onDisable, this))
            .on('enable', $.proxy(this.onEnable, this))
            .on('checkChange', $.proxy(this.onCheckChange, this))
            .on('change', $.proxy(this.onChange, this))
            .on('click', $.proxy(this.onClick, this));

        if ('right' === this.options.button_position) {
            this.$uploadButton.insertAfter(this.$uploadFeedback);
        } else {
            this.$uploadButton.insertBefore(this.$uploadFeedback);
        }

        //match disabled state
        if (this.$element.is('[disabled]')) {
            this.$element.trigger('disable');
        } else {
            this.$upload.on('click', function () { self.$element.trigger('click'); });
        }

        //insert original input file in dom, css if hide it outside of screen
        this.$upload.insertAfter(this.$element);
        this.$element.insertAfter(this.$upload);

    };

    CustomFileInput.prototype = {
        constructor: CustomFileInput,

        onClick : function() {
            var self = this;
            this.$element.data('val', this.$element.val());
            setTimeout(function(){
                self.$element.trigger('checkChange');
            } ,100);
        },

        onCheckChange: function() {
            if(this.$element.val() && this.$element.val() != this.$element.data('val')){
                this.$element.trigger('change');
            }
        },

        onEnable: function() {
            this.$element.removeAttr('disabled');
            this.$upload.removeClass('customfile-disabled');
        },

        onDisable: function() {
            this.$element.attr('disabled',true);
            this.$upload.addClass('customfile-disabled');
        },

        onFocus: function() {
            this.$upload.addClass('customfile-focus');
            this.$element.data('val', this.$element.val());
        },

        onBlur: function() {
            this.$upload.removeClass('customfile-focus');
            this.$element.trigger('checkChange');
        },

        onChange : function() {
            //get file name
            var fileName = this.$element.val().split(/\\/).pop();
            if (!fileName) {
                this.$uploadFeedback
                    .val(this.options.feedback_text) //set feedback text to filename
                    .removeClass('customfile-feedback-populated'); //add class to show populated state
                this.$uploadButton.text(this.options.button_text);
            } else {
                this.$uploadFeedback
                    .val(fileName) //set feedback text to filename
                    .addClass('customfile-feedback-populated'); //add class to show populated state
                this.$uploadButton.text(this.options.button_change_text);
            }

            //add the callback
            this.options.callback();
        }
    };

    $.fn.customFileInput = function(option){
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('customFileInput')
            var options = $.extend({}, $.fn.customFileInput.defaults, $this.data(), typeof option == 'object' && option);
            if (!data) {
                $this.data('customFileInput', (data = new CustomFileInput(this, options)));
            }
        })
    };

    $.fn.customFileInput.defaults = {
        button_position 	: 'right',
        feedback_text		: 'No file selected...',
        button_text			: 'Browse',
        button_change_text	: 'Change',
        callback : function() {}
    }

}(window.jQuery);

/*!
 * froala_editor v1.1.5 (http://editor.froala.com)
 * Copyright 2014-2014 Froala
 */
if("undefined"==typeof jQuery)throw new Error("Froala requires jQuery");!function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,a(c).data(),"object"==typeof d&&d),this.browser=b.browser(),this.disabledList=[],this._id=++b.count,this.init(c)};b.count=0,b.VALID_NODES=["P","PRE","BLOCKQUOTE","H1","H2","H3","H4","H5","H6","DIV","LI"],b.COLORS=["#000000","#444444","#666666","#999999","#CCCCCC","#EEEEEE","#F3F3F3","#FFFFFF","#FF0000","#FF9900","#FFFF00","#00FF00","#00FFFF","#0000FF","#9900FF","#FF00FF","#F4CCCC","#FCE5CD","#FFF2CC","#D9EAD3","#D0E0E3","#CFE2F3","#D9D2E9","#EAD1DC","#EA9999","#F9CB9C","#FFE599","#B6D7A8","#A2C4C9","#9FC5E8","#B4A7D6","#D5A6BD","#E06666","#F6B26B","#FFD966","#93C47D","#76A5AF","#6FA8DC","#8E7CC3","#C27BA0","#CC0000","#E69138","#F1C232","#6AA84F","#45818E","#3D85C6","#674EA7","#A64D79","#990000","#B45F06","#BF9000","#38771D","#134F5C","#0B5394","#351C75","#741B47","#660000","#783F04","#7F6000","#274E13","#0C343D","#073763","#201211","#4C1130"],b.image_commands={floatImageLeft:{title:"Float Left",icon:{type:"font",value:"fa fa-align-left"}},floatImageNone:{title:"Float None",icon:{type:"font",value:"fa fa-align-justify"}},floatImageRight:{title:"Float Right",icon:{type:"font",value:"fa fa-align-right"}},linkImage:{title:"Insert Link",icon:{type:"font",value:"fa fa-link"}},replaceImage:{title:"Replace Image",icon:{type:"font",value:"fa fa-exchange"}},removeImage:{title:"Remove Image",icon:{type:"font",value:"fa fa-trash-o"}}},b.commands={bold:{title:"Bold",icon:"fa fa-bold",shortcut:"(Ctrl + B)"},italic:{title:"Italic",icon:"fa fa-italic",shortcut:"(Ctrl + I)"},underline:{cmd:"underline",title:"Underline",icon:"fa fa-underline",shortcut:"(Ctrl + U)"},strikeThrough:{title:"Strikethrough",icon:"fa fa-strikethrough"},fontSize:{title:"Font Size",icon:"fa fa-text-height",seed:[{min:11,max:52}]},color:{icon:"fa fa-tint",title:"Color",seed:[{cmd:"backColor",value:b.COLORS,title:"Background Color"},{cmd:"foreColor",value:b.COLORS,title:"Text Color"}]},formatBlock:{title:"Format Block",icon:"fa fa-paragraph",seed:[{value:"n",title:"Normal"},{value:"p",title:"Paragraph"},{value:"pre",title:"Code"},{value:"blockquote",title:"Quote"},{value:"h1",title:"Heading 1"},{value:"h2",title:"Heading 2"},{value:"h3",title:"Heading 3"},{value:"h4",title:"Heading 4"},{value:"h5",title:"Heading 5"},{value:"h6",title:"Heading 6"}]},blockStyle:{title:"Block Style",icon:"fa fa-magic"},fontFamily:{title:"Font Family",icon:"fa fa-font"},align:{title:"Alignment",icon:"fa fa-align-center",seed:[{cmd:"justifyLeft",title:"Align Left",icon:"fa fa-align-left"},{cmd:"justifyCenter",title:"Align Center",icon:"fa fa-align-center"},{cmd:"justifyRight",title:"Align Right",icon:"fa fa-align-right"},{cmd:"justifyFull",title:"Justify",icon:"fa fa-align-justify"}]},insertOrderedList:{title:"Numbered List",icon:"fa fa-list-ol"},insertUnorderedList:{title:"Bulleted List",icon:"fa fa-list-ul"},outdent:{title:"Indent Less",icon:"fa fa-dedent",activeless:!0,shortcut:"(Ctrl + <)"},indent:{title:"Indent More",icon:"fa fa-indent",activeless:!0,shortcut:"(Ctrl + >)"},selectAll:{title:"Select All",icon:"fa fa-file-text",shortcut:"(Ctrl + A)"},createLink:{title:"Insert Link",icon:"fa fa-link",shortcut:"(Ctrl + K)"},insertImage:{title:"Insert Image",icon:"fa fa-picture-o",activeless:!0,shortcut:"(Ctrl + P)"},undo:{title:"Undo",icon:"fa fa-undo",activeless:!0,shortcut:"(Ctrl+Z)"},redo:{title:"Redo",icon:"fa fa-repeat",activeless:!0,shortcut:"(Shift+Ctrl+Z)"},html:{title:"Show HTML",icon:"fa fa-code"},save:{title:"Save",icon:"fa fa-floppy-o"},insertVideo:{title:"Insert Video",icon:"fa fa-video-camera"},insertHorizontalRule:{title:"Insert Horizontal Line",icon:"fa fa-minus"}},b.LANGS=[],b.DEFAULTS={allowedImageTypes:["jpeg","jpg","png","gif"],alwaysBlank:!1,alwaysVisible:!1,autosave:!1,autosaveInterval:1e4,blockTags:["n","p","blockquote","pre","h1","h2","h3","h4","h5","h6"],defaultBlockStyle:{"f-italic":"Italic","f-typewriter":"Typewriter","f-spaced":"Spaced","f-uppercase":"Uppercase"},blockStyles:{},borderColor:"#252528",buttons:["bold","italic","underline","strikeThrough","fontSize","fontFamily","color","sep","formatBlock","blockStyle","align","insertOrderedList","insertUnorderedList","outdent","indent","sep","createLink","insertImage","insertVideo","undo","redo","insertHorizontalRule","html"],crossDomain:!0,customButtons:{},customDropdowns:{},customText:!1,defaultImageWidth:300,direction:"ltr",editorClass:"",enableScript:!1,fontList:["Arial, Helvetica","Impact, Charcoal","Tahoma, Geneva","Verdana, Geneva","Times New Roman, Times"],height:"auto",icons:{},imageButtons:["floatImageLeft","floatImageNone","floatImageRight","linkImage","replaceImage","removeImage"],imageErrorCallback:!1,imageDeleteURL:null,imageDeleteParams:{},imageMargin:10,imageMove:!0,imageUploadParams:{},imageUploadParam:"file",imageUploadURL:"http://i.froala.com/upload",imagesLoadURL:"http://i.froala.com/images",imagesLoadParams:{},imageUpload:!0,imageUploadToS3:!1,inverseSkin:!1,inlineMode:!0,language:"en_us",maxImageSize:10485760,mediaManager:!0,minHeight:"auto",noFollow:!0,paragraphy:!0,placeholder:"Type something",plainPaste:!1,preloaderSrc:"",saveURL:null,saveParams:{},shortcuts:!0,spellcheck:!1,textNearImage:!0,toolbarFixed:!0,typingTimer:200,width:"auto",zIndex:1e3},b.hexToRGB=function(a){var b=/^#?([a-f\d])([a-f\d])([a-f\d])$/i;a=a.replace(b,function(a,b,c,d){return b+b+c+c+d+d});var c=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(a);return c?{r:parseInt(c[1],16),g:parseInt(c[2],16),b:parseInt(c[3],16)}:null},b.hexToRGBString=function(a){var b=this.hexToRGB(a);return"rgb("+b.r+", "+b.g+", "+b.b+")"},b.getIEversion=function(){var a,b,c=-1;return"Microsoft Internet Explorer"==navigator.appName?(a=navigator.userAgent,b=new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})"),null!==b.exec(a)&&(c=parseFloat(RegExp.$1))):"Netscape"==navigator.appName&&(a=navigator.userAgent,b=new RegExp("Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})"),null!==b.exec(a)&&(c=parseFloat(RegExp.$1))),c},b.browser=function(){var a={};if(b.getIEversion()>0)a.msie=!0;else{var c=navigator.userAgent.toLowerCase(),d=/(chrome)[ \/]([\w.]+)/.exec(c)||/(webkit)[ \/]([\w.]+)/.exec(c)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(c)||/(msie) ([\w.]+)/.exec(c)||c.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(c)||[],e={browser:d[1]||"",version:d[2]||"0"};d[1]&&(a[e.browser]=!0),parseInt(e.version,10)<9&&a.msie&&(a.oldMsie=!0),a.chrome?a.webkit=!0:a.webkit&&(a.safari=!0)}return a},b.prototype.text=function(){var a="";return window.getSelection?a=window.getSelection():document.getSelection?a=document.getSelection():document.selection&&(a=document.selection.createRange().text),a.toString()},b.prototype.selectionInEditor=function(){var b=this.getSelectionParent(),c=!1;return b==this.$element.get(0)&&(c=!0),c===!1&&a(b).parents().each(a.proxy(function(a,b){b==this.$element.get(0)&&(c=!0)},this)),c},b.prototype.getSelection=function(){var a="";return a=window.getSelection?window.getSelection():document.getSelection?document.getSelection():document.selection.createRange()},b.prototype.getRange=function(){if(window.getSelection){var a=window.getSelection();if(a.getRangeAt&&a.rangeCount)return a.getRangeAt(0)}return document.createRange()},b.prototype.clearSelection=function(){if(window.getSelection){var a=window.getSelection();a.removeAllRanges()}else document.selection.createRange&&(document.selection.createRange(),document.selection.empty())},b.prototype.getSelectionElement=function(){var b=this.getSelection();if(b.rangeCount){var c=b.getRangeAt(0).startContainer;return 1!=c.nodeType&&(c=c.parentNode),a(c).children().length>0&&a(a(c).children()[0]).text()==this.text()&&(c=a(c).children()[0]),c}return this.$element.get(0)},b.prototype.getSelectionParent=function(){var b,c=null;return window.getSelection?(b=window.getSelection(),b.rangeCount&&(c=b.getRangeAt(0).commonAncestorContainer,1!=c.nodeType&&(c=c.parentNode))):(b=document.selection)&&"Control"!=b.type&&(c=b.createRange().parentElement()),null!=c&&(a.inArray(this.$element.get(0),a(c).parents())>=0||c==this.$element.get(0))?c:null},b.prototype.nodeInRange=function(a,b){var c;if(a.intersectsNode)return a.intersectsNode(b);c=b.ownerDocument.createRange();try{c.selectNode(b)}catch(d){c.selectNodeContents(b)}return-1==a.compareBoundaryPoints(Range.END_TO_START,c)&&1==a.compareBoundaryPoints(Range.START_TO_END,c)},b.prototype.getElementFromNode=function(c){for(1!=c.nodeType&&(c=c.parentNode);null!==c&&b.VALID_NODES.indexOf(c.tagName)<0;)c=c.parentNode;return null!=c&&"LI"==c.tagName&&a(c).find(b.VALID_NODES.join()).length>0?null:a.makeArray(a(c).parents()).indexOf(this.$element.get(0))>=0?c:null},b.prototype.nextNode=function(a){if(a.hasChildNodes())return a.firstChild;for(;a&&!a.nextSibling;)a=a.parentNode;return a?a.nextSibling:null},b.prototype.getRangeSelectedNodes=function(a){var b=a.startContainer,c=a.endContainer;if(b==c)return[b];for(var d=[];b&&b!=c;)d.push(b=this.nextNode(b));for(b=a.startContainer;b&&b!=a.commonAncestorContainer;)d.unshift(b),b=b.parentNode;return d},b.prototype.getSelectedNodes=function(){if(window.getSelection){var a=window.getSelection();if(!a.isCollapsed)return this.getRangeSelectedNodes(a.getRangeAt(0));if(this.selectionInEditor()){var b=a.getRangeAt(0).startContainer;return 3==b.nodeType?[b.parentNode]:[b]}}return[]},b.prototype.getSelectionElements=function(){var b=this.getSelectedNodes(),c=[];return a.each(b,a.proxy(function(a,b){if(null!==b){var d=this.getElementFromNode(b);c.indexOf(d)<0&&d!=this.$element.get(0)&&null!==d&&c.push(d)}},this)),0===c.length&&c.push(this.$element.get(0)),c},b.prototype.getSelectionLink=function(){var a,b=null;return window.getSelection?(a=window.getSelection(),b=1!==a.anchorNode.nodeType?a.anchorNode.parentNode.parentNode.href:a.anchorNode.parentNode.href):(a=document.selection)&&"Control"!=a.type&&(b=a.createRange().parentElement().href),void 0===b?null:b},b.prototype.saveSelection=function(){var a,b,c,d=this.getSelection();if(d.getRangeAt&&d.rangeCount){for(c=[],a=0,b=d.rangeCount;b>a;a+=1)c.push(d.getRangeAt(a));this.savedRanges=c}else this.savedRanges=null},b.prototype.restoreSelection=function(){var a,b,c=this.getSelection();if(this.savedRanges)for(c.removeAllRanges(),a=0,b=this.savedRanges.length;b>a;a+=1)c.addRange(this.savedRanges[a])},b.prototype.saveSelectionByMarkers=function(){var a=this.getRange();this.placeMarker(a,!0),this.placeMarker(a,!1)},b.prototype.restoreSelectionByMarkers=function(){var a=this.$element.find("#marker-true"),b=this.$element.find("#marker-false");return a.length&&b.length?(this.$element.removeAttr("contenteditable"),this.$element.focus(),this.setSelection(a[0],0,b[0],0),this.removeMarkers(),this.isImage||this.isHTML||this.$element.attr("contenteditable",!0),void this.$element.focus()):!1},b.prototype.setSelection=function(a,b,c,d){try{null===c&&(c=a),null===d&&(d=b);var e=this.getSelection();if(!e)return;var f=this.getRange();f.setStart(a,b),f.setEnd(c,d),e.removeAllRanges(),e.addRange(f)}catch(g){}},b.prototype.placeMarker=function(b,c){try{var d=b.cloneRange();d.collapse(c),d.insertNode(a('<span id="marker-'+c+'">',document)[0]),d.detach()}catch(e){}},b.prototype.removeMarkers=function(){this.$element.find("#marker-true, #marker-false").removeAttr("id")},b.prototype.getBoundingRect=function(){var b;if(this.browser.mozilla){b={},this.saveSelectionByMarkers();var c=this.$element.find("#marker-true"),d=this.$element.find("#marker-false");b.left=c.offset().left-a(window).scrollLeft(),b.top=c.offset().top-a(window).scrollTop(),b.width=Math.abs(d.offset().left-c.offset().left),b.height=d.offset().top-c.offset().top+d.get(0).getBoundingClientRect().height,b.right=1,b.bottom=1,this.restoreSelectionByMarkers()}else b=this.getRange().getBoundingClientRect();return b},b.prototype.repositionEditor=function(b){var c,d,e;(this.options.inlineMode||b)&&(c=this.getBoundingRect(),c.left>0&&c.top>0&&c.right>0&&c.bottom>0?(d=c.left+c.width/2+a(window).scrollLeft(),e=c.top+c.height+a(window).scrollTop(),this.isTouch()&&(d=c.left+c.width/2,e=c.top+c.height),this.showByCoordinates(d,e)):this.options.alwaysVisible?this.hide():(document.execCommand("selectAll"),c=this.getBoundingRect(),d=c.left+a(window).scrollLeft(),e=c.top+c.height+a(window).scrollTop(),this.isTouch()&&(d=c.left,e=c.top+c.height),this.showByCoordinates(d,e-20),this.getRange().collapse(!1)))},b.prototype.destroy=function(){this.sync(),this.$editor.remove(),this.$popup_editor&&this.$popup_editor.remove(),this.$overlay&&this.$overlay.remove(),this.$image_modal&&(this.hideMediaManager(),this.$image_modal.remove()),this.$element.replaceWith(this.getHTML()),this.$box.removeClass("froala-box"),this.$box.find(".html-switch").remove(),this.$box.removeData("fa.editable"),clearTimeout(this.typingTimer),clearTimeout(this.ajaxInterval),this.$element.off("mousedown mouseup click keydown keyup focus keypress touchstart touchend touch drop"),this.$element.off("mousedown mouseup click keydown keyup focus keypress touchstart touchend touch drop","**"),a(window).off("mouseup."+this._id),a(window).off("keydown."+this._id),a(window).off("keyup."+this._id),a(window).off("hide."+this._id),a(document).off("selectionchange."+this._id),void 0!==this.$upload_frame&&this.$upload_frame.remove(),this.$textarea&&(this.$box.remove(),this.$textarea.removeData("fa.editable"),this.$textarea.show())},b.prototype.callback=function(b,c,d){void 0===d&&(d=!0);var e=b+"Callback",f=!0;return this.options[e]&&a.isFunction(this.options[e])&&(f=c?this.options[e].apply(this,c):this.options[e].call(this)),d===!0&&(this.sync(),this.$element.focus()),void 0===f?!0:f},b.prototype.html5Compliant=function(b){b.find("b").each(function(b,c){a(c).replaceWith("<strong>"+a(c).html()+"</strong>")}),b.find("i:not(.fa)").each(function(b,c){a(c).replaceWith("<em>"+a(c).html()+"</em>")})},b.prototype.syncClean=function(b,c){var d="span:empty";c&&(d="span:empty:not(#marker-true):not(#marker-false)");for(var e=!1,f=function(b,c){0===c.attributes.length&&(a(c).remove(),e=!1)};b.find(d).length&&e===!1;)e=!0,b.find(d).each(f);b.find("a").addClass("f-link")},b.prototype.addImageWrapper=function(){this.isImage||this.$element.find("img").each(function(b,c){0===a(c).parents(".f-img-wrap").length&&(a(c).parents("a").length>0?a(a(c).parents("a")[0]).wrap('<span class="f-img-wrap"></span>'):a(c).wrap('<span class="f-img-wrap"></span>'))})},b.prototype.sync=function(){this.restoreSelectionByMarkers(),this.syncClean(this.$element),this.disableImageResize(),this.$element.trigger("placeholderCheck"),this.trackHTML!==this.getHTML()&&(this.callback("contentChanged",[],!1),this.refreshImageList(),this.trackHTML=this.getHTML()),this.$textarea&&this.$textarea.val(this.getHTML())},b.prototype.emptyElement=function(b){if("IMG"==b.tagName||a(b).find("img").length>0)return!1;for(var c=a(b).text(),d=0;d<c.length;d++)if("\n"!==c[d]&&"\r"!==c[d]&&"	"!==c[d])return!1;return!0},b.prototype.init=function(a){this.initElement(a),this.initElementStyle(),this.initUndoRedo(),this.enableTyping(),this.initShortcuts(),this.initEditor(),this.initDrag(),this.initOptions(),this.initEditorSelection(),this.initAjaxSaver(),this.initImageResizer(),this.initImagePopup(),this.initLink(),this.setLanguage(),this.setCustomText(),this.registerPaste(),this.$element.blur()},b.prototype.initLink=function(){var b=this;this.$element.on("click touchend","a",function(c){c.stopPropagation(),c.preventDefault(),b.link=!0,b.clearSelection(),b.removeMarkers(),a(this).before('<span id="marker-true"></span>'),a(this).after('<span id="marker-false"></span>'),b.restoreSelectionByMarkers(),b.exec("createLink"),b.$link_wrapper.find('input[type="text"]').val(a(this).attr("href")),b.$link_wrapper.find('input[type="checkbox"]').prop("checked","_blank"==a(this).attr("target")),WYSIWYGModernizr.mq("(max-device-width: 320px) and (-webkit-min-device-pixel-ratio: 2)")?b.showByCoordinates(a(this).offset().left+a(this).width()/2,a(this).offset().top+a(this).height()):b.repositionEditor(!0),b.$popup_editor.show()})},b.prototype.imageHandle=function(){var b=this,c=a("<span>").addClass("f-img-handle").on({movestart:function(c){b.hide(),b.$element.addClass("f-non-selectable").removeAttr("contenteditable"),b.isResizing=!0,a(this).attr("data-start-x",c.startX),a(this).attr("data-start-y",c.startY)},move:function(c){var d=a(this),e=c.pageX-parseInt(d.attr("data-start-x"),10);d.attr("data-start-x",c.pageX),d.attr("data-start-y",c.pageY);var f=d.prevAll("img");d.hasClass("f-h-ne")||d.hasClass("f-h-se")?f.attr("width",f.width()+e):f.attr("width",f.width()-e),b.callback("imageResize",[],!1)},moveend:function(){b.isResizing=!1,a(this).removeAttr("data-start-x"),a(this).removeAttr("data-start-y"),b.$element.removeClass("f-non-selectable"),b.isImage||b.$element.attr("contenteditable",!0),b.$element.removeAttr("data-resize"),b.callback("imageResizeEnd")}});return c},b.prototype.disableImageResize=function(){if(this.browser.mozilla)try{document.execCommand("enableObjectResizing",!1,!1),document.execCommand("enableInlineTableEditing",!1,!1)}catch(a){}},b.prototype.initImageResizer=function(){this.disableImageResize();var b=this;this.$element.on("mousedown","img",function(){b.imageHTML=b.getHTML(),!b.options.imageMove||b.browser.msie?b.$element.attr("contenteditable",!1):(a(this).parents(".f-img-wrap").removeAttr("contenteditable"),a(this).parent().hasClass("f-img-editor")&&(b.closeImageMode(),b.hide()))}),document.addEventListener("drop",a.proxy(function(){setTimeout(a.proxy(function(){this.sync(),this.clearSelection()},this),10)},this)),this.$element.find("img").each(function(a,b){b.oncontrolselect=function(){return!1}}),this.$element.on("mouseup","img",function(){b.options.imageMove||b.isImage||b.isHTML||b.$element.attr("contenteditable",!0),a(this).parents(".f-img-wrap").attr("contenteditable","false")}),this.$element.on("click touchend","img",function(c){c.preventDefault(),c.stopPropagation(),b.$element.blur(),b.$image_editor.find("button").removeClass("active");var d=a(this).css("float");b.$image_editor.find('button[data-cmd="floatImage'+d.charAt(0).toUpperCase()+d.slice(1)+'"]').addClass("active"),b.$image_editor.find('.f-image-alt input[type="text"]').val(a(this).attr("alt")||a(this).attr("title")),b.showImageEditor(),a(this).parent().hasClass("f-img-editor")&&"SPAN"==a(this).parent().get(0).tagName||(a(this).wrap('<span class="f-img-editor" style="float: '+a(this).css("float")+"; margin-left:"+a(this).css("margin-left")+" ; margin-right:"+a(this).css("margin-right")+"; margin-bottom: "+a(this).css("margin-bottom")+"; margin-top: "+a(this).css("margin-bottom")+';"></span>'),a(this).css("margin-left","auto"),a(this).css("margin-right","auto"),a(this).css("margin-bottom","auto"),a(this).css("margin-top","auto"),0!==a(this).parents(".f-img-wrap").length||b.isImage||(a(this).parent().wrap('<span class="f-img-wrap"></span>'),a(this).parents(".f-img-wrap").attr("contenteditable",!1)));var e=b.imageHandle();a(this).parent().find(".f-img-handle").remove(),a(this).parent().append(e.clone(!0).addClass("f-h-ne")),a(this).parent().append(e.clone(!0).addClass("f-h-se")),a(this).parent().append(e.clone(!0).addClass("f-h-sw")),a(this).parent().append(e.clone(!0).addClass("f-h-nw")),b.clearSelection(),b.showByCoordinates(a(this).offset().left+a(this).width()/2,a(this).offset().top+a(this).height()),b.imageMode=!0}),this.$element.find("img").each(function(a,b){b.oncontrolselect=function(){return!1}})},b.prototype.initImagePopup=function(){this.$image_editor=a("<div>").addClass("bttn-wrapper f-image-editor");for(var c in this.options.imageButtons){var d=this.options.imageButtons[c];if(void 0!==b.image_commands[d]){var e=b.image_commands[d],f=a("<button>").addClass("fr-bttn").attr("data-cmd",d).attr("title",e.title);void 0!==this.options.icons[d]?this.prepareIcon(f,this.options.icons[d]):this.prepareIcon(f,e.icon),this.$image_editor.append(f)}}var g=a('<div class="f-image-alt">').append('<label><span data-text="true">Title</span>: </label>').append(a('<input type="text">').on("mouseup touchend keydown",function(a){a.stopPropagation()})).append(a('<button class="f-ok" data-text="true">').attr("data-cmd","setImageAlt").attr("title","OK").html("OK"));this.$image_editor.append("<hr/>").append(g);var h=this;this.$image_editor.find("button").click(function(b){b.stopPropagation(),h[a(this).attr("data-cmd")](h.$element.find("span.f-img-editor"))}),this.$popup_editor.append(this.$image_editor)},b.prototype.floatImageLeft=function(a){a.css("margin-left","auto"),a.css("margin-right",this.options.imageMargin),a.css("margin-bottom",this.options.imageMargin),a.css("margin-top",this.options.imageMargin),a.css("float","left"),a.find("img").css("float","left"),this.isImage&&this.$element.css("float","left"),this.saveUndoStep(),this.callback("floatImageLeft"),a.find("img").click()},b.prototype.floatImageNone=function(a){a.css("margin-left","auto"),a.css("margin-right","auto"),a.css("margin-bottom",this.options.imageMargin),a.css("margin-top",this.options.imageMargin),a.css("float","none"),a.find("img").css("float","none"),this.isImage||(a.parent().get(0)==this.$element.get(0)?a.wrap('<div style="text-align: center;"></div>'):a.parents(".f-img-wrap:first").css("text-align","center")),this.isImage&&this.$element.css("float","none"),this.saveUndoStep(),this.callback("floatImageNone"),a.find("img").click()},b.prototype.floatImageRight=function(a){a.css("margin-right","auto"),a.css("margin-left",this.options.imageMargin),a.css("margin-bottom",this.options.imageMargin),a.css("margin-top",this.options.imageMargin),a.css("float","right"),a.find("img").css("float","right"),this.isImage&&this.$element.css("float","right"),this.saveUndoStep(),this.callback("floatImageRight"),a.find("img").click()},b.prototype.linkImage=function(a){this.showInsertLink(),this.imageMode=!0,"A"==a.parent().get(0).tagName?(this.$link_wrapper.find('input[type="text"]').val(a.parent().attr("href")),"_blank"==a.parent().attr("target")?this.$link_wrapper.find('input[type="checkbox"]').prop("checked",!0):this.$link_wrapper.find('input[type="checkbox"]').prop("checked",!1)):(this.$link_wrapper.find('input[type="text"]').val("http://"),this.$link_wrapper.find('input[type="checkbox"]').prop("checked",this.options.alwaysBlank))},b.prototype.replaceImage=function(a){this.showInsertImage(),this.imageMode=!0,this.$image_wrapper.find('input[type="text"]').val(a.find("img").attr("src"))},b.prototype.removeImage=function(c){var d=c.find("img").get(0),e="Are you sure? Image will be deleted.";if(b.LANGS[this.options.language]&&(e=b.LANGS[this.options.language].translation[e]),confirm(e)){if(this.callback("beforeRemoveImage",[a(d).attr("src")],!1)===!0){var f=a(d).attr("src");c.parent(".f-img-wrap").remove(),this.refreshImageList(!0),this.hide(),this.saveUndoStep(),this.callback("afterRemoveImage",[f]),this.focus()}}else c.find("img").click()},b.prototype.setImageAlt=function(a){a.find("img").attr("alt",this.$image_editor.find('.f-image-alt input[type="text"]').val()),a.find("img").attr("title",this.$image_editor.find('.f-image-alt input[type="text"]').val()),this.saveUndoStep(),this.hide(),this.closeImageMode(),this.callback("setImageAlt")},b.prototype.getSelectionTextInfo=function(a){var b,c,d=!1,e=!1;if(window.getSelection){var f=window.getSelection();f.rangeCount&&(b=f.getRangeAt(0),c=b.cloneRange(),c.selectNodeContents(a),c.setEnd(b.startContainer,b.startOffset),d=""===c.toString(),c.selectNodeContents(a),c.setStart(b.endContainer,b.endOffset),e=""===c.toString())}else document.selection&&"Control"!=document.selection.type&&(b=document.selection.createRange(),c=b.duplicate(),c.moveToElementText(a),c.setEndPoint("EndToStart",b),d=""===c.text,c.moveToElementText(a),c.setEndPoint("StartToEnd",b),e=""===c.text);return{atStart:d,atEnd:e}},b.prototype.endsWith=function(a,b){return-1!==a.indexOf(b,a.length-b.length)},b.prototype.initElement=function(b){"TEXTAREA"==b.tagName?(this.$textarea=a(b),void 0!==this.$textarea.attr("placeholder")&&"Type something"==this.options.placeholder&&(this.options.placeholder=this.$textarea.attr("placeholder")),this.$element=a("<div>").html(this.$textarea.val()),this.$textarea.before(this.$element).hide(),this.$textarea.on("submit",a.proxy(function(){this.sync()},this))):"IMG"==b.tagName?("A"==a(b).parent().tagName&&(b=a(b).parent()),this.isImage=!0,this.imageList=[],this.options.paragraphy=!1,this.options.imageMargin="auto",a(b).wrap("<div>"),this.$element=a(b).parent(),this.$element.css("display","inline-block"),this.$element.css("max-width","100%"),this.$element.css("margin","auto")):("DIV"!=b.tagName&&this.options.buttons.indexOf("formatBlock")>=0&&this.disabledList.push("formatBlock"),this.$element=a(b)),this.isImage||(this.$box=this.$element,this.$element=a("<div>"),this.setHTML(this.$box.html(),!1),this.$box.empty(),this.$box.html(this.$element).addClass("froala-box"),this.$element.on("keydown",a.proxy(function(b){var c=b.which,d=["PRE","BLOCKQUOTE"],e=this.getSelectionElements()[0];if(13==c&&""===this.text()&&d.indexOf(e.tagName)>=0)if(this.getSelectionTextInfo(e).atEnd&&!b.shiftKey){b.preventDefault();var f=a("<p><br></p>");a(e).after(f),this.setSelection(f.get(0),0,null,0)}else(this.browser.webkit||this.browser.msie)&&(b.preventDefault(),this.insertHTML(this.endsWith(a(e).html(),"<br>")||!this.getSelectionTextInfo(e).atEnd?"<br>":"<br><br>"))},this)),this.$element.on("keyup",a.proxy(function(b){this.refreshButtons();var c=b.which;if(13==c&&""===this.text()&&this.browser.safari){var d=a(this.getSelectionElement());if(d.parent("li").length){this.saveSelectionByMarkers(),d.before('<span id="li-before"></span>'),d.after('<span id="li-after"></span>');var e=this.$element.html();e=e.replace(/<span id=\"li-before\"><\/span>/g,"</li><li>"),e=e.replace(/<span id=\"li-after\"><\/span>/g,"</li>"),this.$element.html(e),this.restoreSelectionByMarkers()}}13==c&&this.webkitParagraphy()},this))),this.trackHTML=this.getHTML(),this.sync(),this.$element.on("drop",function(){setTimeout(function(){a("html").click()},1)})},b.prototype.webkitParagraphy=function(){this.$element.find("*").each(a.proxy(function(b,c){if(this.emptyElement(c)&&"DIV"==c.tagName&&this.options.paragraphy===!0){var d=a("<p><br/></p>");a(c).replaceWith(d),this.setSelection(d.get(0),0,null,0)}},this))},b.prototype.refreshImageList=function(b){this.addImageWrapper();var c=[];if(this.$element.find("img").each(function(b,d){c.push(a(d).attr("src"))}),void 0===b)for(var d=0;d<this.imageList.length;d++)c.indexOf(this.imageList[d])<0&&this.callback("afterRemoveImage",[this.imageList[d]],!1);this.imageList=c},b.prototype.trim=function(a){return String(a).replace(/^\s+|\s+$/g,"")},b.prototype.unwrapText=function(){this.options.paragraphy||this.$element.find("div").each(function(b,c){void 0===a(c).attr("style")&&a(c).replaceWith(a(c).html()+"<br/>")})},b.prototype.wrapText=function(){if(this.isImage)return!1;this.webkitParagraphy();var c=[],d=["SPAN","A","B","I","EM","U","S","STRONG","STRIKE","FONT"],e=this,f=function(){var b;if(b=a(e.options.paragraphy===!0?"<p>":"<div>"),1==c.length&&("marker-false"===a(c[0]).attr("id")||"marker-true"===a(c[0]).attr("id")||""===a(c[0]).text()))return void(c=[]);for(var d in c)b.append(a(c[d]).clone()),d==c.length-1?a(c[d]).replaceWith(b):a(c[d]).remove();c=[]};this.$element.contents().filter(function(){this.nodeType==Node.TEXT_NODE&&a(this).text().trim().length>0||d.indexOf(this.tagName)>=0?c.push(this):this.nodeType==Node.TEXT_NODE&&0===a(this).text().trim().length?a(this).remove():f()}),f(),this.options.paragraphy&&this.$element.find("img").each(a.proxy(function(c,d){for(var e=a(d).parent();e.get(0)!=this.$element.get(0);){if(b.VALID_NODES.indexOf(e.get(0).tagName)>=0)return!0;e=e.parent()}a(d).parents(".f-img-wrap").length>0?a(d).parents(".f-img-wrap").wrap("<p></p>"):a(d).wrap("<p></p>")},this)),this.$element.find("> p, > div").each(function(b,c){0===a(c).text().trim().length&&0===a(c).find("img").length&&0===a(c).find("br").length&&a(c).append("<br/>")}),this.$element.find("div:empty, > br").remove()},b.prototype.setHTML=function(a,b){void 0===b&&(b=!0),this.options.enableScript||(a=this.stripScript(a)),this.$element.html(this.clean(a,!0,!1)),this.imageList=[],this.refreshImageList(),this.options.paragraphy&&this.wrapText(),b===!0&&this.sync()},b.prototype.registerPaste=function(){var b=this;this.$element.get(0).onpaste=function(){if(!b.isHTML){if(b.callback("beforePaste",[],!1)!==!0)return!1;b.pasting=!0,b.saveSelectionByMarkers();var c=a(window).scrollTop(),d=a('<div contenteditable="true"></div>').appendTo("body").focus();window.setTimeout(function(){var e=d.html();d.remove(),a(window).scrollTop(c),b.restoreSelectionByMarkers(),b.insertHTML(b.options.plainPaste?a("<div>").html(e).text():b.clean(e,!1,!0)),b.sync(),b.$element.trigger("placeholderCheck"),b.pasting=!1,b.callback("afterPaste")},1)}}},b.prototype._extractContent=function(a){for(var b,c=document.createDocumentFragment();b=a.firstChild;)c.appendChild(b);return c},b.prototype.clean=function(b,c,d){var e=["title","href","alt","src","style","width","height","target","rel","name","value","type"],f=["PRE","BLOCKQUOTE"];c===!0&&(e.push("id"),e.push("class"));var g=this,h=a("<div>").html(b);return h.find("*").each(function(b,c){f.indexOf(c.tagName)>=0&&a(c).html(a(c).text()),a.each(c.attributes,function(){void 0!==this&&e.indexOf(this.name)<0&&0!==this.name.indexOf("data-")&&a(c).removeAttr(this.name)}),d===!0&&a(c).removeAttr("style"),"A"==c.tagName&&a(c).attr("href",g.sanitizeURL(a(c).attr("href")))}),this.cleanNewLine(h.html())},b.prototype.cleanNewLine=function(a){var b=new RegExp("\\n","g");return a.replace(b,"")},b.prototype.stripScript=function(a){if(this.options.enableScript)return a;var b=document.createElement("div");b.innerHTML=a;for(var c=b.getElementsByTagName("script"),d=c.length;d--;)c[d].parentNode.removeChild(c[d]);return b.innerHTML},b.prototype.initElementStyle=function(){this.isImage||this.$element.attr("contenteditable",!0),this.$element.addClass("froala-element").addClass(this.options.editorClass),this.$element.css("outline",0),this.browser.msie||this.$element.addClass("not-msie")},b.prototype.initUndoRedo=function(){(this.isEnabled("undo")||this.isEnabled("redo"))&&(this.undoStack=[],this.undoIndex=0,this.saveUndoStep()),this.disableBrowserUndo()},b.prototype.enableTyping=function(){this.typingTimer=null,this.$element.on("keydown",a.proxy(function(){clearTimeout(this.typingTimer),this.ajaxSave=!1,this.oldHTML=this.$element.html(),this.typingTimer=setTimeout(a.proxy(function(){this.$element.html()!=this.oldHTML&&((this.isEnabled("undo")||this.isEnabled("redo"))&&this.$element.html()!=this.undoStack[this.undoIndex-1]&&this.saveUndoStep(),this.sync())},this),Math.max(this.options.typingTimer,200))},this))},b.prototype.getHTML=function(b){if(this.isHTML)return this.$html_area.val();var c=this.$element.clone();this.html5Compliant(c),this.syncClean(c,b),c.find(".f-img-editor > img").each(function(b,c){a(c).css("margin-left",a(c).parent().css("margin-left")),a(c).css("margin-right",a(c).parent().css("margin-right")),a(c).css("margin-bottom",a(c).parent().css("margin-bottom")),a(c).css("margin-top",a(c).parent().css("margin-top")),a(c).siblings("span.f-img-handle").remove().end().unwrap()}),c.find(".f-img-wrap").removeAttr("contenteditable"),this.isImage&&c.find(".f-img-wrap").each(function(b,c){a(c).replaceWith(a(c).html())}),void 0===b&&c.find("span#marker-true, span#marker-false").remove(),c.find("a:empty").remove();var d=c.html();return d.replace(/\u200B/g,"")},b.prototype.getText=function(){return this.$element.text()},b.prototype.initAjaxSaver=function(){this.ajaxHTML=this.getHTML(),this.ajaxSave=!0,this.ajaxInterval=setInterval(a.proxy(function(){this.ajaxHTML!=this.getHTML()&&this.ajaxSave&&(this.options.autosave&&this.save(),this.ajaxHTML=this.getHTML()),this.ajaxSave=!0},this),Math.max(this.options.autosaveInterval,100))
},b.prototype.disableBrowserUndo=function(){a("body").keydown(function(a){var b=a.which,c=a.ctrlKey||a.metaKey;if(!this.isHTML&&c){if(75==b)return a.preventDefault(),!1;if(90==b&&a.shiftKey)return a.preventDefault(),!1;if(90==b)return a.preventDefault(),!1}})},b.prototype.saveUndoStep=function(){if(this.isEnabled("undo")||this.isEnabled("redo")){for(;this.undoStack.length>this.undoIndex;)this.undoStack.pop();this.selectionInEditor()&&this.$element.is(":focus")&&this.saveSelectionByMarkers(),this.undoStack.push(this.getHTML(!0)),this.undoIndex++,this.selectionInEditor()&&this.$element.is(":focus")&&this.restoreSelectionByMarkers(),this.refreshUndoRedo()}},b.prototype.initShortcuts=function(){this.options.shortcuts&&this.$element.on("keydown",a.proxy(function(a){var b=a.which,c=a.ctrlKey||a.metaKey;if(!this.isHTML&&c){if(70==b)return this.show(null),!1;if(66==b)return this.execDefaultShortcut("bold");if(73==b)return this.execDefaultShortcut("italic");if(85==b)return this.execDefaultShortcut("underline");if(75==b)return this.execDefaultShortcut("createLink");if(80==b)return this.repositionEditor(),this.execDefaultShortcut("insertImage");if(65==b)return this.execDefaultShortcut("selectAll");if(221==b)return this.execDefaultShortcut("indent");if(219==b)return this.execDefaultShortcut("outdent");if(72==b)return this.execDefaultShortcut("html");if(48==b)return this.execDefaultShortcut("formatBlock","n");if(49==b)return this.execDefaultShortcut("formatBlock","h1");if(50==b)return this.execDefaultShortcut("formatBlock","h2");if(51==b)return this.execDefaultShortcut("formatBlock","h3");if(52==b)return this.execDefaultShortcut("formatBlock","h4");if(53==b)return this.execDefaultShortcut("formatBlock","h5");if(54==b)return this.execDefaultShortcut("formatBlock","h6");if(222==b)return this.execDefaultShortcut("formatBlock","blockquote");if(220==b)return this.execDefaultShortcut("formatBlock","pre");if(46==b||8==b)return this.execDefaultShortcut("strikeThrough");if(90==b&&a.shiftKey)return this.redo(),a.stopPropagation(),!1;if(90==b)return this.undo(),a.stopPropagation(),!1}9!=b||a.shiftKey?9==b&&a.shiftKey&&a.preventDefault():(a.preventDefault(),this.insertHTML("&nbsp;&nbsp;&nbsp;&nbsp;",!1))},this))},b.prototype.textEmpty=function(b){return(""===a(b).text()||b===this.$element.get(0))&&0===a(b).find("br").length},b.prototype.focus=function(){if(!this.isHTML&&(this.$element.focus(),""===this.text())){var a,c,d=this.getSelectionElements();for(a in d)if(c=d[a],!this.textEmpty(c))return void this.setSelection(c,0,null,0);d=this.$element.find(b.VALID_NODES.join(","));for(a in d)if(c=d[a],!this.textEmpty(c))return void this.setSelection(c,0,null,0);this.setSelection(this.$element.get(0),0,null,0)}},b.prototype.insertHTML=function(a,b){this.isHTML||(this.$element.focus(),this.selectionInEditor()||this.setSelection(this.$element.get(0),0,null,0));var c,d;if(window.getSelection){if(c=window.getSelection(),c.getRangeAt&&c.rangeCount){d=c.getRangeAt(0),d.deleteContents();var e=document.createElement("div");e.innerHTML=a;for(var f,g,h=document.createDocumentFragment();f=e.firstChild;)g=h.appendChild(f);var i=h.firstChild;d.insertNode(h),g&&(d=d.cloneRange(),d.setStartAfter(g),b?d.setStartBefore(i):d.collapse(!0),c.removeAllRanges(),c.addRange(d))}}else if((c=document.selection)&&"Control"!=c.type){var j=c.createRange();j.collapse(!0),c.createRange().pasteHTML(a),b&&(d=c.createRange(),d.setEndPoint("StartToStart",j),d.select())}},b.prototype.execDefaultShortcut=function(a,b){return this.isEnabled(a)?(this.exec(a,b),!1):!0},b.prototype.initEditor=function(){this.$editor=a("<div>"),this.$editor.addClass("froala-editor").hide(),a("body").append(this.$editor),this.options.inlineMode?this.initInlineEditor():this.initBasicEditor()},b.prototype.toolbarTop=function(){a(window).on("scroll resize",a.proxy(function(){this.options.toolbarFixed||this.options.inlineMode||(a(window).scrollTop()>this.$box.offset().top&&a(window).scrollTop()<this.$box.offset().top+this.$box.height()?(this.$editor.addClass("f-scroll"),this.$box.css("padding-top",this.$editor.height()),this.$editor.css("top",a(window).scrollTop()-this.$box.offset().top)):a(window).scrollTop()<this.$box.offset().top&&(this.$editor.removeClass("f-scroll"),this.$box.css("padding-top",""),this.$editor.css("top","")))},this))},b.prototype.initBasicEditor=function(){this.$element.addClass("f-basic"),this.$popup_editor=this.$editor.clone(),this.$popup_editor.appendTo(a("body")),this.$editor.addClass("f-basic").show(),this.$editor.insertBefore(this.$element),this.toolbarTop()},b.prototype.initInlineEditor=function(){this.$popup_editor=this.$editor},b.prototype.initDrag=function(){this.drag_support={filereader:"undefined"!=typeof FileReader,formdata:!!window.FormData,progress:"upload"in new XMLHttpRequest}},b.prototype.initOptions=function(){this.setDimensions(),this.setDirection(),this.setBorderColor(),this.setPlaceholder(),this.setPlaceholderEvents(),this.setSpellcheck(),this.setImageUploadURL(),this.setButtons(),this.setInverseSkin(),this.setTextNearImage(),this.setZIndex()},b.prototype.setImageUploadURL=function(a){a&&(this.options.imageUploadURL=a),this.options.imageUploadToS3&&(this.options.imageUploadURL="https://"+this.options.imageUploadToS3.bucket+"."+this.options.imageUploadToS3.region+".amazonaws.com/")},b.prototype.closeImageMode=function(){this.$element.find("span.f-img-editor > img").each(function(b,c){a(c).css("margin-left",a(c).parent().css("margin-left")),a(c).css("margin-right",a(c).parent().css("margin-right")),a(c).css("margin-bottom",a(c).parent().css("margin-bottom")),a(c).css("margin-top",a(c).parent().css("margin-top")),a(c).siblings("span.f-img-handle").remove().end().unwrap()}),this.$element.find("span.f-img-editor").length&&(this.$element.find("span.f-img-editor").remove(),this.$element.parents("span.f-img-editor").remove()),this.$element.removeClass("f-non-selectable"),this.isImage||this.isHTML||this.$element.attr("contenteditable",!0),this.$image_editor.hide()},b.prototype.isTouch=function(){return WYSIWYGModernizr.touch&&void 0!==window.Touch},b.prototype.initEditorSelection=function(){a(window).on("hide."+this._id,a.proxy(function(){this.hide(!1)},this)),this.$element.on("mousedown touchstart",a.proxy(function(){this.$element.attr("data-resize")||(this.closeImageMode(),this.hide())},this)),this.$element.contextmenu(a.proxy(function(a){return a.preventDefault(),this.options.inlineMode&&this.$element.focus(),!1},this)),this.$element.on("mouseup touchend",a.proxy(function(a){var b=this.text();""===b&&!this.options.alwaysVisible&&(3!=a.which&&2!=a.button||!this.options.inlineMode||this.isImage)||this.isTouch()?this.options.inlineMode||this.refreshButtons():(a.stopPropagation(),this.show(a)),this.imageMode=!1},this)),this.$element.on("mousedown touchstart","img, a",a.proxy(function(a){this.isResizing||this.$element.attr("data-resize")||a.stopPropagation()},this)),this.$element.on("mousedown touchstart",".f-img-handle",a.proxy(function(){this.$element.attr("data-resize",!0)},this)),this.$element.on("mouseup touchend",".f-img-handle",a.proxy(function(){this.$element.removeAttr("data-resize")},this)),this.$editor.on("mouseup touchend",a.proxy(function(a){a.stopPropagation(),this.options.inlineMode===!1&&this.hide()},this)),this.$popup_editor.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()},this)),this.$link_wrapper&&this.$link_wrapper.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()})),this.$image_wrapper&&this.$image_wrapper.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()})),this.$video_wrapper&&this.$video_wrapper.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()})),this.$overlay&&this.$overlay.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()})),this.$image_modal&&this.$image_modal.on("mouseup touchend",a.proxy(function(a){a.stopPropagation()})),a(window).on("mouseup."+this._id,a.proxy(function(){this.selectionInEditor()&&""!==this.text()&&!this.isTouch()?this.show(null):(this.hide(),this.closeImageMode())},this)),a(window).on("mouseup touchend",a.proxy(function(){a(window).trigger("window."+this._id)},this)),a(document).on("selectionchange."+this._id,a.proxy(function(a){if(this.options.inlineMode&&this.selectionInEditor()&&this.link!==!0&&this.isTouch()){var b=this.text();""!==b?(WYSIWYGModernizr.mq("(max-device-width: 320px) and (-webkit-min-device-pixel-ratio: 2)")?this.hide():this.show(null),a.stopPropagation()):this.hide()}},this)),a(document).on("selectionchange",function(b){a(document).trigger("selectionchange."+this._id,[b])}),a(window).bind("keydown."+this._id,a.proxy(function(a){var b=a.which;if(this.imageMode){if(13==b)return this.$element.find(".f-img-editor").parents(".f-img-wrap").before("<br/>"),this.sync(),this.$element.find(".f-img-editor img").click(),!1;if(46==b||8==b)return this.removeImage(this.$element.find(".f-img-editor")),!1}a.ctrlKey||(this.hide(),this.closeImageMode())},this)),a(window).bind("keydown",function(b){a(window).trigger("keydown."+this._id,[b])}),a(window).bind("keyup."+this._id,a.proxy(function(){this.selectionInEditor()&&""!==this.text()&&this.repositionEditor()},this)),a(window).bind("keyup",function(b){a(window).trigger("keyup."+this._id,[b])})},b.prototype.setTextNearImage=function(a){void 0!==a&&(this.options.textNearImage=a),this.options.textNearImage===!0?this.$element.removeClass("f-tni"):this.$element.addClass("f-tni")},b.prototype.setPlaceholder=function(a){a&&(this.options.placeholder=a),this.$textarea&&this.$textarea.attr("placeholder",this.options.placeholder),this.$element.attr("data-placeholder",this.options.placeholder)},b.prototype.isEmpty=function(){var a=this.$element.text().replace(/(\r\n|\n|\r|\t)/gm,"");return""===a&&0===this.$element.find("img, iframe, input").length&&0===this.$element.find("p > br, div > br").length&&0===this.$element.find("li, h1, h2, h3, h4, h5, h6, blockquote, pre").length},b.prototype.fakeEmpty=function(a){void 0===a&&(a=this.$element);var b=a.text().replace(/(\r\n|\n|\r|\t)/gm,"");return""===b&&1==a.find("p, div").length&&1==a.find("p > br, div > br").length&&0===a.find("img, iframe").length},b.prototype.setPlaceholderEvents=function(){this.$element.on("keyup keydown focus placeholderCheck",a.proxy(function(){if(this.pasting)return!1;if(!this.isEmpty()||this.fakeEmpty()||this.isHTML)!this.$element.find("p").length&&this.options.paragraphy?(this.wrapText(),this.$element.find("p, div").length?this.setSelection(this.$element.find("p, div")[0],this.$element.find("p, div").text().length,null,this.$element.find("p, div").text().length):this.$element.removeClass("f-placeholder")):this.fakeEmpty()===!1||this.$element.find(b.VALID_NODES.join(",")).length>1?this.$element.removeClass("f-placeholder"):this.$element.addClass("f-placeholder");else{var c,d=this.selectionInEditor()||this.$element.is(":focus");this.options.paragraphy?(c=a("<p><br/></p>"),this.$element.html(c),d&&this.setSelection(c.get(0),0,null,0),this.$element.addClass("f-placeholder")):this.$element.addClass("f-placeholder")}},this)),this.$element.trigger("placeholderCheck")},b.prototype.setDimensions=function(a,b,c){a&&(this.options.height=a),b&&(this.options.width=b),c&&(this.options.minHeight=c),"auto"!=this.options.height&&this.$element.css("height",this.options.height),"auto"!=this.options.minHeight&&this.$element.css("minHeight",this.options.minHeight),"auto"!=this.options.width&&this.$box.css("width",this.options.width)},b.prototype.setDirection=function(a){a&&(this.options.direction=a),"ltr"!=this.options.direction&&"rtl"!=this.options.direction&&(this.options.direction="ltr"),"rtl"==this.options.direction?(this.$element.addClass("f-rtl"),this.$editor.addClass("f-rtl"),this.$popup_editor.addClass("f-rtl")):(this.$element.removeClass("f-rtl"),this.$editor.removeClass("f-rtl"),this.$popup_editor.removeClass("f-rtl"))},b.prototype.setZIndex=function(a){a&&(this.options.zIndex=a),this.$popup_editor.css("z-index",this.options.zIndex),this.$overlay&&this.$overlay.css("z-index",this.options.zIndex+1),this.$image_modal&&this.$image_modal.css("z-index",this.options.zIndex+2)},b.prototype.setBorderColor=function(a){a&&(this.options.borderColor=a);var c=b.hexToRGB(this.options.borderColor);null!==c&&(this.$editor.css("border-color",this.options.borderColor),this.$editor.attr("data-border-color",this.options.borderColor),this.$image_modal&&this.$image_modal.find(".f-modal-wrapper").css("border-color",this.options.borderColor),this.options.inlineMode||this.$element.css("border-color",this.options.borderColor))},b.prototype.setSpellcheck=function(a){void 0!==a&&(this.options.spellcheck=a),this.$element.attr("spellcheck",this.options.spellcheck)},b.prototype.setInverseSkin=function(a){void 0!==a&&(this.options.inverseSkin=a),this.options.inverseSkin?(this.$editor.addClass("f-inverse"),this.$popup_editor.addClass("f-inverse")):(this.$editor.removeClass("f-inverse"),this.$popup_editor.removeClass("f-inverse"))},b.prototype.customizeText=function(b){if(b){var c=this.$editor.find("[title]").add(this.$popup_editor.find("[title]"));this.$image_modal&&(c=c.add(this.$image_modal.find("[title]"))),c.each(a.proxy(function(c,d){for(var e in b)a(d).attr("title").toLowerCase()==e.toLowerCase()&&a(d).attr("title",b[e])},this)),c=this.$editor.find('[data-text="true"]').add(this.$popup_editor.find('[data-text="true"]')),this.$image_modal&&(c=c.add(this.$image_modal.find('[data-text="true"]'))),c.each(a.proxy(function(c,d){for(var e in b)a(d).text().toLowerCase()==e.toLowerCase()&&a(d).text(b[e])},this))}},b.prototype.setLanguage=function(b){void 0!==b&&(this.options.language=b),a.Editable.LANGS[this.options.language]&&(this.customizeText(a.Editable.LANGS[this.options.language].translation),a.Editable.LANGS[this.options.language].direction&&this.setDirection(a.Editable.LANGS[this.options.language].direction),a.Editable.LANGS[this.options.language].translation[this.options.placeholder]&&this.setPlaceholder(a.Editable.LANGS[this.options.language].translation[this.options.placeholder]))},b.prototype.setCustomText=function(a){a&&(this.options.customText=a),this.options.customText&&this.customizeText(this.options.customText)},b.prototype.execHTML=function(){this.html()},b.prototype.initHTMLArea=function(){this.$html_area=a('<textarea wrap="hard">').keydown(function(b){var c=b.keyCode||b.which;if(9==c){b.preventDefault();var d=a(this).get(0).selectionStart,e=a(this).get(0).selectionEnd;a(this).val(a(this).val().substring(0,d)+"	"+a(this).val().substring(e)),a(this).get(0).selectionStart=a(this).get(0).selectionEnd=d+1}})},b.prototype.setButtons=function(c){c&&(this.options.buttons=c),this.$bttn_wrapper=a("<div>").addClass("bttn-wrapper"),this.$editor.append(this.$bttn_wrapper);for(var d in this.options.buttons){var e,f;"sep"==this.options.buttons[d]&&this.$bttn_wrapper.append(this.options.inlineMode?'<div class="f-clear"></div><hr/>':'<span class="f-sep"></span>');var g=b.commands[this.options.buttons[d]];if(void 0!==g)switch(g.cmd=this.options.buttons[d],g.cmd){case"color":e=this.buildDropdownColor(g),f=this.buildDropdownButton(g,"fr-color-picker").append(e),this.$bttn_wrapper.append(f);break;case"align":e=this.buildDropdownAlign(g),f=this.buildDropdownButton(g,"fr-selector").append(e),this.$bttn_wrapper.append(f);break;case"fontSize":e=this.buildDropdownFontsize(g),f=this.buildDropdownButton(g).append(e),this.$bttn_wrapper.append(f);break;case"formatBlock":e=this.buildDropdownFormatBlock(g),f=this.buildDropdownButton(g).append(e),this.$bttn_wrapper.append(f);break;case"blockStyle":e=this.buildDropdownBlockStyle(g),f=this.buildDropdownButton(g).append(e),this.$bttn_wrapper.append(f);break;case"fontFamily":e=this.buildDropdownFontFamily(),f=this.buildDropdownButton(g,"fr-family").append(e),this.$bttn_wrapper.append(f);break;case"createLink":f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f);break;case"insertImage":f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f);break;case"insertVideo":f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f),this.buildInsertVideo();break;case"undo":case"redo":f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f),f.prop("disabled",!0);break;case"html":f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f),this.options.inlineMode&&this.$box.append(f.clone(!0).addClass("html-switch").attr("title","Hide HTML").click(a.proxy(this.execHTML,this))),this.initHTMLArea();break;default:f=this.buildDefaultButton(g),this.$bttn_wrapper.append(f)}else{if(g=this.options.customButtons[this.options.buttons[d]],void 0===g){if(g=this.options.customDropdowns[this.options.buttons[d]],void 0===g)continue;f=this.buildCustomDropdown(g),this.$bttn_wrapper.append(f);continue}f=this.buildCustomButton(g),this.$bttn_wrapper.append(f)}}this.buildCreateLink(),this.buildInsertImage(),this.options.mediaManager&&this.buildMediaManager(),this.bindButtonEvents()},b.prototype.buildDefaultButton=function(b){var c=a("<button>").addClass("fr-bttn").attr("title",b.title).attr("data-cmd",b.cmd).attr("data-activeless",b.activeless);return void 0!==this.options.icons[b.cmd]?this.prepareIcon(c,this.options.icons[b.cmd]):this.addButtonIcon(c,b),c},b.prototype.prepareIcon=function(a,b){"font"==b.type?this.addButtonIcon(a,{icon:b.value}):"img"==b.type?this.addButtonIcon(a,{icon_img:b.value,title:a.attr("title")}):"txt"==b.type&&this.addButtonIcon(a,{icon_txt:b.value})},b.prototype.addButtonIcon=function(b,c){b.append(c.icon?a("<i>").addClass(c.icon):c.icon_alt?a("<i>").addClass("for-text").html(c.icon_alt):c.icon_img?a('<img src="'+c.icon_img+'">').attr("alt",c.title):c.icon_txt?a("<i>").html(c.icon_txt):c.title)},b.prototype.buildCustomButton=function(b){var c=a("<button>").addClass("fr-bttn").attr("title",b.title);return this.prepareIcon(c,b.icon),c.on("click touchend",a.proxy(function(a){a.stopPropagation(),a.preventDefault(),b.callback(this)},this)),c},b.prototype.buildCustomDropdown=function(b){var c=a("<div>").addClass("fr-bttn fr-dropdown"),d=a("<button>").addClass("fr-trigger").attr("title",b.title);this.prepareIcon(d,b.icon),c.append(d);var e=a("<ul>").addClass("fr-dropdown-menu");for(var f in b.options){var g=a("<li>").append(a('<a href="#">').append(f).on("click touch",b.options[f]));e.append(g)}return c.append(e)},b.prototype.buildDropdownButton=function(b,c){c=c||"";var d=a("<div>").addClass("fr-bttn fr-dropdown").addClass(c),e=a("<button>").addClass("fr-trigger").attr("title",b.title);return this.addButtonIcon(e,b),d.append(e),d},b.prototype.buildDropdownColor=function(b){var c=a("<div>").addClass("fr-dropdown-menu");for(var d in b.seed){var e=b.seed[d],f=a("<div>").append(a('<p data-text="true">').html(e.title));for(var g in e.value){var h=e.value[g];f.append(a("<button>").addClass("fr-color-bttn").attr("data-cmd",e.cmd).attr("data-val",h).attr("data-activeless",b.activeless).css("background-color",h).html("&nbsp;")),g%8==7&&g>0&&(f.append("<hr/>"),(7==g||15==g)&&f.append(a("<div>").addClass("separator")))}c.append(f)}return c},b.prototype.buildDropdownAlign=function(b){var c=a("<ul>").addClass("fr-dropdown-menu");for(var d in b.seed){var e=b.seed[d],f=a("<li>").append(a("<button>").addClass("fr-bttn").attr("data-cmd",e.cmd).attr("title",e.title).attr("data-activeless",b.activeless).append(a("<i>").addClass(e.icon)));c.append(f)}return c},b.prototype.buildDropdownFontsize=function(b){var c=a("<ul>").addClass("fr-dropdown-menu f-font-sizes");for(var d in b.seed)for(var e=b.seed[d],f=e.min;f<=e.max;f++){var g=a("<li>").attr("data-cmd",b.cmd).attr("data-val",f+"px").attr("data-activeless",b.activeless).append(a('<a href="#">').append(a("<span>").text(f+"px")));c.append(g)}return c},b.prototype.buildDropdownFormatBlock=function(b){var c=a("<ul>").addClass("fr-dropdown-menu");for(var d in b.seed){var e=b.seed[d];if(-1!=a.inArray(e.value,this.options.blockTags)){var f=a("<li>").append(a("<li>").attr("data-cmd",b.cmd).attr("data-val",e.value).attr("data-activeless",b.activeless).append(a('<a href="#" data-text="true">').addClass("format_"+e.value).attr("title",e.title).text(e.title)));c.append(f)}}return c},b.prototype.buildDropdownBlockStyle=function(){var b=a("<ul>").addClass("fr-dropdown-menu fr-block-style");return b.append(a('<li data-cmd="blockStyle" data-val="*">').append(a('<a href="#" data-text="true">').text("Default"))),b},b.prototype.buildDropdownFontFamily=function(){var b=a("<ul>").addClass("fr-dropdown-menu");for(var c in this.options.fontList){var d=this.options.fontList[c],e=a("<li>").attr("data-cmd","fontFamily").attr("data-val",d).append(a('<a href="#" data-text="true">').attr("title",d).css("font-family",d).text(d));b.append(e)}return b},b.prototype.buildMediaManager=function(){this.$image_modal=a("<div>").addClass("froala-modal").appendTo("body"),this.$overlay=a("<div>").addClass("froala-overlay").appendTo("body");var c=a("<div>").addClass("f-modal-wrapper").appendTo(this.$image_modal);a("<h4>").append('<span data-text="true">Manage images</span>').append(a('<i class="fa fa-times" title="Cancel">').click(a.proxy(function(){this.hideMediaManager()},this))).appendTo(c),this.$preloader=a('<img src="'+this.options.preloaderSrc+'" alt="Loading..."/>').addClass("f-preloader").appendTo(c),this.$preloader.hide(),this.$media_images=a("<div>").addClass("f-image-list").appendTo(c),WYSIWYGModernizr.touch&&this.$media_images.addClass("f-touch"),this.$media_images.on("click touch","img",a.proxy(function(b){b.stopPropagation();var c=b.currentTarget;this.writeImage(a(c).attr("src")),this.hideMediaManager()},this)),this.$media_images.on("click touchend",".f-delete-img",a.proxy(function(c){c.stopPropagation();var d=a(c.currentTarget).prev(),e="Are you sure? Image will be deleted.";b.LANGS[this.options.language]&&(e=b.LANGS[this.options.language].translation[e]),confirm(e)&&this.callback("beforeDeleteImage",[a(d).attr("src")],!1)!==!1&&(a(d).remove(),this.deleteImage(a(d).attr("src")))},this)),this.hideMediaManager()},b.prototype.deleteImage=function(b){this.options.imageDeleteURL?a.post(this.options.imageDeleteURL,a.extend({src:b},this.options.imageDeleteParams),a.proxy(function(a){this.callback("imageDeleteSuccess",[a],!1)},this)):this.callback("imageDeleteError",["Missing imageDeleteURL option."],!1)},b.prototype.loadImage=function(c){var d=new Image,e=a("<div>").addClass("f-empty");d.onload=a.proxy(function(){var a="Delete";b.LANGS[this.options.language]&&(a=b.LANGS[this.options.language].translation[a]),e.append('<img src="'+c+'"/><a class="f-delete-img"><span data-text="true">'+a+"</span></a>"),e.removeClass("f-empty"),this.$media_images.hide(),this.$media_images.show(),this.callback("imageLoaded",[c],!1)},this),d.onerror=a.proxy(function(){e.remove(),this.throwImageError(1)},this),d.src=c,this.$media_images.append(e)},b.prototype.processLoadedImages=function(a){try{var b=a;this.$media_images.empty();for(var c=0;c<b.length;c++)this.loadImage(b[c])}catch(d){this.throwImageError(4)}},b.prototype.loadImages=function(){this.$preloader.show(),this.$media_images.empty(),this.options.imagesLoadURL?a.get(this.options.imagesLoadURL,this.options.imagesLoadParams,a.proxy(function(a){this.callback("imagesLoaded",[a],!1),this.processLoadedImages(a),this.$preloader.hide()},this),"json").fail(a.proxy(function(){this.callback("imagesLoadError",["Load request failed."],!1),this.$preloader.hide()},this)):(this.callback("imagesLoadError",["Missing imagesLoadURL option."],!1),this.$preloader.hide())},b.prototype.showMediaManager=function(){this.$image_modal.show(),this.$overlay.show(),this.loadImages(),a("body").css("overflow","hidden")},b.prototype.hideMediaManager=function(){this.$image_modal.hide(),this.$overlay.hide(),a("body").css("overflow","")},b.prototype.buildInsertImage=function(){this.$image_wrapper=a("<div>").addClass("image-wrapper"),this.$popup_editor.append(this.$image_wrapper);var c=this;this.$progress_bar=a('<p class="f-progress">').append("<span></span>");var d=null;if(this.options.imageUpload){if(d=a('<div class="f-upload">').append('<strong data-text="true">Drop Image</strong><br>(<span data-text="true">or click</span>)').append(a('<form method="post" action="'+this.options.imageUploadURL+'" encoding="multipart/form-data" enctype="multipart/form-data" target="frame-'+this._id+'">').append(a('<input type="file" accept="image/*" name="'+this.options.imageUploadParam+'" />'))),this.browser.msie&&b.getIEversion()<=9&&null!==d){this.$upload_frame=a('<iframe id="frame-'+this._id+'" name="frame-'+c._id+'" src="javascript:false;" style="width:0; height:0; border:0px solid #FFF;" data-loaded="true">'),d.find("form").append(this.$upload_frame);var e=this.$upload_frame.bind("load",function(){e.unbind("load"),e.bind("load",function(){try{if(c.options.imageUploadToS3){var b=d.find("form").attr("action"),e=d.find('form input[name="key"]').val(),f=b+e;c.writeImage(f),c.options.imageUploadToS3.callback&&c.options.imageUploadToS3.callback.call(this,f,e)}else{var g=a(this).contents().text();c.parseImageResponse(g)}}catch(h){c.throwImageError(7)}})})}this.$image_wrapper.on("change",'input[type="file"]',function(){if(void 0!==this.files)c.uploadFile(this.files),setTimeout(function(){c.showInsertImage()},500);else{var b=a(this).parents("form");b.find('input[type="hidden"]').remove();var d;for(d in c.options.imageUploadParams)b.prepend('<input type="hidden" name="'+d+'" value="'+c.options.imageUploadParams[d]+'" />');if(void 0!==c.options.imageUploadToS3){for(d in c.options.imageUploadToS3.params)b.prepend('<input type="hidden" name="'+d+'" value="'+c.options.imageUploadToS3.params[d]+'" />');b.prepend('<input type="hidden" name="success_action_status" value="201" />'),b.prepend('<input type="hidden" name="X-Requested-With" value="xhr" />'),b.prepend('<input type="hidden" name="Content-Type" value="" />'),b.prepend('<input type="hidden" name="key" value="'+c.options.imageUploadToS3.keyStart+(new Date).getTime()+"-"+a(this).val().match(/[^\/\\]+$/)+'" />')}else b.prepend('<input type="hidden" name="XHR_CORS_TRARGETORIGIN" value="'+window.location.href+'" />');b.submit(),c.$image_list.hide(),c.$progress_bar.show(),c.$progress_bar.find("span").css("width","100%").text("Please wait!")}}),this.buildDragUpload(d),d=a('<li class="drop-upload">').append(d)}var f=a('<input type="text" placeholder="http://example.com"/>').on("mouseup touchend keydown",a.proxy(function(a){a.stopPropagation()},this)),g=null;this.options.mediaManager&&(g=a('<button class="f-browse">').append('<i class="fa fa-search"></i>').on("click",a.proxy(function(){this.showMediaManager()},this)).find("i").click(a.proxy(function(){this.showMediaManager()},this)).end()),this.$image_list=a("<ul>").append(d).append(a('<li class="url-upload">').append('<label><span data-text="true">Enter URL</span>: </label>').append(f).append(g).append(a('<button class="f-ok" data-text="true">OK</button>').click(a.proxy(function(){this.writeImage(f.val(),!0)},this)))),this.$image_wrapper.append(a("<h4>").append('<span data-text="true">Insert image</span>').append(a('<i class="fa fa-times" title="Cancel">').click(a.proxy(function(){this.$bttn_wrapper.show(),this.hideImageWrapper(!0),this.restoreSelection(),this.options.inlineMode||this.imageMode?this.imageMode&&this.showImageEditor():this.hide()},this)))).append(this.$image_list).append(this.$progress_bar).click(function(a){a.stopPropagation()}).find("*").click(function(a){a.stopPropagation()}).end().hide()},b.prototype.writeVideo=function(a){this.$element.focus(),this.restoreSelection(),this.insertHTML(this.stripScript(a)),this.saveUndoStep(),this.$bttn_wrapper.show(),this.hideVideoWrapper(),this.hide(),this.callback("insertVideo",[a])},b.prototype.buildInsertVideo=function(){this.$video_wrapper=a("<div>").addClass("video-wrapper"),this.$popup_editor.append(this.$video_wrapper);var b=a('<textarea placeholder="Embeded code">').on("mouseup touchend keydown",a.proxy(function(a){a.stopPropagation()},this)),c=a("<p>").append(a('<button class="f-ok" data-text="true">OK</button>').click(a.proxy(function(){this.writeVideo(b.val())},this)));this.$video_wrapper.append(a("<h4>").append('<span data-text="true">Insert video</span>').append(a('<i class="fa fa-times" title="Cancel">').click(a.proxy(function(){this.$bttn_wrapper.show(),this.hideVideoWrapper(),this.restoreSelection(),this.options.inlineMode||this.hide()},this)))).append(b).append(c).click(function(a){a.stopPropagation()}).find("*").click(function(a){a.stopPropagation()}).end().hide()},b.prototype.buildCreateLink=function(){this.$link_wrapper=a("<div>").addClass("link-wrapper"),this.$popup_editor.append(this.$link_wrapper);var b=a('<input type="text">').attr("placeholder","http://www.example.com").on("mouseup touchend keydown",function(a){a.stopPropagation()}),c=a('<input type="checkbox" id="f-checkbox-'+this._id+'">').click(function(a){a.stopPropagation()}),d=a('<button class="f-ok" type="button" data-text="true">').text("OK").on("touchend",function(a){a.stopPropagation()}).click(a.proxy(function(){this.writeLink(b.val(),c.prop("checked"))},this)),e=a('<button class="f-ok f-unlink" data-text="true" type="button">').text("UNLINK").on("click touch",a.proxy(function(){this.link=!0,this.writeLink("",c.prop("checked"))},this));this.$link_wrapper.append(a("<h4>").append('<span data-text="true">Insert link</span>').append(a('<i class="fa fa-times" title="Cancel">').click(a.proxy(function(){this.$bttn_wrapper.show(),this.hideLinkWrapper(),this.options.inlineMode||this.imageMode?this.imageMode&&this.showImageEditor():this.hide(),this.restoreSelection()},this)))).append(b).append(a("<p>").append(c).append(' <label for="f-checkbox-'+this._id+'" data-text="true">Open in new tab</label>').append(d).append(e)).end().hide()},b.prototype.buildDragUpload=function(b){var c=this;b.on({dragover:function(){return a(this).addClass("f-hover"),!1},dragend:function(){return a(this).removeClass("f-hover"),!1},drop:function(b){a(this).removeClass("f-hover"),b.preventDefault(),c.uploadFile(b.originalEvent.dataTransfer.files)}})},b.prototype.hideImageLoader=function(){this.$progress_bar.hide(),this.$progress_bar.find("span").css("width","0%").text(""),this.$image_list.show()},b.prototype.writeImage=function(b,c){c&&(b=this.sanitizeURL(b));var d=new Image;return d.onerror=a.proxy(function(){this.hideImageLoader(),this.throwImageError(1)},this),this.imageMode?(d.onload=a.proxy(function(){this.$element.find(".f-img-editor > img").attr("src",b),this.hide(),this.hideImageLoader(),this.$image_editor.show(),this.saveUndoStep(),this.callback("replaceImage",[b])},this),d.src=b,!1):(d.onload=a.proxy(function(){this.$element.focus(),this.restoreSelection(),this.callback("imageLoaded",[b],!1);var c='<img alt="Image title" src="'+b+'" width="'+this.options.defaultImageWidth+'" style="min-width: 16px; min-height: 16px; margin-bottom: '+this.options.imageMargin+"px; margin-left: auto; margin-right: auto; margin-top: "+this.options.imageMargin+'px">',d=this.getSelectionElements()[0];this.getSelectionTextInfo(d).atStart&&d!=this.$element.get(0)?a(d).before("<p>"+c+"</p>"):this.insertHTML(c),this.$element.find("img").each(function(a,b){b.oncontrolselect=function(){return!1}}),this.hide(),this.hideImageLoader(),this.saveUndoStep(),this.callback("insertImage",[b])},this),void(d.src=b))},b.prototype.throwImageError=function(b){var c="Unknown image upload error.";1==b?c="Bad link.":2==b?c="No link in upload response.":3==b?c="Error during file upload.":4==b?c="Parsing response failed.":5==b?c="Image too large.":6==b?c="Invalid image type.":7==b&&(c="Image can be uploaded only to same domain in IE 9."),this.options.imageErrorCallback&&a.isFunction(this.options.imageErrorCallback)&&this.options.imageErrorCallback({errorCode:b,errorStatus:c}),this.hideImageLoader()},b.prototype.uploadFile=function(b){if(this.callback("beforeFileUpload",[b],!1)!==!0)return!1;if(void 0!==b&&b.length>0){var c;if(this.drag_support.formdata&&(c=this.drag_support.formdata?new FormData:null),c){var d;for(d in this.options.imageUploadParams)c.append(d,this.options.imageUploadParams[d]);
if(void 0!==this.options.imageUploadToS3){for(d in this.options.imageUploadToS3.params)c.append(d,this.options.imageUploadToS3.params[d]);c.append("success_action_status","201"),c.append("X-Requested-With","xhr"),c.append("Content-Type",b[0].type),c.append("key",this.options.imageUploadToS3.keyStart+(new Date).getTime()+"-"+b[0].name)}if(c.append(this.options.imageUploadParam,b[0]),b[0].size>this.options.maxImageSize)return this.throwImageError(5),!1;if(this.options.allowedImageTypes.indexOf(b[0].type.replace(/image\//g,""))<0)return this.throwImageError(6),!1}if(c){var e;this.options.crossDomain?e=this.createCORSRequest("POST",this.options.imageUploadURL):(e=new XMLHttpRequest,e.open("POST",this.options.imageUploadURL)),e.onload=a.proxy(function(){this.$progress_bar.find("span").css("width","100%").text("Please wait!");try{200==e.status?this.parseImageResponse(e.responseText):201==e.status?this.parseImageResponseXML(e.responseXML):this.throwImageError(3)}catch(a){this.throwImageError(4)}},this),e.onerror=a.proxy(function(){this.throwImageError(3)},this),e.upload.onprogress=a.proxy(function(a){if(a.lengthComputable){var b=a.loaded/a.total*100|0;this.$progress_bar.find("span").css("width",b+"%")}},this),e.send(c),this.$image_list.hide(),this.$progress_bar.show()}}},b.prototype.parseImageResponse=function(b){try{var c=a.parseJSON(b);c.link?this.writeImage(c.link):this.throwImageError(2)}catch(d){this.throwImageError(4)}},b.prototype.parseImageResponseXML=function(b){try{var c=a(b).find("Location").text(),d=a(b).find("Key").text();this.options.imageUploadToS3.callback.call(this,c,d),c?this.writeImage(c):this.throwImageError(2)}catch(e){this.throwImageError(4)}},b.prototype.createCORSRequest=function(a,b){var c=new XMLHttpRequest;return"withCredentials"in c?c.open(a,b,!0):"undefined"!=typeof XDomainRequest?(c=new XDomainRequest,c.open(a,b)):c=null,c},b.prototype.writeLink=function(b,c,d){this.options.noFollow&&(d=!0),this.options.alwaysBlank&&(c=!0);var e="",f="";if(d===!0&&(e='rel="nofollow"'),c===!0&&(f='target="_blank"'),b=this.sanitizeURL(b),this.imageMode)return""!==b?("A"!=this.$element.find(".f-img-editor").parent().get(0).tagName?this.$element.find(".f-img-editor").wrap('<a class="f-link" href="'+b+'" '+f+" "+e+"></a>"):(c===!0?this.$element.find(".f-img-editor").parent().attr("target","_blank"):this.$element.find(".f-img-editor").parent().removeAttr("target"),d===!0?this.$element.find(".f-img-editor").parent().attr("rel","nofollow"):this.$element.find(".f-img-editor").parent().removeAttr("rel"),this.$element.find(".f-img-editor").parent().attr("href",b)),this.callback("insertImageLink",[b])):("A"==this.$element.find(".f-img-editor").parent().get(0).tagName&&a(this.$element.find(".f-img-editor").get(0)).unwrap(),this.callback("removeImageLink")),this.saveUndoStep(),this.showImageEditor(),this.$element.find(".f-img-editor").find("img").click(),this.link=!1,!1;if(this.restoreSelection(),document.execCommand("unlink",!1,b),this.saveSelectionByMarkers(),this.$element.find("span.f-link").each(function(b,c){a(c).replaceWith(a(c).html())}),this.restoreSelectionByMarkers(),""!==b){document.execCommand("createLink",!1,b);for(var g=this.getSelectionLinks(),h=0;h<g.length;h++)c===!0&&a(g[h]).attr("target","_blank"),d===!0&&a(g[h]).attr("rel","nofollow"),a(g[h]).addClass("f-link");this.$element.find("a:empty").remove(),this.callback("insertLink",[b])}else this.$element.find("a:empty").remove(),this.callback("removeLink");this.saveUndoStep(),this.hideLinkWrapper(),this.$bttn_wrapper.show(),this.options.inlineMode||this.hide(),this.link=!1},b.prototype.getSelectionLinks=function(){var a,b,c,d,e=[];if(window.getSelection){var f=window.getSelection();if(f.getRangeAt&&f.rangeCount){d=document.createRange();for(var g=0;g<f.rangeCount;++g)if(a=f.getRangeAt(g),b=a.commonAncestorContainer,1!=b.nodeType&&(b=b.parentNode),"a"==b.nodeName.toLowerCase())e.push(b);else{c=b.getElementsByTagName("a");for(var h=0;h<c.length;++h)d.selectNodeContents(c[h]),d.compareBoundaryPoints(a.END_TO_START,a)<1&&d.compareBoundaryPoints(a.START_TO_END,a)>-1&&e.push(c[h])}d.detach()}}else if(document.selection&&"Control"!=document.selection.type)if(a=document.selection.createRange(),b=a.parentElement(),"a"==b.nodeName.toLowerCase())e.push(b);else{c=b.getElementsByTagName("a"),d=document.body.createTextRange();for(var i=0;i<c.length;++i)d.moveToElementText(c[i]),d.compareEndPoints("StartToEnd",a)>-1&&d.compareEndPoints("EndToStart",a)<1&&e.push(c[i])}return e},b.prototype.isEnabled=function(b){return a.inArray(b,this.options.buttons)>=0},b.prototype.show=function(b){if(void 0!==b){if(this.options.inlineMode)if(null!==b&&"touchend"!==b.type){var c=b.pageX,d=b.pageY;c<this.$element.offset().left&&(c=this.$element.offset().left),c>this.$element.offset().left+this.$element.width()&&(c=this.$element.offset().left+this.$element.width()),d<this.$element.offset.top&&(d=this.$element.offset().top),d>this.$element.offset().top+this.$element.height()&&(d=this.$element.offset().top+this.$element.height()),20>c&&(c=20),0>d&&(d=0),c+this.$editor.width()>a(window).width()-50?(this.$editor.addClass("right-side"),c=a(window).width()-(c+30),this.$editor.css("top",d+20),this.$editor.css("right",c),this.$editor.css("left","auto")):(this.$editor.removeClass("right-side"),this.$editor.css("top",d+20),this.$editor.css("left",c-20),this.$editor.css("right","auto")),a(".froala-editor:not(.f-basic)").hide(),this.$editor.show()}else a(".froala-editor:not(.f-basic)").hide(),this.$editor.show(),this.repositionEditor();this.hideLinkWrapper(),this.hideVideoWrapper(),this.hideImageWrapper(),this.$bttn_wrapper.show(),this.$bttn_wrapper.find(".fr-dropdown").removeClass("active"),this.refreshButtons(),this.imageMode=!1}},b.prototype.showByCoordinates=function(b,c){b-=20,c+=15,b+this.$popup_editor.width()>a(window).width()-50?(this.$popup_editor.addClass("right-side"),b=a(window).width()-(b+40),this.$popup_editor.css("top",c),this.$popup_editor.css("right",b),this.$popup_editor.css("left","auto")):(this.$popup_editor.removeClass("right-side"),this.$popup_editor.css("top",c),this.$popup_editor.css("left",b),this.$popup_editor.css("right","auto")),this.$popup_editor.show()},b.prototype.showLinkWrapper=function(){this.$link_wrapper&&(this.$link_wrapper.show(),setTimeout(a.proxy(function(){this.$link_wrapper.find('input[type="text"]').focus().select()},this),0))},b.prototype.hideLinkWrapper=function(){this.$link_wrapper&&this.$link_wrapper.hide()},b.prototype.showImageWrapper=function(){this.$image_wrapper&&this.$image_wrapper.show()},b.prototype.showVideoWrapper=function(){this.$video_wrapper&&this.$video_wrapper.show()},b.prototype.hideImageWrapper=function(a){this.$image_wrapper&&(this.$element.attr("data-resize")||a||this.closeImageMode(),this.$image_wrapper.hide())},b.prototype.hideVideoWrapper=function(){this.$video_wrapper&&this.$video_wrapper.hide()},b.prototype.showInsertLink=function(){this.options.inlineMode&&this.$bttn_wrapper.hide(),this.showLinkWrapper(),this.hideImageWrapper(!0),this.hideVideoWrapper(),this.$image_editor.hide(),this.link=!0},b.prototype.showInsertImage=function(){this.options.inlineMode&&this.$bttn_wrapper.hide(),this.hideLinkWrapper(),this.hideVideoWrapper(),this.showImageWrapper(),this.$image_editor.hide()},b.prototype.showInsertVideo=function(){this.options.inlineMode&&this.$bttn_wrapper.hide(),this.hideLinkWrapper(),this.hideImageWrapper(),this.showVideoWrapper(),this.$image_editor.hide()},b.prototype.showImageEditor=function(){this.options.inlineMode&&this.$bttn_wrapper.hide(),this.hideLinkWrapper(),this.hideImageWrapper(!0),this.hideVideoWrapper(),this.$image_editor.show(),this.options.imageMove||this.$element.attr("contenteditable",!1)},b.prototype.hideOtherEditors=function(){for(var c=1;c<=b.count;c++)c!=this._id&&a(window).trigger("hide."+c)},b.prototype.hide=function(a){void 0===a&&(a=!0),a?this.hideOtherEditors():(this.closeImageMode(),this.imageMode=!1),this.$popup_editor.hide(),this.hideLinkWrapper(),this.hideImageWrapper(),this.hideVideoWrapper(),this.$image_editor.hide(),this.link=!1},b.prototype.positionPopup=function(b){a(this.$editor.find('button.fr-bttn[data-cmd="'+b+'"]')).length&&(this.$popup_editor.css("top",this.$editor.find('button.fr-bttn[data-cmd="'+b+'"]').offset().top+30),this.$popup_editor.css("left",this.$editor.find('button.fr-bttn[data-cmd="'+b+'"]').offset().left),this.$popup_editor.show())},b.prototype.bindButtonEvents=function(){this.bindDropdownEvents(),this.bindCommandEvents(this.$bttn_wrapper.find("[data-cmd]"))},b.prototype.bindDropdownEvents=function(){var b=this;this.$bttn_wrapper.find(".fr-dropdown").on("click touchend",function(c){return c.stopPropagation(),c.preventDefault(),b.options.inlineMode===!1&&b.hide(),a(this).attr("data-disabled")?!1:(a(".fr-dropdown").not(this).removeClass("active"),void a(this).toggleClass("active"))}),a(window).on("click touchend",a.proxy(function(){this.$editor.find(".fr-dropdown").removeClass("active")},this)),this.$element.on("click touchend","img, a",a.proxy(function(){this.$editor.find(".fr-dropdown").removeClass("active")},this));var c=this.$bttn_wrapper.find(".fr-selector button.fr-bttn");c.bind("select",function(){a(this).parents(".fr-selector").find(" > button > i").attr("class",a(this).find("i").attr("class"))}).on("click touch",function(){a(this).parents("ul").find("button").removeClass("active"),a(this).parents(".fr-selector").removeClass("active").trigger("mouseout"),a(this).trigger("select")})},b.prototype.bindCommandEvents=function(b){b.on("touchmove",function(){a(this).data("dragging",!0)}),b.on("click touchend",a.proxy(function(b){b.stopPropagation(),b.preventDefault();var c=b.currentTarget;if(a(c).data("dragging"))return a(c).removeData("dragging"),!1;var d=a(c).data("cmd"),e=a(c).data("val");a(c).parents(".fr-dropdown").removeClass("active"),this.exec(d,e),this.$bttn_wrapper.find(".fr-dropdown").removeClass("active")},this))},b.prototype._startInDefault=function(a){this.$element.focus(),this.$bttn_wrapper.find('[data-cmd="'+a+'"]').toggleClass("active"),document.execCommand(a)},b.prototype._startInFontExec=function(a,b,c){this.$element.focus(),this.insertHTML('<span data-inserted="true" data-font="'+b+'" style="'+a+": "+c+'"></span>',!0);var d=this.$element.find("[data-inserted]");d.removeAttr("data-inserted"),this.setSelection(d.get(0),0,null,0)},b.prototype.exec=function(a,b){if(!this.selectionInEditor()&&"html"!==a&&"undo"!==a&&"redo"!==a&&"selectAll"!==a&&"save"!=a&&"insertImage"!==a&&"insertVideo"!==a)return!1;if(this.selectionInEditor()){if(""===this.text()){if("bold"===a||"italic"===a||"underline"===a||"strikeThrough"==a)return this._startInDefault(a),!1;if("fontSize"==a)return this._startInFontExec("font-size",a,b),!1;if("fontFamily"==a)return this._startInFontExec("font-family",a,b),!1}if(""===this.text()&&"insertHorizontalRule"!=a&&"fontSize"!==a&&"formatBlock"!==a&&"blockStyle"!==a&&"indent"!==a&&"outdent"!==a&&"justifyLeft"!==a&&"justifyRight"!==a&&"justifyFull"!==a&&"justifyCenter"!==a&&"html"!==a&&"undo"!==a&&"redo"!==a&&"selectAll"!==a&&"save"!==a&&"insertImage"!==a&&"insertVideo"!==a&&"insertOrderedList"!==a&&"insertUnorderedList"!==a)return!1}switch(a){case"fontSize":this.fontExec("font-size",a,b);break;case"fontFamily":this.fontExec("font-family",a,b);break;case"backColor":this.backColor(b);break;case"foreColor":this.foreColor(b);break;case"formatBlock":this.formatBlock(b);break;case"blockStyle":this.blockStyle(b);break;case"createLink":this.insertLink();break;case"insertImage":this.insertImage();break;case"indent":this.indent();break;case"outdent":this.outdent(!0);break;case"justifyLeft":case"justifyRight":case"justifyCenter":case"justifyFull":this.align(a);break;case"insertOrderedList":case"insertUnorderedList":this.formatList(a);break;case"insertVideo":this.insertVideo();break;case"indent":case"outdent":this.execDefault(a,b),this.repositionEditor();break;case"undo":this.undo();break;case"redo":this.redo();break;case"html":this.html();break;case"save":this.save();break;case"selectAll":this.$element.focus(),this.execDefault(a,b);break;case"insertHorizontalRule":this.execDefault(a,b),this.hide();break;default:this.execDefault(a,b)}"undo"!=a&&"redo"!=a&&"selectAll"!=a&&"createLink"!=a&&"insertImage"!=a&&"html"!=a&&"insertVideo"!=a&&this.saveUndoStep(),"createLink"!=a&&"insertImage"!=a&&this.refreshButtons()},b.prototype.removeFormat=function(){document.execCommand("removeFormat",!1,!1),document.execCommand("unlink",!1,!1)},b.prototype.undo=function(){if(this.undoIndex>1){var a=this.getHTML(),b=this.undoStack[--this.undoIndex-1];this.$element.html(b),this.$element.focus(),this.restoreSelectionByMarkers(),this.hide(),this.callback("undo",[this.$element.html(),a])}this.refreshUndoRedo()},b.prototype.redo=function(){if(this.undoIndex<this.undoStack.length){var a=this.$element.html(),b=this.undoStack[this.undoIndex++];this.$element.html(b),this.$element.focus(),this.restoreSelectionByMarkers(),this.hide(),this.callback("redo",[this.$element.html(),a])}this.refreshUndoRedo()},b.prototype.save=function(){return this.callback("beforeSave",[],!1)!==!0?!1:void(this.options.saveURL?a.post(this.options.saveURL,a.extend({body:this.getHTML()},this.options.saveParams),a.proxy(function(a){this.callback("afterSave",[a])},this)).fail(a.proxy(function(){this.callback("saveError",["Save request failed on the server."])},this)):this.callback("saveError",["Missing save URL."]))},b.prototype.sanitizeURL=function(a){return this.options.enableScript?a:/^https?:\/\//.test(a)?String(a).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;"):encodeURIComponent(a).replace("%23","#").replace("%2F","/")},b.prototype.html=function(){var a;this.isHTML?(this.isHTML=!1,a=this.options.enableScript?this.$html_area.val():this.stripScript(this.$html_area.val()),a=this.clean(a,!0,!1),this.$element.html(a).attr("contenteditable",!0),this.$box.removeClass("f-html"),this.$editor.find('.fr-bttn:not([data-cmd="html"])').prop("disabled",!1),this.$editor.find("div.fr-bttn").removeAttr("data-disabled"),this.$editor.find('.fr-bttn[data-cmd="html"]').removeClass("active"),this.saveUndoStep(),this.options.pragraphy&&this.wrapText(),this.refreshButtons(),this.callback("htmlHide",[a]),this.focus()):(a=this.options.inlineMode?"\n\n"+this.getHTML():html_beautify(this.getHTML()),this.$html_area.val(a).trigger("resize"),this.options.inlineMode&&this.$box.find(".html-switch").css("top",this.$box.css("padding-top")),this.$html_area.css("height",this.$element.height()+20),this.$element.html(this.$html_area).removeAttr("contenteditable"),this.$box.addClass("f-html"),this.$editor.find('button.fr-bttn:not([data-cmd="html"])').prop("disabled",!0),this.$editor.find("div.fr-bttn").attr("data-disabled",!0),this.$editor.find('.fr-bttn[data-cmd="html"]').addClass("active"),this.options.inlineMode&&this.hide(),this.isHTML=!0,this.callback("htmlShow",[a]))},b.prototype.beautifyFont=function(b){for(var c=!0,d=a.proxy(function(){this.$element.find('span[data-font="'+b+'"] + span[data-font="'+b+'"]').each(function(d,e){a(e).css(b)==a(e).prev().css(b)&&(a(e).prepend(a(e).prev().html()),a(e).prev().remove(),c=!0)}),this.$element.find('span[data-font="'+b+'"] + span#marker-true + span[data-font="'+b+'"], span[data-font="'+b+'"] + span#marker-false + span[data-font="'+b+'"]').each(function(d,e){a(e).css(b)==a(e).prev().prev().css(b)&&(a(e).prepend(a(e).prev().clone()),a(e).prepend(a(e).prev().prev().html()),a(e).prev().prev().remove(),a(e).prev().remove(),c=!0)})},this);c;)c=!1,d()},b.prototype.fontExec=function(b,c,d){document.execCommand("fontSize",!1,1),this.saveSelectionByMarkers();var e=[];this.$element.find("font").each(function(c,f){var g=a("<span>").attr("data-font",b).css(b,d).html(a(f).html());0===a(f).parents("font").length&&e.push(g),a(f).replaceWith(g)});var f=function(c,d){a(d).css(b,"")};for(var g in e){var h=e[g];a(h).find("*").each(f)}this.$element.find('span[data-font="'+b+'"] > span[data-font="'+b+'"]').each(function(c,d){a(d).attr("style")&&(a(d).before('<span class="close-span"></span>'),a(d).after('<span data-font="'+b+'" style="'+b+": "+a(d).parent().css(b)+';" data-open="true"></span>'))});var i=this.$element.html();i=i.replace(new RegExp('<span class="close-span"></span>',"g"),"</span>"),i=i.replace(new RegExp('data-open="true"></span>',"g"),">"),this.$element.html(i),this.beautifyFont(b),this.$element.find('span[style=""]').each(function(b,c){a(c).replaceWith(a(c).html())}),this.$element.find('span[data-font="'+b+'"]').each(function(c,d){a(d).css(b)==a(d).parent().css(b)&&a(d).replaceWith(a(d).html())}),this.$element.find('span[data-font="'+b+'"]').each(function(b,c){""===a(c).text()&&a(c).replaceWith(a(c).html())}),this.beautifyFont(b),this.restoreSelectionByMarkers(),this.repositionEditor(),this.callback(c,[d])},b.prototype.backColor=function(c){var d="backColor";this.browser.msie||(d="hiliteColor");var e=a(this.getSelectionElement()).css("background-color");document.execCommand(d,!1,c);var f=this.$editor.find('button.fr-color-bttn[data-cmd="backColor"][data-val="'+c+'"]');f.addClass("active"),f.siblings().removeClass("active"),this.callback("backColor",[b.hexToRGBString(c),e])},b.prototype.foreColor=function(c){var d=a(this.getSelectionElement()).css("color");document.execCommand("foreColor",!1,c),this.saveSelectionByMarkers(),this.$element.find("font[color]").each(function(b,d){a(d).replaceWith(a("<span>").css("color",c).html(a(d).html()))}),this.restoreSelectionByMarkers();var e=this.$editor.find('button.fr-color-bttn[data-cmd="foreColor"][data-val="'+c+'"]');e.addClass("active"),e.siblings().removeClass("active"),this.callback("foreColor",[b.hexToRGBString(c),d])},b.prototype.formatBlock=function(b,c){if(this.disabledList.indexOf("formatBlock")>=0)return!1;this.saveSelectionByMarkers(),this.wrapText(),this.restoreSelectionByMarkers();var d=this.getSelectionElements();this.saveSelectionByMarkers();var e;for(var f in d){var g=a(d[f]);if(!this.fakeEmpty(g))if(e="n"==b?this.options.paragraphy?a("<div>").html(g.html()):g.html()+"<br/>":a("<"+b+">").html(g.html()),g.get(0)!=this.$element.get(0)&&"LI"!=g.get(0).tagName){var h=g.prop("attributes");if(e.attr)for(var i in h)"class"!==h[i].name&&e.attr(h[i].name,h[i].value);var j=this.options.blockStyles[b];void 0===j&&(j=this.options.defaultBlockStyle);try{if(void 0!==g.attr("class")&&void 0!==j)for(var k=g.attr("class").split(" "),l=0;l<k.length;l++){var m=k[l];void 0!==j[m]&&void 0===c&&e.addClass(m)}else e.addClass(g.attr("class"));"*"!=c&&e.addClass(c)}catch(n){}g.replaceWith(e)}else g.html(e)}this.unwrapText(),this.restoreSelectionByMarkers(),this.hide(),this.callback("formatBlock")},b.prototype.blockStyle=function(a){var b=this.activeBlockTag();this.formatBlock(b,a)},b.prototype.formatList=function(b){this.saveSelectionByMarkers();var c,d=this.getSelectionElements(),e=!0,f=!1;for(var g in d)if(c=a(d[g]),c.parents("li").length>0||"LI"==c.get(0).tagName){var h;h="LI"==c.get(0).tagName?c:c.parents("li"),c.parents("ol").length>0?(h.before('<span class="close-ol"></span>'),h.after('<span class="open-ol"></span>')):c.parents("ul").length>0&&(h.before('<span class="close-ul"></span>'),h.after('<span class="open-ul"></span>')),h.replaceWith(h.contents()),f=!0}else e=!1;if(f){var i=this.$element.html();i=i.replace(new RegExp('<span class="close-ul"></span>',"g"),"</ul>"),i=i.replace(new RegExp('<span class="open-ul"></span>',"g"),"<ul>"),i=i.replace(new RegExp('<span class="close-ol"></span>',"g"),"</ol>"),i=i.replace(new RegExp('<span class="open-ol"></span>',"g"),"<ol>"),this.$element.html(i),this.$element.find("ul:empty, ol:empty").remove()}if(this.clearSelection(),e===!1){this.wrapText(),this.restoreSelectionByMarkers(),d=this.getSelectionElements(),this.saveSelectionByMarkers();var j=a("<ol>");"insertUnorderedList"==b&&(j=a("<ul>"));for(var k in d)c=a(d[k]),c.get(0)!=this.$element.get(0)&&(j.append(a("<li>").append(c.clone())),k!=d.length-1?c.remove():(c.replaceWith(j),j.find("li")));this.unwrapText()}this.restoreSelectionByMarkers(),this.repositionEditor(),this.callback(b)},b.prototype.align=function(b){var c=this.getSelectionElements();"justifyLeft"==b?b="left":"justifyRight"==b?b="right":"justifyCenter"==b?b="center":"justifyFull"==b&&(b="justify");for(var d in c)a(c[d]).css("text-align",b);this.repositionEditor(),this.callback("align",[b])},b.prototype.indent=function(b){var c=20;b&&(c=-20),this.saveSelectionByMarkers(),this.wrapText(),this.restoreSelectionByMarkers();var d=this.getSelectionElements();this.saveSelectionByMarkers();for(var e in d){var f=a(d[e]);if(f.parentsUntil(this.$element,"li").length>0&&(f=f.parentsUntil(this.$element,"li")),f.get(0)!=this.$element.get(0)){var g=parseInt(f.css("margin-left").replace(/px/,""),10),h=Math.max(0,g+c);f.css("marginLeft",h),"LI"===f.get(0).tagName&&(h%60===0?0===f.parents("ol").length?f.css("list-style-type","disc"):f.css("list-style-type","decimal"):h%40===0?0===f.parents("ol").length?f.css("list-style-type","square"):f.css("list-style-type","lower-latin"):0===f.parents("ol").length?f.css("list-style-type","circle"):f.css("list-style-type","lower-roman"))}else{var i=a("<div>").html(f.html());f.html(i),i.css("marginLeft",Math.max(0,c))}}this.unwrapText(),this.restoreSelectionByMarkers(),this.repositionEditor(),b||this.callback("indent")},b.prototype.outdent=function(){this.indent(!0),this.callback("outdent")},b.prototype.insertLink=function(){this.showInsertLink(),this.options.inlineMode||this.positionPopup("createLink"),this.saveSelection();var b=this.getSelectionLink(),c=this.getSelectionLinks();c.length>0?this.$link_wrapper.find('input[type="checkbox"]').prop("checked","_blank"==a(c[0]).attr("target")):this.$link_wrapper.find('input[type="checkbox"]').prop("checked",this.options.alwaysBlank),this.$link_wrapper.find('input[type="text"]').val(b||"http://")},b.prototype.insertImage=function(){this.showInsertImage(),this.saveSelection(),this.options.inlineMode||this.positionPopup("insertImage"),this.$image_wrapper.find('input[type="text"]').val("")},b.prototype.insertVideo=function(){this.showInsertVideo(),this.saveSelection(),this.options.inlineMode||this.positionPopup("insertVideo"),this.$video_wrapper.find("textarea").val("")},b.prototype.execDefault=function(a,b){document.execCommand(a,!1,b),"insertOrderedList"==a?this.$bttn_wrapper.find('[data-cmd="insertUnorderedList"]').removeClass("active"):"insertUnorderedList"==a&&this.$bttn_wrapper.find('[data-cmd="insertOrderedList"]').removeClass("active"),this.callback(a)},b.prototype.isActive=function(a,b){switch(a){case"fontFamily":return this._isActiveFontFamily(b);case"fontSize":return this._isActiveFontSize(b);case"backColor":return this._isActiveBackColor(b);case"foreColor":return this._isActiveForeColor(b);case"formatBlock":return this._isActiveFormatBlock(b);case"blockStyle":return this._isActiveBlockStyle(b);case"createLink":case"insertImage":return!1;case"justifyLeft":case"justifyRight":case"justifyCenter":case"justifyFull":return this._isActiveAlign(a);case"html":return this._isActiveHTML();case"undo":case"redo":case"save":return!1;default:return this._isActiveDefault(a)}},b.prototype._isActiveFontFamily=function(b){var c=this.getSelectionElement();return a(c).css("fontFamily").replace(/ /g,"")===b.replace(/ /g,"")?!0:!1},b.prototype._isActiveFontSize=function(b){var c=this.getSelectionElement();return a(c).css("fontSize")===b?!0:!1},b.prototype._isActiveBackColor=function(b){for(var c=this.getSelectionElement();a(c).get(0)!=this.$element.get(0);){if(a(c).css("background-color")===b)return!0;if("transparent"!=a(c).css("background-color")&&"rgba(0, 0, 0, 0)"!=a(c).css("background-color"))return!1;c=a(c).parent()}return!1},b.prototype._isActiveForeColor=function(a){return document.queryCommandValue("foreColor")===a?!0:!1},b.prototype._isActiveFormatBlock=function(b){"CODE"===b.toUpperCase()?b="PRE":"N"===b.toUpperCase()&&(b="DIV");for(var c=a(this.getSelectionElement());c.get(0)!=this.$element.get(0);){if(c.get(0).tagName==b.toUpperCase())return!0;c=c.parent()}return!1},b.prototype._isActiveBlockStyle=function(b){for(var c=a(this.getSelectionElement());c.get(0)!=this.$element.get(0);){if(c.hasClass(b))return!0;c=c.parent()}return!1},b.prototype._isActiveAlign=function(b){var c=this.getSelectionElements();return"justifyLeft"==b?b="left":"justifyRight"==b?b="right":"justifyCenter"==b?b="center":"justifyFull"==b&&(b="justify"),b==a(c[0]).css("text-align")?!0:!1},b.prototype._isActiveHTML=function(){return this.isHTML?!0:!1},b.prototype._isActiveDefault=function(a){try{if(document.queryCommandState(a)===!0)return!0}catch(b){}return!1},b.prototype.activeBlockTag=function(){return a('.active[data-cmd="formatBlock"]').data("val")},b.prototype.updateBlockStyles=function(){var b=this.activeBlockTag();this.$bttn_wrapper.find(".fr-block-style").empty(),this.$bttn_wrapper.find(".fr-block-style").append(a('<li data-cmd="blockStyle" data-val="*">').append(a('<a href="#" data-text="true">').text("Default")));var c=this.options.blockStyles[b];if(void 0===c&&(c=this.options.defaultBlockStyle),void 0!==c)for(var d in c){var e=c[d];this.$bttn_wrapper.find(".fr-block-style").append(a("<li>").append(a('<a href="#" data-text="true">').text(e).addClass(d)).attr("data-cmd","blockStyle").attr("data-val",d))}this.bindCommandEvents(this.$bttn_wrapper.find(".fr-block-style [data-cmd]"))},b.prototype.refreshButtons=function(){return!this.selectionInEditor()||this.isHTML?!1:(this.refreshUndoRedo(),this.$bttn_wrapper.find('[data-cmd="formatBlock"]').each(a.proxy(function(a,b){this.refreshFormatBlock(b)},this)),this.updateBlockStyles(),this.$bttn_wrapper.find("[data-cmd]").not('[data-cmd="formatBlock"]').each(a.proxy(function(b,c){switch(a(c).data("cmd")){case"fontSize":this.refreshFontSize(c);break;case"fontFamily":this.refreshFontFamily(c);break;case"backColor":this.refreshBackColor(c);break;case"foreColor":this.refreshForeColor(c);break;case"formatBlock":this.refreshFormatBlock(c);break;case"blockStyle":this.refreshBlockStyle(c);break;case"createLink":case"insertImage":break;case"justifyLeft":case"justifyRight":case"justifyCenter":case"justifyFull":this.refreshAlign(c);break;case"html":this.isActive("html")?a(c).addClass("active"):a(c).removeClass("active");break;case"undo":case"redo":case"save":break;default:this.refreshDefault(c)}},this)),void(0===this.$bttn_wrapper.find(".fr-block-style .active").length&&this.$bttn_wrapper.find(".fr-block-style li:first").addClass("active")))},b.prototype.refreshBlockStyle=function(b){this.disabledList.indexOf("blockStyle")>=0&&a(b).parents(".fr-dropdown").attr("data-disabled",!0),a(b).removeClass("active"),this.isActive(a(b).data("cmd"),a(b).data("val"))&&a(b).addClass("active")},b.prototype.refreshFormatBlock=function(b){this.disabledList.indexOf("formatBlock")>=0&&a(b).parents(".fr-dropdown").attr("data-disabled",!0),a(b).removeClass("active"),this.isActive(a(b).data("cmd"),a(b).data("val"))&&a(b).addClass("active")},b.prototype.refreshUndoRedo=function(){if(this.isEnabled("undo")||this.isEnabled("redo")){if(void 0===this.$editor)return;this.$bttn_wrapper.find('[data-cmd="undo"], [data-cmd="redo"]').prop("disabled",!1),(0===this.undoStack.length||this.undoIndex<=1||this.isHTML)&&this.$bttn_wrapper.find('[data-cmd="undo"]').prop("disabled",!0),(this.undoIndex==this.undoStack.length||this.isHTML)&&this.$bttn_wrapper.find('[data-cmd="redo"]').prop("disabled",!0)}},b.prototype.refreshDefault=function(b){a(b).removeClass("active"),this.isActive(a(b).data("cmd"))&&a(b).addClass("active")},b.prototype.refreshAlign=function(b){var c=a(b).data("cmd");this.isActive(c)&&(a(b).parents("ul").find(".fr-bttn").removeClass("active"),a(b).addClass("active"),a(b).parents(".fr-dropdown").find(".fr-trigger").html(a(b).html()))},b.prototype.refreshForeColor=function(b){a(b).removeClass("active"),this.isActive("foreColor",b.style.backgroundColor)&&a(b).addClass("active")},b.prototype.refreshBackColor=function(b){a(b).removeClass("active"),this.isActive("backColor",b.style.backgroundColor)&&a(b).addClass("active")},b.prototype.refreshFontSize=function(b){a(b).removeClass("active"),this.isActive("fontSize",a(b).data("val"))&&a(b).addClass("active")},b.prototype.refreshFontFamily=function(b){a(b).removeClass("active"),this.isActive("fontFamily",a(b).data("val"))&&a(b).addClass("active")},b.prototype.option=function(b,c){if(void 0===b)return this.options;if(b instanceof Object)this.options=a.extend({},this.options,b),this.initOptions(),this.setCustomText(),this.setLanguage();else{if(void 0===c)return this.options[b];switch(this.options[b]=c,b){case"borderColor":this.setBorderColor();break;case"direction":this.setDirection();break;case"height":case"width":case"minHeight":this.setDimensions();break;case"spellcheck":this.setSpellcheck();break;case"placeholder":this.setPlaceholder();break;case"customText":this.setCustomText();break;case"inverseSkin":this.setInverseSkin();break;case"language":this.setLanguage();break;case"textNearImage":this.setTextNearImage();break;case"zIndex":this.setZIndex()}}};var c=a.fn.editable;a.fn.editable=function(c){for(var d=[],e=0;e<arguments.length;e++)d.push(arguments[e]);if(a("html").data("editable",!0),"string"==typeof c){var f=[];return this.each(function(){var b=a(this),e=b.data("fa.editable"),g=e[c].apply(e,d.slice(1));f.push(void 0===g?this:g)}),f}return this.each(function(){var d=a(this),e=d.data("fa.editable");e||d.data("fa.editable",e=new b(this,c))})},a.fn.editable.Constructor=b,a.Editable=b,a.fn.editable.noConflict=function(){return a.fn.editable=c,this}}(window.jQuery),function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a,b){function c(a){function b(){d?(c(),M(b),e=!0,d=!1):e=!1}var c=a,d=!1,e=!1;this.kick=function(){d=!0,e||b()},this.end=function(a){var b=c;a&&(e?(c=d?function(){b(),a()}:a,d=!0):a())}}function d(){return!0}function e(){return!1}function f(a){a.preventDefault()}function g(a){N[a.target.tagName.toLowerCase()]||a.preventDefault()}function h(a){return 1===a.which&&!a.ctrlKey&&!a.altKey}function i(a,b){var c,d;if(a.identifiedTouch)return a.identifiedTouch(b);for(c=-1,d=a.length;++c<d;)if(a[c].identifier===b)return a[c]}function j(a,b){var c=i(a.changedTouches,b.identifier);if(c&&(c.pageX!==b.pageX||c.pageY!==b.pageY))return c}function k(a){var b;h(a)&&(b={target:a.target,startX:a.pageX,startY:a.pageY,timeStamp:a.timeStamp},J(document,O.move,l,b),J(document,O.cancel,m,b))}function l(a){var b=a.data;s(a,b,a,n)}function m(){n()}function n(){K(document,O.move,l),K(document,O.cancel,m)}function o(a){var b,c;N[a.target.tagName.toLowerCase()]||(b=a.changedTouches[0],c={target:b.target,startX:b.pageX,startY:b.pageY,timeStamp:a.timeStamp,identifier:b.identifier},J(document,P.move+"."+b.identifier,p,c),J(document,P.cancel+"."+b.identifier,q,c))}function p(a){var b=a.data,c=j(a,b);c&&s(a,b,c,r)}function q(a){var b=a.data,c=i(a.changedTouches,b.identifier);c&&r(b.identifier)}function r(a){K(document,"."+a,p),K(document,"."+a,q)}function s(a,b,c,d){var e=c.pageX-b.startX,f=c.pageY-b.startY;I*I>e*e+f*f||v(a,b,c,e,f,d)}function t(){return this._handled=d,!1}function u(a){try{a._handled()}catch(b){return!1}}function v(a,b,c,d,e,f){{var g,h;b.target}g=a.targetTouches,h=a.timeStamp-b.timeStamp,b.type="movestart",b.distX=d,b.distY=e,b.deltaX=d,b.deltaY=e,b.pageX=c.pageX,b.pageY=c.pageY,b.velocityX=d/h,b.velocityY=e/h,b.targetTouches=g,b.finger=g?g.length:1,b._handled=t,b._preventTouchmoveDefault=function(){a.preventDefault()},L(b.target,b),f(b.identifier)}function w(a){var b=a.data.timer;a.data.touch=a,a.data.timeStamp=a.timeStamp,b.kick()}function x(a){var b=a.data.event,c=a.data.timer;y(),D(b,c,function(){setTimeout(function(){K(b.target,"click",e)},0)})}function y(){K(document,O.move,w),K(document,O.end,x)}function z(a){var b=a.data.event,c=a.data.timer,d=j(a,b);d&&(a.preventDefault(),b.targetTouches=a.targetTouches,a.data.touch=d,a.data.timeStamp=a.timeStamp,c.kick())
}function A(a){var b=a.data.event,c=a.data.timer,d=i(a.changedTouches,b.identifier);d&&(B(b),D(b,c))}function B(a){K(document,"."+a.identifier,z),K(document,"."+a.identifier,A)}function C(a,b,c){var d=c-a.timeStamp;a.type="move",a.distX=b.pageX-a.startX,a.distY=b.pageY-a.startY,a.deltaX=b.pageX-a.pageX,a.deltaY=b.pageY-a.pageY,a.velocityX=.3*a.velocityX+.7*a.deltaX/d,a.velocityY=.3*a.velocityY+.7*a.deltaY/d,a.pageX=b.pageX,a.pageY=b.pageY}function D(a,b,c){b.end(function(){return a.type="moveend",L(a.target,a),c&&c()})}function E(){return J(this,"movestart.move",u),!0}function F(){return K(this,"dragstart drag",f),K(this,"mousedown touchstart",g),K(this,"movestart",u),!0}function G(a){"move"!==a.namespace&&"moveend"!==a.namespace&&(J(this,"dragstart."+a.guid+" drag."+a.guid,f,b,a.selector),J(this,"mousedown."+a.guid,g,b,a.selector))}function H(a){"move"!==a.namespace&&"moveend"!==a.namespace&&(K(this,"dragstart."+a.guid+" drag."+a.guid),K(this,"mousedown."+a.guid))}var I=6,J=a.event.add,K=a.event.remove,L=function(b,c,d){a.event.trigger(c,d,b)},M=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(a){return window.setTimeout(function(){a()},25)}}(),N={textarea:!0,input:!0,select:!0,button:!0},O={move:"mousemove",cancel:"mouseup dragstart",end:"mouseup"},P={move:"touchmove",cancel:"touchend",end:"touchend"};a.event.special.movestart={setup:E,teardown:F,add:G,remove:H,_default:function(a){function d(){C(f,g.touch,g.timeStamp),L(a.target,f)}var f,g;a._handled()&&(f={target:a.target,startX:a.startX,startY:a.startY,pageX:a.pageX,pageY:a.pageY,distX:a.distX,distY:a.distY,deltaX:a.deltaX,deltaY:a.deltaY,velocityX:a.velocityX,velocityY:a.velocityY,timeStamp:a.timeStamp,identifier:a.identifier,targetTouches:a.targetTouches,finger:a.finger},g={event:f,timer:new c(d),touch:b,timeStamp:b},a.identifier===b?(J(a.target,"click",e),J(document,O.move,w,g),J(document,O.end,x,g)):(a._preventTouchmoveDefault(),J(document,P.move+"."+a.identifier,z,g),J(document,P.end+"."+a.identifier,A,g)))}},a.event.special.move={setup:function(){J(this,"movestart.move",a.noop)},teardown:function(){K(this,"movestart.move",a.noop)}},a.event.special.moveend={setup:function(){J(this,"movestart.moveend",a.noop)},teardown:function(){K(this,"movestart.moveend",a.noop)}},J(document,"mousedown.move",k),J(document,"touchstart.move",o),"function"==typeof Array.prototype.indexOf&&!function(a){for(var b=["changedTouches","targetTouches"],c=b.length;c--;)-1===a.event.props.indexOf(b[c])&&a.event.props.push(b[c])}(a)}),window.WYSIWYGModernizr=function(a,b,c){function d(a){n.cssText=a}function e(a,b){return typeof a===b}var f,g,h,i="2.7.1",j={},k=b.documentElement,l="modernizr",m=b.createElement(l),n=m.style,o=({}.toString," -webkit- -moz- -o- -ms- ".split(" ")),p={},q=[],r=q.slice,s=function(a,c,d,e){var f,g,h,i,j=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))for(;d--;)h=b.createElement("div"),h.id=e?e[d]:l+(d+1),j.appendChild(h);return f=["&#173;",'<style id="s',l,'">',a,"</style>"].join(""),j.id=l,(m?j:n).innerHTML+=f,n.appendChild(j),m||(n.style.background="",n.style.overflow="hidden",i=k.style.overflow,k.style.overflow="hidden",k.appendChild(n)),g=c(j,a),m?j.parentNode.removeChild(j):(n.parentNode.removeChild(n),k.style.overflow=i),!!g},t=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return s("@media "+b+" { #"+l+" { position: absolute; } }",function(b){d="absolute"==(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle).position}),d},u={}.hasOwnProperty;h=e(u,"undefined")||e(u.call,"undefined")?function(a,b){return b in a&&e(a.constructor.prototype[b],"undefined")}:function(a,b){return u.call(a,b)},Function.prototype.bind||(Function.prototype.bind=function(a){var b=this;if("function"!=typeof b)throw new TypeError;var c=r.call(arguments,1),d=function(){if(this instanceof d){var e=function(){};e.prototype=b.prototype;var f=new e,g=b.apply(f,c.concat(r.call(arguments)));return Object(g)===g?g:f}return b.apply(a,c.concat(r.call(arguments)))};return d}),p.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:s(["@media (",o.join("touch-enabled),("),l,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=9===a.offsetTop}),c};for(var v in p)h(p,v)&&(g=v.toLowerCase(),j[g]=p[v](),q.push((j[g]?"":"no-")+g));return j.addTest=function(a,b){if("object"==typeof a)for(var d in a)h(a,d)&&j.addTest(d,a[d]);else{if(a=a.toLowerCase(),j[a]!==c)return j;b="function"==typeof b?b():b,"undefined"!=typeof enableClasses&&enableClasses&&(k.className+=" "+(b?"":"no-")+a),j[a]=b}return j},d(""),m=f=null,j._version=i,j._prefixes=o,j.mq=t,j.testStyles=s,j}(this,this.document),function(){function a(a){return a.replace(/^\s+|\s+$/g,"")}function b(a){return a.replace(/^\s+/g,"")}function c(c,d,e,f){function g(){return this.pos=0,this.token="",this.current_mode="CONTENT",this.tags={parent:"parent1",parentcount:1,parent1:""},this.tag_type="",this.token_text=this.last_token=this.last_text=this.token_type="",this.newlines=0,this.indent_content=i,this.Utils={whitespace:"\n\r	 ".split(""),single_token:"br,input,link,meta,!doctype,basefont,base,area,hr,wbr,param,img,isindex,?xml,embed,?php,?,?=".split(","),extra_liners:"head,body,/html".split(","),in_array:function(a,b){for(var c=0;c<b.length;c++)if(a===b[c])return!0;return!1}},this.traverse_whitespace=function(){var a="";if(a=this.input.charAt(this.pos),this.Utils.in_array(a,this.Utils.whitespace)){for(this.newlines=0;this.Utils.in_array(a,this.Utils.whitespace);)o&&"\n"===a&&this.newlines<=p&&(this.newlines+=1),this.pos++,a=this.input.charAt(this.pos);return!0}return!1},this.get_content=function(){for(var a="",b=[],c=!1;"<"!==this.input.charAt(this.pos);){if(this.pos>=this.input.length)return b.length?b.join(""):["","TK_EOF"];if(this.traverse_whitespace())b.length&&(c=!0);else{if(q){var d=this.input.substr(this.pos,3);if("{{#"===d||"{{/"===d)break;if("{{"===this.input.substr(this.pos,2)&&"{{else}}"===this.get_tag(!0))break}a=this.input.charAt(this.pos),this.pos++,c&&(this.line_char_count>=this.wrap_line_length?(this.print_newline(!1,b),this.print_indentation(b)):(this.line_char_count++,b.push(" ")),c=!1),this.line_char_count++,b.push(a)}}return b.length?b.join(""):""},this.get_contents_to=function(a){if(this.pos===this.input.length)return["","TK_EOF"];var b="",c=new RegExp("</"+a+"\\s*>","igm");c.lastIndex=this.pos;var d=c.exec(this.input),e=d?d.index:this.input.length;return this.pos<e&&(b=this.input.substring(this.pos,e),this.pos=e),b},this.record_tag=function(a){this.tags[a+"count"]?(this.tags[a+"count"]++,this.tags[a+this.tags[a+"count"]]=this.indent_level):(this.tags[a+"count"]=1,this.tags[a+this.tags[a+"count"]]=this.indent_level),this.tags[a+this.tags[a+"count"]+"parent"]=this.tags.parent,this.tags.parent=a+this.tags[a+"count"]},this.retrieve_tag=function(a){if(this.tags[a+"count"]){for(var b=this.tags.parent;b&&a+this.tags[a+"count"]!==b;)b=this.tags[b+"parent"];b&&(this.indent_level=this.tags[a+this.tags[a+"count"]],this.tags.parent=this.tags[b+"parent"]),delete this.tags[a+this.tags[a+"count"]+"parent"],delete this.tags[a+this.tags[a+"count"]],1===this.tags[a+"count"]?delete this.tags[a+"count"]:this.tags[a+"count"]--}},this.indent_to_tag=function(a){if(this.tags[a+"count"]){for(var b=this.tags.parent;b&&a+this.tags[a+"count"]!==b;)b=this.tags[b+"parent"];b&&(this.indent_level=this.tags[a+this.tags[a+"count"]])}},this.get_tag=function(a){var b,c,d,e="",f=[],g="",h=!1,i=this.pos,j=this.line_char_count;a=void 0!==a?a:!1;do{if(this.pos>=this.input.length)return a&&(this.pos=i,this.line_char_count=j),f.length?f.join(""):["","TK_EOF"];if(e=this.input.charAt(this.pos),this.pos++,this.Utils.in_array(e,this.Utils.whitespace))h=!0;else{if(("'"===e||'"'===e)&&(e+=this.get_unformatted(e),h=!0),"="===e&&(h=!1),f.length&&"="!==f[f.length-1]&&">"!==e&&h&&(this.line_char_count>=this.wrap_line_length?(this.print_newline(!1,f),this.print_indentation(f)):(f.push(" "),this.line_char_count++),h=!1),q&&"<"===d&&e+this.input.charAt(this.pos)==="{{"&&(e+=this.get_unformatted("}}"),f.length&&" "!==f[f.length-1]&&"<"!==f[f.length-1]&&(e=" "+e),h=!0),"<"!==e||d||(b=this.pos-1,d="<"),q&&!d&&f.length>=2&&"{"===f[f.length-1]&&"{"==f[f.length-2]&&(b="#"===e||"/"===e?this.pos-3:this.pos-2,d="{"),this.line_char_count++,f.push(e),f[1]&&"!"===f[1]){f=[this.get_comment(b)];break}if(q&&"{"===d&&f.length>2&&"}"===f[f.length-2]&&"}"===f[f.length-1])break}}while(">"!==e);var k,l,m=f.join("");k=m.indexOf(-1!==m.indexOf(" ")?" ":"{"===m[0]?"}":">"),l="<"!==m[0]&&q?"#"===m[2]?3:2:1;var o=m.substring(l,k).toLowerCase();return"/"===m.charAt(m.length-2)||this.Utils.in_array(o,this.Utils.single_token)?a||(this.tag_type="SINGLE"):q&&"{"===m[0]&&"else"===o?a||(this.indent_to_tag("if"),this.tag_type="HANDLEBARS_ELSE",this.indent_content=!0,this.traverse_whitespace()):"script"===o?a||(this.record_tag(o),this.tag_type="SCRIPT"):"style"===o?a||(this.record_tag(o),this.tag_type="STYLE"):this.is_unformatted(o,n)?(g=this.get_unformatted("</"+o+">",m),f.push(g),b>0&&this.Utils.in_array(this.input.charAt(b-1),this.Utils.whitespace)&&f.splice(0,0,this.input.charAt(b-1)),c=this.pos-1,this.Utils.in_array(this.input.charAt(c+1),this.Utils.whitespace)&&f.push(this.input.charAt(c+1)),this.tag_type="SINGLE"):"!"===o.charAt(0)?a||(this.tag_type="SINGLE",this.traverse_whitespace()):a||("/"===o.charAt(0)?(this.retrieve_tag(o.substring(1)),this.tag_type="END",this.traverse_whitespace()):(this.record_tag(o),"html"!==o.toLowerCase()&&(this.indent_content=!0),this.tag_type="START",this.traverse_whitespace()),this.Utils.in_array(o,this.Utils.extra_liners)&&(this.print_newline(!1,this.output),this.output.length&&"\n"!==this.output[this.output.length-2]&&this.print_newline(!0,this.output))),a&&(this.pos=i,this.line_char_count=j),f.join("")},this.get_comment=function(a){var b="",c=">",d=!1;for(this.pos=a,input_char=this.input.charAt(this.pos),this.pos++;this.pos<=this.input.length&&(b+=input_char,b[b.length-1]!==c[c.length-1]||-1===b.indexOf(c));)!d&&b.length<10&&(0===b.indexOf("<![if")?(c="<![endif]>",d=!0):0===b.indexOf("<![cdata[")?(c="]]>",d=!0):0===b.indexOf("<![")?(c="]>",d=!0):0===b.indexOf("<!--")&&(c="-->",d=!0)),input_char=this.input.charAt(this.pos),this.pos++;return b},this.get_unformatted=function(a,b){if(b&&-1!==b.toLowerCase().indexOf(a))return"";var c="",d="",e=0,f=!0;do{if(this.pos>=this.input.length)return d;if(c=this.input.charAt(this.pos),this.pos++,this.Utils.in_array(c,this.Utils.whitespace)){if(!f){this.line_char_count--;continue}if("\n"===c||"\r"===c){d+="\n",this.line_char_count=0;continue}}d+=c,this.line_char_count++,f=!0,q&&"{"===c&&d.length&&"{"===d[d.length-2]&&(d+=this.get_unformatted("}}"),e=d.length)}while(-1===d.toLowerCase().indexOf(a,e));return d},this.get_token=function(){var a;if("TK_TAG_SCRIPT"===this.last_token||"TK_TAG_STYLE"===this.last_token){var b=this.last_token.substr(7);return a=this.get_contents_to(b),"string"!=typeof a?a:[a,"TK_"+b]}if("CONTENT"===this.current_mode)return a=this.get_content(),"string"!=typeof a?a:[a,"TK_CONTENT"];if("TAG"===this.current_mode){if(a=this.get_tag(),"string"!=typeof a)return a;var c="TK_TAG_"+this.tag_type;return[a,c]}},this.get_full_indent=function(a){return a=this.indent_level+a||0,1>a?"":Array(a+1).join(this.indent_string)},this.is_unformatted=function(a,b){if(!this.Utils.in_array(a,b))return!1;if("a"!==a.toLowerCase()||!this.Utils.in_array("a",b))return!0;var c=this.get_tag(!0),d=(c||"").match(/^\s*<\s*\/?([a-z]*)\s*[^>]*>\s*$/);return!d||this.Utils.in_array(d,b)?!0:!1},this.printer=function(a,c,d,e,f){this.input=a||"",this.output=[],this.indent_character=c,this.indent_string="",this.indent_size=d,this.brace_style=f,this.indent_level=0,this.wrap_line_length=e,this.line_char_count=0;for(var g=0;g<this.indent_size;g++)this.indent_string+=this.indent_character;this.print_newline=function(a,b){this.line_char_count=0,b&&b.length&&(a||"\n"!==b[b.length-1])&&b.push("\n")},this.print_indentation=function(a){for(var b=0;b<this.indent_level;b++)a.push(this.indent_string),this.line_char_count+=this.indent_string.length},this.print_token=function(a){(a||""!==a)&&this.output.length&&"\n"===this.output[this.output.length-1]&&(this.print_indentation(this.output),a=b(a)),this.print_token_raw(a)},this.print_token_raw=function(a){a&&""!==a&&(a.length>1&&"\n"===a[a.length-1]?(this.output.push(a.slice(0,-1)),this.print_newline(!1,this.output)):this.output.push(a));for(var b=0;b<this.newlines;b++)this.print_newline(b>0,this.output);this.newlines=0},this.indent=function(){this.indent_level++},this.unindent=function(){this.indent_level>0&&this.indent_level--}},this}var h,i,j,k,l,m,n,o,p,q;for(d=d||{},void 0!==d.wrap_line_length&&0!==parseInt(d.wrap_line_length,10)||void 0===d.max_char||0===parseInt(d.max_char,10)||(d.wrap_line_length=d.max_char),i=d.indent_inner_html||!0,j=parseInt(d.indent_size||1,10),k=d.indent_char||"	",m=d.brace_style||"collapse",l=0===parseInt(d.wrap_line_length,10)?32786:parseInt(d.wrap_line_length||1e5,10),n=d.unformatted||["a","span","bdo","em","strong","dfn","code","samp","kbd","var","cite","abbr","acronym","q","sub","sup","tt","i","b","big","small","u","s","strike","font","ins","del","pre","address","dt","h1","h2","h3","h4","h5","h6"],o=d.preserve_newlines||!0,p=o?parseInt(d.max_preserve_newlines||32786,10):0,q=d.indent_handlebars||!0,h=new g,h.printer(c,k,j,l,m);;){var r=h.get_token();if(h.token_text=r[0],h.token_type=r[1],"TK_EOF"===h.token_type)break;switch(h.token_type){case"TK_TAG_START":h.print_newline(!1,h.output),h.print_token(h.token_text),h.indent_content&&(h.indent(),h.indent_content=!1),h.current_mode="CONTENT";break;case"TK_TAG_STYLE":case"TK_TAG_SCRIPT":h.print_newline(!1,h.output),h.print_token(h.token_text),h.current_mode="CONTENT";break;case"TK_TAG_END":if("TK_CONTENT"===h.last_token&&""===h.last_text){var s=h.token_text.match(/\w+/)[0],t=null;h.output.length&&(t=h.output[h.output.length-1].match(/(?:<|{{#)\s*(\w+)/)),(null===t||t[1]!==s)&&h.print_newline(!1,h.output)}h.print_token(h.token_text),h.current_mode="CONTENT";break;case"TK_TAG_SINGLE":var u=h.token_text.match(/^\s*<([a-z]+)/i);u&&h.Utils.in_array(u[1],n)||h.print_newline(!1,h.output),h.print_token(h.token_text),h.current_mode="CONTENT";break;case"TK_TAG_HANDLEBARS_ELSE":h.print_token(h.token_text),h.indent_content&&(h.indent(),h.indent_content=!1),h.current_mode="CONTENT";break;case"TK_CONTENT":h.print_token(h.token_text),h.current_mode="TAG";break;case"TK_STYLE":case"TK_SCRIPT":if(""!==h.token_text){h.print_newline(!1,h.output);var v,w=h.token_text,x=1;"TK_SCRIPT"===h.token_type?v="function"==typeof e&&e:"TK_STYLE"===h.token_type&&(v="function"==typeof f&&f),"keep"===d.indent_scripts?x=0:"separate"===d.indent_scripts&&(x=-h.indent_level);var y=h.get_full_indent(x);if(v)w=v(w.replace(/^\s*/,y),d);else{var z=w.match(/^\s*/)[0],A=z.match(/[^\n\r]*$/)[0].split(h.indent_string).length-1,B=h.get_full_indent(x-A);w=w.replace(/^\s*/,y).replace(/\r\n|\r|\n/g,"\n"+B).replace(/\s+$/,"")}w&&(h.print_token_raw(y+a(w)),h.print_newline(!1,h.output))}h.current_mode="TAG"}h.last_token=h.token_type,h.last_text=h.token_text}return h.output.join("")}if("function"==typeof define&&define.amd)define(["./beautify","./beautify-css"],function(a,b){return{html_beautify:function(d,e){return c(d,e,a,b)}}});else if("undefined"!=typeof exports){var d=require("./beautify.js").js_beautify,e=require("./beautify-css.js").css_beautify;exports.html_beautify=function(a,b){return c(a,b,d,e)}}else"undefined"!=typeof window?window.html_beautify=function(a,b){return c(a,b,window.js_beautify,window.css_beautify)}:"undefined"!=typeof global&&(global.html_beautify=function(a,b){return c(a,b,global.js_beautify,global.css_beautify)})}();/*!
 * v0.0.4
 * Copyright (c) 2013 First Opinion
 * formatter.js is open sourced under the MIT license.
 *
 * thanks to digitalBush/jquery.maskedinput for some of the trickier
 * keycode handling
 */

;(function ($, window, document, undefined) {

// Defaults
var defaults = {
  persistent: false,
  repeat: false,
  placeholder: ' '
};

// Regexs for input validation
var inptRegs = {
  '9': new RegExp('[0-9]'),
  'a': new RegExp('[A-Za-z]'),
  '*': new RegExp('[A-Za-z0-9]')
};

//
// Class Constructor - Called with new Formatter(el, opts)
// Responsible for setting up required instance variables, and
// attaching the event listener to the element.
//
function Formatter(el, opts) {
  // Cache this
  var self = this;

  // Make sure we have an element. Make accesible to instance
  self.el = el;
  if (!self.el) {
    throw new TypeError('Must provide an existing element');
  }

  // Merge opts with defaults
  self.opts = utils.extend({}, defaults, opts);

  // Make sure we have valid opts
  if (typeof self.opts.pattern === 'undefined') {
    throw new TypeError('Must provide a pattern');
  }

  // Get info about the given pattern
  var parsed   = pattern.parse(self.opts.pattern);
  self.mLength = parsed.mLength;
  self.chars   = parsed.chars;
  self.inpts   = parsed.inpts;

  // Init values
  self.hldrs = {};
  self.focus = 0;

  // Add Listeners
  utils.addListener(self.el, 'keydown', function (evt) {
    self._keyDown(evt);
  });
  utils.addListener(self.el, 'keypress', function (evt) {
    self._keyPress(evt);
  });
  utils.addListener(self.el, 'paste', function (evt) {
    self._paste(evt);
  });

  // Persistence
  if (self.opts.persistent) {
    // Format on start
    self._processKey(null, true);
    self.el.blur();

    // Add Listeners
    utils.addListener(self.el, 'focus', function (evt) {
      self._focus(evt);
    });
    utils.addListener(self.el, 'click', function (evt) {
      self._focus(evt);
    });
    utils.addListener(self.el, 'touchstart', function (evt) {
      self._focus(evt);
    });
  }
}

//
// @private
// Handler called on all keyDown strokes. All keys trigger
// this handler. Only process delete keys.
//
Formatter.prototype._keyDown = function (evt) {
  // The first thing we need is the character code
  var k = evt.which || evt.keyCode;

  // If delete key
  if (k && utils.isDelKey(k)) {
    // Process the keyCode and prevent default
    this._processKey(null, k);
    return utils.preventDefault(evt);
  }
};

//
// @private
// Handler called on all keyPress strokes. Only processes
// character keys (as long as no modifier key is in use).
//
Formatter.prototype._keyPress = function (evt) {
  // The first thing we need is the character code
  var k, isSpecial;
  // Mozilla will trigger on special keys and assign the the value 0
  // We want to use that 0 rather than the keyCode it assigns.
  if (evt.which) {
    k = evt.which;
  } else {
    k = evt.keyCode;
    isSpecial = utils.isSpecialKey(k);
  }
  // Process the keyCode and prevent default
  if (!utils.isDelKey(k) && !isSpecial && !utils.isModifier(evt)) {
    this._processKey(String.fromCharCode(k), false);
    return utils.preventDefault(evt);
  }
};

//
// @private
// Handler called on paste event.
//
Formatter.prototype._paste = function (evt) {
  // Process the clipboard paste and prevent default
  this._processKey(utils.getClip(evt), false);
  return utils.preventDefault(evt);
};

//
// @private
// Handle called on focus event.
//
Formatter.prototype._focus = function (evt) {
  // Wrapped in timeout so that we can grab input selection
  var self = this;
  setTimeout(function () {
    // Grab selection
    var selection = inptSel.get(self.el);
    // Char check
    var isAfterStart = selection.end > self.focus;
        isFirstChar  = selection.end === 0;
    // If clicked in front of start, refocus to start
    if (isAfterStart || isFirstChar) {
      inptSel.set(self.el, self.focus);
    }
  }, 0);
};

//
// @private
// Using the provided key information, alter el value.
//
Formatter.prototype._processKey = function (chars, delKey) {
  // Get current state
  this.sel = inptSel.get(this.el);
  this.val = this.el.value;

  // Init values
  this.delta = 0;

  // If chars were highlighted, we need to remove them
  if (this.sel.begin !== this.sel.end) {
    this.delta = (-1) * Math.abs(this.sel.begin - this.sel.end);
    this.val   = utils.removeChars(this.val, this.sel.begin, this.sel.end);
  }
  // If delKey
  else if (delKey) {
    // Delete
    if (delKey && delKey == 46) {
      // Adjust focus to make sure its not on a formatted char
      while (this.chars[this.sel.begin]) {
        this._nextPos();
      }
      // As long as we are not at the end
      if (this.sel.begin < this.val.length) {
        // We will simulate a delete by moving the caret to the next char
        // and then deleting
        this._nextPos();
        this.val = utils.removeChars(this.val, this.sel.end -1, this.sel.end);
        this.delta = -1;
      }
    // or Backspace and not at start
    } else if (delKey && this.sel.begin - 1 >= 0) {
      this.val = utils.removeChars(this.val, this.sel.end -1, this.sel.end);
      this.delta = -1;
    }
  }

  // If the key is not a del key, it should convert to a str
  if (!delKey) {
    // Add char at position and increment delta
    this.val = utils.addChars(this.val, chars, this.sel.begin);
    this.delta += chars.length;
  }

  // Format el.value (also handles updating caret position)
  this._formatValue();
};

//
// @private
// Quick helper method to move the caret to the next pos
//
Formatter.prototype._nextPos = function () {
  this.sel.end ++;
  this.sel.begin ++;
};

//
// @private
// Alter element value to display characters matching the provided
// instance pattern. Also responsible for updatin
//
Formatter.prototype._formatValue = function () {
  // Set caret pos
  this.curPos = this.sel.end;
  this.newPos = this.curPos + this.delta;

  // Remove all formatted chars from val
  this._removeChars();
  // Validate inpts
  this._validateInpts();
  // Add formatted characters
  this._addChars();

  // Set vakye and adhere to maxLength
  this.el.value = this.val.substr(0, this.mLength);

  // Set new caret position
  inptSel.set(this.el, this.newPos);
};

//
// @private
// Remove all formatted before and after a specified pos
//
Formatter.prototype._removeChars = function () {
  // Delta shouldn't include placeholders
  if (this.sel.end > this.focus) {
    this.delta += this.sel.end - this.focus;
  }

  // Account for shifts during removal
  var shift = 0;

  // Loop through all possible char positions
  for (var i = 0; i <= this.mLength; i++) {
    // Get transformed position
    var curChar = this.chars[i],
        curHldr = this.hldrs[i],
        pos = i + shift,
        val;

    // If after selection we need to account for delta
    pos = (i >= this.sel.begin) ? pos + this.delta : pos;
    val = this.val[pos];
    // Remove char and account for shift
    if (curChar && curChar == val || curHldr && curHldr == val) {
      this.val = utils.removeChars(this.val, pos, pos + 1);
      shift--;
    }
  }

  // All hldrs should be removed now
  this.hldrs = {};

  // Set focus to last character
  this.focus = this.val.length;
};

//
// @private
// Make sure all inpts are valid, else remove and update delta
//
Formatter.prototype._validateInpts = function () {
  // Loop over each char and validate
  for (var i = 0; i < this.val.length; i++) {
    // Get char inpt type
    var inptType = this.inpts[i];

    // If improper type, or char doesnt match, remove
    if (!inptRegs[inptType] || !inptRegs[inptType].test(this.val[i])) {
      // Check bounds
      if (this.inpts[i]) {
        this.val = utils.removeChars(this.val, i, i + 1);
        this.focusStart--;
        this.newPos--;
        this.delta--;
        i--;
      }
    }
  }
};

//
// @private
// Loop over val and add formatted chars as necessary
//
Formatter.prototype._addChars = function () {
  if (this.opts.persistent) {
    // Loop over all possible characters
    for (var i = 0; i <= this.mLength; i++) {
      if (!this.val[i]) {
        // Add placeholder at pos
        this.val = utils.addChars(this.val, this.opts.placeholder, i);
        this.hldrs[i] = this.opts.placeholder;
      }
      this._addChar(i);
    }

    // Adjust focus to make sure its not on a formatted char
    while (this.chars[this.focus]) {
      this.focus++;
    }
  } else {
    // Avoid caching val.length, as it changes during manipulations
    for (var j = 0; j <= this.val.length; j++) {
      this._addChar(j);
    }
  }
};

//
// @private
// Add formattted char at position
//
Formatter.prototype._addChar = function (i) {
  // If char exists at position
  var char = this.chars[i];
  if (!char) { return true; }

  // If chars are added in between the old pos and new pos
  // we need to increment pos and delta
  if (utils.isBetween(i, [this.sel.begin -1, this.newPos +1])) {
    this.newPos ++;
    this.delta ++;
  }

  // If character added before focus, incr
  if (i <= this.focus) {
    this.focus++;
  }

  // When moving backwards there are some race conditions where we
  // dont want to add the character
  if (this.delta < 0 && (this.val[i] == char )) { return true; }

  // Updateholder
  if (this.hldrs[i]) {
    delete this.hldrs[i];
    this.hldrs[i + 1] = this.opts.placeholder;
  }

  // Update value
  this.val = utils.addChars(this.val, char, i);
};
// Define module
var pattern = {};

// Match information
var DELIM_SIZE = 4;

// Our regex used to parse
var regexp  = new RegExp('{{([^}]+)}}', 'g');

//
// Helper method to parse pattern str
//
var getMatches = function (pattern) {
  // Populate array of matches
  var matches = [],
      match;
  while(match = regexp.exec(pattern)) {
    matches.push(match);
  }

  return matches;
};

//
// Create an object holding all formatted characters
// with corresponding positions
//
pattern.parse = function (pattern) {
  // Our obj to populate
  var info = { inpts: {}, chars: {} };

  // Pattern information
  var matches = getMatches(pattern),
      pLength = pattern.length;

  // Counters
  var mCount = 0,
      iCount = 0,
      i = 0;

  // Add inpts, move to end of match, and process
  var processMatch = function (val) {
    var valLength = val.length;
    for (var j = 0; j < valLength; j++) {
      info.inpts[iCount] = val[j];
      iCount++;
    }
    mCount ++;
    i += (val.length + DELIM_SIZE - 1);
  };

  // Process match or add chars
  for (i; i < pLength; i++) {
    if (i == matches[mCount].index) {
      processMatch(matches[mCount][1]);
    } else {
      info.chars[i - (mCount * DELIM_SIZE)] = pattern[i];
    }
  }

  // Set mLength and return
  info.mLength = i - (mCount * DELIM_SIZE);
  return info;
};
// Define module
var inptSel = {};

//
// Get begin and end positions of selected input. Return 0's
// if there is no selectiion data
//
inptSel.get = function (el) {
  // If normal browser return with result
  if (typeof el.selectionStart == "number") {
    return {
      begin: el.selectionStart,
      end: el.selectionEnd
    };
  }

  // Uh-Oh. We must be IE. Fun with TextRange!!
  var range = document.selection.createRange();
  // Determine if there is a selection
  if (range && range.parentElement() == el) {
    var inputRange = el.createTextRange(),
        endRange   = el.createTextRange(),
        length     = el.value.length;

    // Create a working TextRange for the input selection
    inputRange.moveToBookmark(range.getBookmark());

    // Move endRange begin pos to end pos (hence endRange)
    endRange.collapse(false);

    // If we are at the very end of the input, begin and end
    // must both be the length of the el.value
    if (inputRange.compareEndPoints("StartToEnd", endRange) > -1) {
      return { begin: length, end: length };
    }

    // Note: moveStart usually returns the units moved, which
    // one may think is -length, however, it will stop when it
    // gets to the begin of the range, thus giving us the
    // negative value of the pos.
    return {
      begin: -inputRange.moveStart("character", -length),
      end: -inputRange.moveEnd("character", -length)
    };
  }

  //Return 0's on no selection data
  return { begin: 0, end: 0 };
};

//
// Set the caret position at a specified location
//
inptSel.set = function (el, pos) {
  // If normal browser
  if (el.setSelectionRange) {
    el.focus();
    el.setSelectionRange(pos,pos);

  // IE = TextRange fun
  } else if (el.createTextRange) {
    var range = el.createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
};
// Define module
var utils = {};

// Useragent info for keycode handling
var uAgent = (typeof navigator !== 'undefined') ? navigator.userAgent : null,
    iPhone = /iphone/i.test(uAgent);

//
// Shallow copy properties from n objects to destObj
//
utils.extend = function (destObj) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      destObj[key] = arguments[i][key];
    }
  }
  return destObj;
};

//
// Add a given character to a string at a defined pos
//
utils.addChars = function (str, chars, pos) {
  return str.substr(0, pos) + chars + str.substr(pos, str.length);
};

//
// Remove a span of characters
//
utils.removeChars = function (str, start, end) {
  return str.substr(0, start) + str.substr(end, str.length);
};

//
// Return true/false is num false between bounds
//
utils.isBetween = function (num, bounds) {
  bounds.sort(function(a,b) { return a-b; });
  return (num > bounds[0] && num < bounds[1]);
};

//
// Helper method for cross browser event listeners
//
utils.addListener = function (el, evt, handler) {
  return (typeof el.addEventListener != "undefined")
    ? el.addEventListener(evt, handler, false)
    : el.attachEvent('on' + evt, handler);
};

//
// Helper method for cross browser implementation of preventDefault
//
utils.preventDefault = function (evt) {
  return (evt.preventDefault) ? evt.preventDefault() : (evt.returnValue = false);
};

//
// Helper method for cross browser implementation for grabbing
// clipboard data
//
utils.getClip = function (evt) {
  if (evt.clipboardData) { return evt.clipboardData.getData('Text'); }
  if (window.clipboardData) { return window.clipboardData.getData('Text'); }
};

//
// Returns true/false if k is a del key
//
utils.isDelKey = function (k) {
  return k === 8 || k === 46 || (iPhone && k === 127);
};

//
// Returns true/false if k is an arrow key
//
utils.isSpecialKey = function (k) {
  var codes = {
    '35': 'end',
    '36': 'home',
    '37': 'leftarrow',
    '38': 'uparrow',
    '39': 'rightarrow',
    '40': 'downarrow'
  };
  // If del or special key
  return codes[k];
};

//
// Returns true/false if modifier key is held down
//
utils.isModifier = function (evt) {
  return evt.ctrlKey || evt.altKey || evt.metaKey;
};
// A really lightweight plugin wrapper around the constructor,
// preventing against multiple instantiations
var pluginName = 'formatter';
$.fn[pluginName] = function (options) {
  return this.each(function () {
    if (!$.data(this, 'plugin_' + pluginName)) {
      $.data(this, 'plugin_' + pluginName,
      new Formatter(this, options));
    }
  });
};

})( jQuery, window, document);/* ===================================================
 *  jquery-sortable.js v0.9.11
 *  http://johnny.github.com/jquery-sortable/
 * ===================================================
 *  Copyright (c) 2012 Jonas von Andrian
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ========================================================== */

!function ( $, window, undefined){
  var eventNames,
  pluginName = 'sortable',
  containerDefaults = {
    // If true, items can be dragged from this container
    drag: true,
    // If true, items can be droped onto this container
    drop: true,
    // Exclude items from being draggable, if the
    // selector matches the item
    exclude: "",
    // If true, search for nested containers within an item
    nested: true,
    // If true, the items are assumed to be arranged vertically
    vertical: true
  }, // end container defaults
  groupDefaults = {
    // This is executed after the placeholder has been moved.
    // $closestItemOrContainer contains the closest item, the placeholder
    // has been put at or the closest empty Container, the placeholder has
    // been appended to.
    afterMove: function ($placeholder, container, $closestItemOrContainer) {
    },
    // The exact css path between the container and its items, e.g. "> tbody"
    containerPath: "",
    // The css selector of the containers
    containerSelector: "ol, ul, tbody",
    // Distance the mouse has to travel to start dragging
    distance: 0,
    // The css selector of the drag handle
    handle: "",
    // The exact css path between the item and its subcontainers
    itemPath: "",
    // The css selector of the items
    itemSelector: "li, tr",
    // Check if the dragged item may be inside the container.
    // Use with care, since the search for a valid container entails a depth first search
    // and may be quite expensive.
    isValidTarget: function ($item, container) {
      return true
    },
    // Executed before onDrop if placeholder is detached.
    // This happens if pullPlaceholder is set to false and the drop occurs outside a container.
    onCancel: function ($item, container, _super, event) {
    },
    // Executed at the beginning of a mouse move event.
    // The Placeholder has not been moved yet.
    onDrag: function ($item, position, _super, event) {
      $item.css(position)
    },
    // Called after the drag has been started,
    // that is the mouse button is beeing held down and
    // the mouse is moving.
    // The container is the closest initialized container.
    // Therefore it might not be the container, that actually contains the item.
    onDragStart: function ($item, container, _super, event) {
      $item.css({
        height: $item.height(),
        width: $item.width()
      })
      $item.addClass("dragged")
      $("body").addClass("dragging")
    },
    // Called when the mouse button is beeing released
    onDrop: function ($item, container, _super, event) {
      $item.removeClass("dragged").removeAttr("style")
      $("body").removeClass("dragging")
    },
    // Called on mousedown. If falsy value is returned, the dragging will not start.
    // If clicked on input element, ignore
    onMousedown: function ($item, _super, event) {
      if (event.target.nodeName != 'INPUT') {
        event.preventDefault()
        return true
      }
    },
    // Template for the placeholder. Can be any valid jQuery input
    // e.g. a string, a DOM element.
    // The placeholder must have the class "placeholder"
    placeholder: '<li class="placeholder"/>',
    // If true, the position of the placeholder is calculated on every mousemove.
    // If false, it is only calculated when the mouse is above a container.
    pullPlaceholder: true,
    // Specifies serialization of the container group.
    // The pair $parent/$children is either container/items or item/subcontainers.
    // Note that this default method only works, if every item only has one subcontainer
    serialize: function ($parent, $children, parentIsContainer) {
      var result = $.extend({}, $parent.data())

      if(parentIsContainer)
        return $children
      else if ($children[0]){
        result.children = $children
        delete result.subContainer
      }

      delete result.sortable

      return result
    },
    // Set tolerance while dragging. Positive values decrease sensitivity,
    // negative values increase it.
    tolerance: 0
  }, // end group defaults
  containerGroups = {},
  groupCounter = 0,
  emptyBox = {
    left: 0,
    top: 0,
    bottom: 0,
    right:0
  }
  eventNames = {
    start: "touchstart.sortable mousedown.sortable",
    drop: "touchend.sortable touchcancel.sortable mouseup.sortable",
    drag: "touchmove.sortable mousemove.sortable",
    scroll: "scroll.sortable"
  }

  /*
   * a is Array [left, right, top, bottom]
   * b is array [left, top]
   */
  function d(a,b) {
    var x = Math.max(0, a[0] - b[0], b[0] - a[1]),
    y = Math.max(0, a[2] - b[1], b[1] - a[3])
    return x+y;
  }

  function setDimensions(array, dimensions, tolerance, useOffset) {
    var i = array.length,
    offsetMethod = useOffset ? "offset" : "position"
    tolerance = tolerance || 0

    while(i--){
      var el = array[i].el ? array[i].el : $(array[i]),
      // use fitting method
      pos = el[offsetMethod]()
      pos.left += parseInt(el.css('margin-left'), 10)
      pos.top += parseInt(el.css('margin-top'),10)
      dimensions[i] = [
        pos.left - tolerance,
        pos.left + el.outerWidth() + tolerance,
        pos.top - tolerance,
        pos.top + el.outerHeight() + tolerance
      ]
    }
  }

  function getRelativePosition(pointer, element) {
    var offset = element.offset()
    return {
      left: pointer.left - offset.left,
      top: pointer.top - offset.top
    }
  }

  function sortByDistanceDesc(dimensions, pointer, lastPointer) {
    pointer = [pointer.left, pointer.top]
    lastPointer = lastPointer && [lastPointer.left, lastPointer.top]

    var dim,
    i = dimensions.length,
    distances = []

    while(i--){
      dim = dimensions[i]
      distances[i] = [i,d(dim,pointer), lastPointer && d(dim, lastPointer)]
    }
    distances = distances.sort(function  (a,b) {
      return b[1] - a[1] || b[2] - a[2] || b[0] - a[0]
    })

    // last entry is the closest
    return distances
  }

  function ContainerGroup(options) {
    this.options = $.extend({}, groupDefaults, options)
    this.containers = []

    if(!this.options.parentContainer){
      this.scrollProxy = $.proxy(this.scroll, this)
      this.dragProxy = $.proxy(this.drag, this)
      this.dropProxy = $.proxy(this.drop, this)
      this.placeholder = $(this.options.placeholder)

      if(!options.isValidTarget)
        this.options.isValidTarget = undefined
    }
  }

  ContainerGroup.get = function  (options) {
    if( !containerGroups[options.group]) {
      if(options.group === undefined)
        options.group = groupCounter ++
      containerGroups[options.group] = new ContainerGroup(options)
    }
    return containerGroups[options.group]
  }

  ContainerGroup.prototype = {
    dragInit: function  (e, itemContainer) {
      this.$document = $(itemContainer.el[0].ownerDocument)

      if(itemContainer.enabled()){
        // get item to drag
        this.item = $(e.target).closest(this.options.itemSelector)
        this.itemContainer = itemContainer

        if(!this.options.onMousedown(this.item, groupDefaults.onMousedown, e))
          return

        this.setPointer(e)
        this.toggleListeners('on')
      } else {
        this.toggleListeners('on', ['drop'])
      }

      this.dragInitDone = true
    },
    drag: function  (e) {
      if(!this.dragging){
        if(!this.distanceMet(e))
          return

        this.options.onDragStart(this.item, this.itemContainer, groupDefaults.onDragStart, e)
        this.item.before(this.placeholder)
        this.dragging = true
      }

      this.setPointer(e)
      // place item under the cursor
      this.options.onDrag(this.item,
                          getRelativePosition(this.pointer, this.item.offsetParent()),
                          groupDefaults.onDrag,
                          e)

      var x = e.pageX,
      y = e.pageY,
      box = this.sameResultBox,
      t = this.options.tolerance

      if(!box || box.top - t > y || box.bottom + t < y || box.left - t > x || box.right + t < x)
        if(!this.searchValidTarget())
          this.placeholder.detach()
    },
    drop: function  (e) {
      this.toggleListeners('off')

      this.dragInitDone = false

      if(this.dragging){
        // processing Drop, check if placeholder is detached
        if(this.placeholder.closest("html")[0])
          this.placeholder.before(this.item).detach()
        else
          this.options.onCancel(this.item, this.itemContainer, groupDefaults.onCancel, e)

        this.options.onDrop(this.item, this.getContainer(this.item), groupDefaults.onDrop, e)

        // cleanup
        this.clearDimensions()
        this.clearOffsetParent()
        this.lastAppendedItem = this.sameResultBox = undefined
        this.dragging = false
      }
    },
    searchValidTarget: function  (pointer, lastPointer) {
      if(!pointer){
        pointer = this.relativePointer || this.pointer
        lastPointer = this.lastRelativePointer || this.lastPointer
      }

      var distances = sortByDistanceDesc(this.getContainerDimensions(),
                                         pointer,
                                         lastPointer),
      i = distances.length

      while(i--){
        var index = distances[i][0],
        distance = distances[i][1]

        if(!distance || this.options.pullPlaceholder){
          var container = this.containers[index]
          if(!container.disabled){
            if(!this.$getOffsetParent()){
              var offsetParent = container.getItemOffsetParent()
              pointer = getRelativePosition(pointer, offsetParent)
              lastPointer = getRelativePosition(lastPointer, offsetParent)
            }
            if(container.searchValidTarget(pointer, lastPointer))
              return true
          }
        }
      }
      if(this.sameResultBox)
        this.sameResultBox = undefined
    },
    movePlaceholder: function  (container, item, method, sameResultBox) {
      var lastAppendedItem = this.lastAppendedItem
      if(!sameResultBox && lastAppendedItem && lastAppendedItem[0] === item[0])
        return;

      item[method](this.placeholder)
      this.lastAppendedItem = item
      this.sameResultBox = sameResultBox
      this.options.afterMove(this.placeholder, container, item)
    },
    getContainerDimensions: function  () {
      if(!this.containerDimensions)
        setDimensions(this.containers, this.containerDimensions = [], this.options.tolerance, !this.$getOffsetParent())
      return this.containerDimensions
    },
    getContainer: function  (element) {
      return element.closest(this.options.containerSelector).data(pluginName)
    },
    $getOffsetParent: function  () {
      if(this.offsetParent === undefined){
        var i = this.containers.length - 1,
        offsetParent = this.containers[i].getItemOffsetParent()

        if(!this.options.parentContainer){
          while(i--){
            if(offsetParent[0] != this.containers[i].getItemOffsetParent()[0]){
              // If every container has the same offset parent,
              // use position() which is relative to this parent,
              // otherwise use offset()
              // compare #setDimensions
              offsetParent = false
              break;
            }
          }
        }

        this.offsetParent = offsetParent
      }
      return this.offsetParent
    },
    setPointer: function (e) {
      var pointer = {
        left: e.pageX,
        top: e.pageY
      }

      if(this.$getOffsetParent()){
        var relativePointer = getRelativePosition(pointer, this.$getOffsetParent())
        this.lastRelativePointer = this.relativePointer
        this.relativePointer = relativePointer
      }

      this.lastPointer = this.pointer
      this.pointer = pointer
    },
    distanceMet: function (e) {
      return (Math.max(
                Math.abs(this.pointer.left - e.pageX),
                Math.abs(this.pointer.top - e.pageY)
            ) >= this.options.distance)
    },
    scroll: function  (e) {
      this.clearDimensions()
      this.clearOffsetParent()
    },
    toggleListeners: function (method, events) {
      var that = this
      events = events || ['drag','drop','scroll']

      $.each(events,function  (i,event) {
        that.$document[method](eventNames[event], that[event + 'Proxy'])
      })
    },
    clearOffsetParent: function () {
      this.offsetParent = undefined
    },
    // Recursively clear container and item dimensions
    clearDimensions: function  () {
      this.containerDimensions = undefined
      var i = this.containers.length
      while(i--){
        this.containers[i].clearDimensions()
      }
    },
    destroy: function () {
      // TODO iterate over subgroups and destroy them
      // TODO remove all events
      containerGroups[this.options.group] = undefined
    }
  }

  function Container(element, options) {
    this.el = element
    this.options = $.extend( {}, containerDefaults, options)

    this.group = ContainerGroup.get(this.options)
    this.rootGroup = this.options.rootGroup || this.group
    this.parentContainer = this.options.parentContainer
    this.handle = this.rootGroup.options.handle || this.rootGroup.options.itemSelector

    var itemPath = this.rootGroup.options.itemPath,
    target = itemPath ? this.el.find(itemPath) : this.el

    target.on(eventNames.start, this.handle, $.proxy(this.dragInit, this))

    if(this.options.drop)
      this.group.containers.push(this)
  }

  Container.prototype = {
    dragInit: function  (e) {
      var rootGroup = this.rootGroup

      if( !rootGroup.dragInitDone &&
          e.which === 1 &&
          this.options.drag &&
          !$(e.target).is(this.options.exclude))
        rootGroup.dragInit(e, this)
    },
    searchValidTarget: function  (pointer, lastPointer) {
      var distances = sortByDistanceDesc(this.getItemDimensions(),
                                         pointer,
                                         lastPointer),
      i = distances.length,
      rootGroup = this.rootGroup,
      validTarget = !rootGroup.options.isValidTarget ||
        rootGroup.options.isValidTarget(rootGroup.item, this)

      if(!i && validTarget){
        var itemPath = this.rootGroup.options.itemPath,
        target = itemPath ? this.el.find(itemPath) : this.el

        rootGroup.movePlaceholder(this, target, "append")
        return true
      } else
        while(i--){
          var index = distances[i][0],
          distance = distances[i][1]
          if(!distance && this.hasChildGroup(index)){
            var found = this.getContainerGroup(index).searchValidTarget(pointer, lastPointer)
            if(found)
              return true
          }
          else if(validTarget){
            this.movePlaceholder(index, pointer)
            return true
          }
        }
    },
    movePlaceholder: function  (index, pointer) {
      var item = $(this.items[index]),
      dim = this.itemDimensions[index],
      method = "after",
      width = item.outerWidth(),
      height = item.outerHeight(),
      offset = item.offset(),
      sameResultBox = {
        left: offset.left,
        right: offset.left + width,
        top: offset.top,
        bottom: offset.top + height
      }
      if(this.options.vertical){
        var yCenter = (dim[2] + dim[3]) / 2,
        inUpperHalf = pointer.top <= yCenter
        if(inUpperHalf){
          method = "before"
          sameResultBox.bottom -= height / 2
        } else
          sameResultBox.top += height / 2
      } else {
        var xCenter = (dim[0] + dim[1]) / 2,
        inLeftHalf = pointer.left <= xCenter
        if(inLeftHalf){
          method = "before"
          sameResultBox.right -= width / 2
        } else
          sameResultBox.left += width / 2
      }
      if(this.hasChildGroup(index))
        sameResultBox = emptyBox
      this.rootGroup.movePlaceholder(this, item, method, sameResultBox)
    },
    getItemDimensions: function  () {
      if(!this.itemDimensions){
        this.items = this.$getChildren(this.el, "item").filter(":not(.placeholder, .dragged)").get()
        setDimensions(this.items, this.itemDimensions = [], this.options.tolerance)
      }
      return this.itemDimensions
    },
    getItemOffsetParent: function  () {
      var offsetParent,
      el = this.el
      // Since el might be empty we have to check el itself and
      // can not do something like el.children().first().offsetParent()
      if(el.css("position") === "relative" || el.css("position") === "absolute"  || el.css("position") === "fixed")
        offsetParent = el
      else
        offsetParent = el.offsetParent()
      return offsetParent
    },
    hasChildGroup: function (index) {
      return this.options.nested && this.getContainerGroup(index)
    },
    getContainerGroup: function  (index) {
      var childGroup = $.data(this.items[index], "subContainer")
      if( childGroup === undefined){
        var childContainers = this.$getChildren(this.items[index], "container")
        childGroup = false

        if(childContainers[0]){
          var options = $.extend({}, this.options, {
            parentContainer: this,
            rootGroup: this.rootGroup,
            group: groupCounter ++
          })
          childGroup = childContainers[pluginName](options).data(pluginName).group
        }
        $.data(this.items[index], "subContainer", childGroup)
      }
      return childGroup
    },
    enabled: function () {
      return !this.disabled && (!this.parentContainer || this.parentContainer.enabled())
    },
    $getChildren: function (parent, type) {
      var options = this.rootGroup.options,
      path = options[type + "Path"],
      selector = options[type + "Selector"]

      parent = $(parent)
      if(path)
        parent = parent.find(path)

      return parent.children(selector)
    },
    _serialize: function (parent, isContainer) {
      var that = this,
      childType = isContainer ? "item" : "container",

      children = this.$getChildren(parent, childType).not(this.options.exclude).map(function () {
        return that._serialize($(this), !isContainer)
      }).get()

      return this.rootGroup.options.serialize(parent, children, isContainer)
    },
    clearDimensions: function  () {
      this.itemDimensions = undefined
      if(this.items && this.items[0]){
        var i = this.items.length
        while(i--){
          var group = $.data(this.items[i], "subContainer")
          if(group)
            group.clearDimensions()
        }
      }
    }
  }

  var API = {
    enable: function  (ignoreChildren) {
      this.disabled = false
    },
    disable: function  (ignoreChildren) {
      this.disabled = true
    },
    serialize: function () {
      return this._serialize(this.el, true)
    },
    destroy: function () {
      this.rootGroup.destroy()
    }
  }

  $.extend(Container.prototype, API)

  /**
   * jQuery API
   *
   * Parameters are
   *   either options on init
   *   or a method name followed by arguments to pass to the method
   */
  $.fn[pluginName] = function(methodOrOptions) {
    var args = Array.prototype.slice.call(arguments, 1)

    return this.map(function(){
      var $t = $(this),
      object = $t.data(pluginName)

      if(object && API[methodOrOptions])
        return API[methodOrOptions].apply(object, args) || this
      else if(!object && (methodOrOptions === undefined ||
                          typeof methodOrOptions === "object"))
        $t.data(pluginName, new Container($t, methodOrOptions))

      return this
    });
  };

}(jQuery, window);/*
    Slip - swiping and reordering in lists of elements on touch screens, no fuss.

    Fires these events on list elements:

        • slip:swipe
            When swipe has been done and user has lifted finger off the screen.
            If you execute event.preventDefault() the element will be animated back to original position.
            Otherwise it will be animated off the list and set to display:none.

        • slip:beforeswipe
            Fired before first swipe movement starts.
            If you execute event.preventDefault() then element will not move at all.

        • slip:reorder
            Element has been dropped in new location. event.detail contains the location:
                • insertBefore: DOM node before which element has been dropped (null is the end of the list). Use with node.insertBefore().
                • spliceIndex: Index of element before which current element has been dropped, not counting the element iself.
                               For use with Array.splice() if the list is reflecting objects in some array.

        • slip:beforereorder
            When reordering movement starts.
            Element being reordered gets class `slip-reordering`.
            If you execute event.preventDefault() then element will not move at all.

        • slip:beforewait
            If you execute event.preventDefault() then reordering will begin immediately, blocking ability to scroll the page.

        • slip:tap
            When element was tapped without being swiped/reordered.


    Usage:

        CSS:
            You should set `user-select:none` (and WebKit prefixes, sigh) on list elements,
            otherwise unstoppable and glitchy text selection in iOS will get in the way.

            You should set `overflow-x: hidden` on the container or body to prevent horizontal scrollbar
            appearing when elements are swiped off the list.


        var list = document.querySelector('ul#slippylist');
        new Slip(list);

        list.addEventListener('slip:beforeswipe', function(e) {
            if (shouldNotSwipe(e.target)) e.preventDefault();
        });

        list.addEventListener('slip:swipe', function(e) {
            // e.target swiped
            if (thatWasSwipeToRemove) {
                e.target.parentNode.removeChild(e.target);
            } else {
                e.preventDefault(); // will animate back to original position
            }
        });

        list.addEventListener('slip:beforereorder', function(e) {
            if (shouldNotReorder(e.target)) e.preventDefault();
        });

        list.addEventListener('slip:reorder', function(e) {
            // e.target reordered.
            if (reorderedOK) {
                e.target.parentNode.insertBefore(e.target, e.detail.insertBefore);
            } else {
                e.preventDefault();
            }
        });

    Requires:
        • Touch events
        • CSS transforms
        • Function.bind()

    Caveats:
        • Elements must not change size while reordering or swiping takes place (otherwise it will be visually out of sync)
*/
/*!
    Slip.js 1.0

    © 2014 Kornel Lesiński <kornel@geekhood.net>. All rights reserved.

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
       the following disclaimer in the documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
    USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

window['Slip'] = (function(){
    'use strict';

    var damnYouChrome = /Chrome\/[34]/.test(navigator.userAgent); // For bugs that can't be programmatically detected :(
    var needsBodyHandlerHack = damnYouChrome; // Otherwise I _sometimes_ don't get any touchstart events and only clicks instead.
    var compositorDoesNotOrderLayers = damnYouChrome; // Looks like WebKit bug #61824, but iOS Safari doesn't have that problem.

    // -webkit-mess
    var testElement = document.createElement('div');

    var transitionPrefix = "webkitTransition" in testElement.style ? "webkitTransition" : "transition";
    var transformPrefix = "webkitTransform" in testElement.style ? "webkitTransform" : "transform";
    var transformProperty = transformPrefix === "webkitTransform" ? "-webkit-transform" : "transform";
    var userSelectPrefix = "webkitUserSelect" in testElement.style ? "webkitUserSelect" : "userSelect";

    testElement.style[transformPrefix] = 'translateZ(0)';
    var hwLayerMagic = testElement.style[transformPrefix] ? 'translateZ(0) ' : '';
    var hwTopLayerMagic = testElement.style[transformPrefix] ? 'translateZ(1px) ' : '';
    testElement = null;

    var globalInstances = 0;
    var attachedBodyHandlerHack = false;
    var nullHandler = function(){};

    function Slip(container, options) {
        if ('string' === typeof container) container = document.querySelector(container);
        if (!container || !container.addEventListener) throw new Error("Please specify DOM node to attach to");

        if (!this || this === window) return new Slip(container, options);

        this.options = options;

        // Functions used for as event handlers need usable `this` and must not change to be removable
        this.cancel = this.setState.bind(this, this.states.idle);
        this.onTouchStart = this.onTouchStart.bind(this);
        this.onTouchMove = this.onTouchMove.bind(this);
        this.onTouchEnd = this.onTouchEnd.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
        this.onSelection = this.onSelection.bind(this);

        this.setState(this.states.idle);
        this.attach(container);
    }

    function getTransform(node) {
        var transform = node.style[transformPrefix];
        if (transform) {
            return {
                value:transform,
                original:transform,
            };
        }

        if (window.getComputedStyle) {
            var style = window.getComputedStyle(node).getPropertyValue(transformProperty);
            if (style && style !== 'none') return {value:style, original:''};
        }
        return {value:'', original:''};
    }

    // All functions in states are going to be executed in context of Slip object
    Slip.prototype = {

        container: null,
        options: {},
        state: null,

        target: null, // the tapped/swiped/reordered node with height and backed up styles

        usingTouch: false, // there's no good way to detect touchscreen preference other than receiving a touch event (really, trust me).
        mouseHandlersAttached: false,

        startPosition: null, // x,y,time where first touch began
        latestPosition: null, // x,y,time where the finger is currently
        previousPosition: null, // x,y,time where the finger was ~100ms ago (for velocity calculation)

        canPreventScrolling: false,

        states: {
            idle: function() {
                this.target = null;
                this.usingTouch = false;
                this.removeMouseHandlers();

                return {
                    allowTextSelection: true,
                };
            },

            undecided: function() {
                this.target.height = this.target.node.offsetHeight;
                this.target.node.style[transitionPrefix] = '';

                if (!this.dispatch(this.target.originalTarget, 'beforewait')) {
                    this.setState(this.states.reorder);
                } else {
                    var holdTimer = setTimeout(function(){
                        var move = this.getAbsoluteMovement();
                        if (this.canPreventScrolling && move.x < 15 && move.y < 25) {
                            if (this.dispatch(this.target.originalTarget, 'beforereorder')) {
                                this.setState(this.states.reorder);
                            }
                        }
                    }.bind(this), 300);
                }

                return {
                    leaveState: function() {
                        clearTimeout(holdTimer);
                    },

                    onMove: function() {
                        var move = this.getAbsoluteMovement();

                        if (move.x > 20 && move.y < Math.max(100, this.target.height)) {
                            if (this.dispatch(this.target.originalTarget, 'beforeswipe')) {
                                this.setState(this.states.swipe);
                                return false;
                            } else {
                                this.setState(this.states.idle);
                            }
                        }
                        if (move.y > 20) {
                            this.setState(this.states.idle);
                        }

                        // Chrome likes sideways scrolling :(
                        if (move.x > move.y*1.2) return false;
                    },

                    onLeave: function() {
                        this.setState(this.states.idle);
                    },

                    onEnd: function() {
                        var allowDefault = !this.dispatch(this.target.originalTarget, 'tap');
                        this.setState(this.states.idle);
                        return allowDefault;
                    },
                };
            },

            swipe: function() {
                var swipeSuccess = false;
                var container = this.container;

                container.className += ' slip-swiping-container';
                function removeClass() {
                    container.className = container.className.replace(/(?:^| )slip-swiping-container/,'');
                }

                this.target.height = this.target.node.offsetHeight;

                return {
                    leaveState: function() {
                        if (swipeSuccess) {
                            this.animateSwipe(function(target){
                                target.node.style[transformPrefix] = target.baseTransform.original;
                                target.node.style[transitionPrefix] = '';
                                if (this.dispatch(target.node, 'afterswipe')) {
                                    removeClass();
                                    return true;
                                } else {
                                    this.animateToZero(undefined, target);
                                }
                            }.bind(this));
                        } else {
                            this.animateToZero(removeClass);
                        }
                    },

                    onMove: function() {
                        var move = this.getTotalMovement();

                        if (Math.abs(move.y) < this.target.height+20) {
                            this.target.node.style[transformPrefix] = 'translate(' + move.x + 'px,0) ' + hwLayerMagic + this.target.baseTransform.value;
                            return false;
                        } else {
                            this.setState(this.states.idle);
                        }
                    },

                    onLeave: function() {
                        this.state.onEnd.call(this);
                    },

                    onEnd: function() {
                        var dx = this.latestPosition.x - this.previousPosition.x;
                        var dy = this.latestPosition.y - this.previousPosition.y;
                        var velocity = Math.sqrt(dx*dx + dy*dy) / (this.latestPosition.time - this.previousPosition.time + 1);

                        var move = this.getAbsoluteMovement();
                        var swiped = velocity > 0.6 && move.time > 110;

                        if (swiped) {
                            if (this.dispatch(this.target.node, 'swipe')) {
                                swipeSuccess = true; // can't animate here, leaveState overrides anim
                            }
                        }
                        this.setState(this.states.idle);
                        return !swiped;
                    },
                };
            },

            reorder: function() {
                this.target.height = this.target.node.offsetHeight;

                var mouseOutsideTimer;
                var zero = this.target.node.offsetTop + this.target.height/2;
                var otherNodes = []
                var nodes = this.container.childNodes;
                for(var i=0; i < nodes.length; i++) {
                    if (nodes[i].nodeType != 1 || nodes[i] === this.target.node) continue;
                    var t = nodes[i].offsetTop;
                    nodes[i].style[transitionPrefix] = transformProperty + ' 0.2s ease-in-out';
                    otherNodes.push({
                        node: nodes[i],
                        baseTransform: getTransform(nodes[i]),
                        pos: t + (t < zero ? nodes[i].offsetHeight : 0) - zero,
                    });
                }

                this.target.node.className += ' slip-reordering';
                this.target.node.style.zIndex = '99999';
                this.target.node.style[userSelectPrefix] = 'none';
                if (compositorDoesNotOrderLayers) {
                    // Chrome's compositor doesn't sort 2D layers
                    this.container.style.webkitTransformStyle = 'preserve-3d';
                }

                function setPosition() {
                    if (mouseOutsideTimer) {
                        // don't care where the mouse is as long as it moves
                        clearTimeout(mouseOutsideTimer); mouseOutsideTimer = null;
                    }

                    var move = this.getTotalMovement();
                    this.target.node.style[transformPrefix] = 'translate(0,' + move.y + 'px) ' + hwTopLayerMagic + this.target.baseTransform.value;

                    var height = this.target.height;
                    otherNodes.forEach(function(o){
                        var off = 0;
                        if (o.pos < 0 && move.y < 0 && o.pos > move.y) {
                            off = height;
                        }
                        else if (o.pos > 0 && move.y > 0 && o.pos < move.y) {
                            off = -height;
                        }
                        // FIXME: should change accelerated/non-accelerated state lazily
                        o.node.style[transformPrefix] = off ? 'translate(0,'+off+'px) ' + hwLayerMagic + o.baseTransform.value : o.baseTransform.original;
                    });
                    return false;
                }

                setPosition.call(this);

                return {
                    leaveState: function() {
                        if (mouseOutsideTimer) clearTimeout(mouseOutsideTimer);

                        if (compositorDoesNotOrderLayers) {
                            this.container.style.webkitTransformStyle = '';
                        }

                        this.target.node.className = this.target.node.className.replace(/(?:^| )slip-reordering/,'');
                        this.target.node.style[userSelectPrefix] = '';

                        this.animateToZero(function(target){
                            target.node.style.zIndex = '';
                        });
                        otherNodes.forEach(function(o){
                            o.node.style[transformPrefix] = o.baseTransform.original;
                            o.node.style[transitionPrefix] = ''; // FIXME: animate to new position
                        });
                    },

                    onMove: setPosition,

                    onLeave: function() {
                        // don't let element get stuck if mouse left the window
                        // but don't cancel immediately as it'd be annoying near window edges
                        if (mouseOutsideTimer) clearTimeout(mouseOutsideTimer);
                        mouseOutsideTimer = setTimeout(function(){
                            mouseOutsideTimer = null;
                            this.cancel();
                        }.bind(this), 700);
                    },

                    onEnd: function() {
                        var move = this.getTotalMovement();
                        if (move.y < 0) {
                            for(var i=0; i < otherNodes.length; i++) {
                                if (otherNodes[i].pos > move.y) {
                                    this.dispatch(this.target.node, 'reorder', {spliceIndex:i, insertBefore:otherNodes[i].node});
                                    break;
                                }
                            }
                        } else {
                            for(var i=otherNodes.length-1; i >= 0; i--) {
                                if (otherNodes[i].pos < move.y) {
                                    this.dispatch(this.target.node, 'reorder', {spliceIndex:i+1, insertBefore:otherNodes[i+1] ? otherNodes[i+1].node : null});
                                    break;
                                }
                            }
                        }
                        this.setState(this.states.idle);
                        return false;
                    },
                };
            },
        },

        attach: function(container) {
            globalInstances++;
            if (this.container) this.detach();

            // In some cases taps on list elements send *only* click events and no touch events. Spotted only in Chrome 32+
            // Having event listener on body seems to solve the issue (although AFAIK may disable smooth scrolling as a side-effect)
            if (!attachedBodyHandlerHack && needsBodyHandlerHack) {
                attachedBodyHandlerHack = true;
                document.body.addEventListener('touchstart', nullHandler, false);
            }

            this.container = container;
            this.otherNodes = [];

            // selection on iOS interferes with reordering
            document.addEventListener("selectionchange", this.onSelection, false);

            // cancel is called e.g. when iOS detects multitasking gesture
            this.container.addEventListener('touchcancel', this.cancel, false);
            this.container.addEventListener('touchstart', this.onTouchStart, false);
            this.container.addEventListener('touchmove', this.onTouchMove, false);
            this.container.addEventListener('touchend', this.onTouchEnd, false);
            this.container.addEventListener('mousedown', this.onMouseDown, false);
            // mousemove and mouseup are attached dynamically
        },

        detach: function() {
            this.cancel();

            this.container.removeEventListener('mousedown', this.onMouseDown, false);
            this.container.removeEventListener('touchend', this.onTouchEnd, false);
            this.container.removeEventListener('touchmove', this.onTouchMove, false);
            this.container.removeEventListener('touchstart', this.onTouchStart, false);
            this.container.removeEventListener('touchcancel', this.cancel, false);

            document.removeEventListener("selectionchange", this.onSelection, false);

            globalInstances--;
            if (!globalInstances && attachedBodyHandlerHack) {
                attachedBodyHandlerHack = false;
                document.body.removeEventListener('touchstart', nullHandler, false);
            }
        },

        setState: function(newStateCtor){
            if (this.state) {
                if (this.state.ctor === newStateCtor) return;
                if (this.state.leaveState) this.state.leaveState.call(this);
            }

            // Must be re-entrant in case ctor changes state
            var prevState = this.state;
            var nextState = newStateCtor.call(this);
            if (this.state === prevState) {
                nextState.ctor = newStateCtor;
                this.state = nextState;
            }
        },

        findTargetNode: function(targetNode) {
            while(targetNode && targetNode.parentNode !== this.container) {
                targetNode = targetNode.parentNode;
            }
            return targetNode;
        },

        onSelection: function(e) {
            var isRelated = e.target === document || this.findTargetNode(e);
            if (!isRelated) return;

            if (e.cancelable || e.defaultPrevented) {
                if (!this.state.allowTextSelection) {
                    e.preventDefault();
                }
            } else {
                // iOS doesn't allow selection to be prevented
                this.setState(this.states.idle);
            }
        },

        addMouseHandlers: function() {
            // unlike touch events, mousemove/up is not conveniently fired on the same element,
            // but I don't need to listen to unrelated events all the time
            if (!this.mouseHandlersAttached) {
                this.mouseHandlersAttached = true;
                document.documentElement.addEventListener('mouseleave', this.onMouseLeave, false);
                window.addEventListener('mousemove', this.onMouseMove, true);
                window.addEventListener('mouseup', this.onMouseUp, true);
                window.addEventListener('blur', this.cancel, false);
            }
        },

        removeMouseHandlers: function() {
            if (this.mouseHandlersAttached) {
                this.mouseHandlersAttached = false;
                document.documentElement.removeEventListener('mouseleave', this.onMouseLeave, false);
                window.removeEventListener('mousemove', this.onMouseMove, true);
                window.removeEventListener('mouseup', this.onMouseUp, true);
                window.removeEventListener('blur', this.cancel, false);
            }
        },

        onMouseLeave: function(e) {
            if (e.target === document.documentElement || e.relatedTarget === document.documentElement) {
                if (this.state.onLeave) {
                    this.state.onLeave.call(this);
                }
            }
        },

        onMouseDown: function(e) {
            if (this.usingTouch || e.button != 0 || !this.setTarget(e)) return;

            this.addMouseHandlers(); // mouseup, etc.

            this.canPreventScrolling = true; // or rather it doesn't apply to mouse

            this.startAtPosition({
                x: e.clientX,
                y: e.clientY,
                time: e.timeStamp,
            });
        },

        onTouchStart: function(e) {
            this.usingTouch = true;
            this.canPreventScrolling = true;

            // This implementation cares only about single touch
            if (e.touches.length > 1) {
                this.setState(this.states.idle);
                return;
            }

            if (!this.setTarget(e)) return;

            this.startAtPosition({
                x: e.touches[0].clientX,
                y: e.touches[0].clientY - window.scrollY,
                time: e.timeStamp,
            });
        },

        setTarget: function(e) {
            var targetNode = this.findTargetNode(e.target);
            if (!targetNode) {
                this.setState(this.states.idle);
                return false;
            }

            this.target = {
                originalTarget: e.target,
                node: targetNode,
                baseTransform: getTransform(targetNode),
            };
            return true;
        },

        startAtPosition: function(pos) {
            this.startPosition = this.previousPosition = this.latestPosition = pos;
            this.setState(this.states.undecided);
        },

        updatePosition: function(e, pos) {
            this.latestPosition = pos;

            if (this.state.onMove) {
                if (this.state.onMove.call(this) === false) {
                    e.preventDefault();
                }
            }

            // sample latestPosition 100ms for velocity
            if (this.latestPosition.time - this.previousPosition.time > 100) {
                this.previousPosition = this.latestPosition;
            }
        },

        onMouseMove: function(e) {
            this.updatePosition(e, {
                x: e.clientX,
                y: e.clientY,
                time: e.timeStamp,
            });
        },

        onTouchMove: function(e) {
            this.updatePosition(e, {
                x: e.touches[0].clientX,
                y: e.touches[0].clientY - window.scrollY,
                time: e.timeStamp,
            });

            // In Apple's touch model only the first move event after touchstart can prevent scrolling (and event.cancelable is broken)
            this.canPreventScrolling = false;
        },

        onMouseUp: function(e) {
            if (this.state.onEnd && false === this.state.onEnd.call(this)) {
                e.preventDefault();
            }
        },

        onTouchEnd: function(e) {
            if (e.touches.length > 1) {
                this.cancel();
            } else if (this.state.onEnd && false === this.state.onEnd.call(this)) {
                e.preventDefault();
            }
        },

        getTotalMovement: function() {
            return {
                x:this.latestPosition.x - this.startPosition.x,
                y:this.latestPosition.y - this.startPosition.y,
            };
        },

        getAbsoluteMovement: function() {
            return {
                x: Math.abs(this.latestPosition.x - this.startPosition.x),
                y: Math.abs(this.latestPosition.y - this.startPosition.y),
                time:this.latestPosition.time - this.startPosition.time,
            };
        },

        dispatch: function(targetNode, eventName, detail) {
            var event = document.createEvent('CustomEvent');
            if (event && event.initCustomEvent) {
                event.initCustomEvent('slip:' + eventName, true, true, detail);
            } else {
                event = document.createEvent('Event');
                event.initEvent('slip:' + eventName, true, true);
                event.detail = detail;
            }
            return targetNode.dispatchEvent(event);
        },

        getSiblings: function(target) {
            var siblings = [];
            var tmp = target.node.nextSibling;
            while(tmp) {
                if (tmp.nodeType == 1) siblings.push({
                    node: tmp,
                    baseTransform: getTransform(tmp),
                });
                tmp = tmp.nextSibling;
            }
            return siblings;
        },

        animateToZero: function(callback, target) {
            // save, because this.target/container could change during animation
            target = target || this.target;

            target.node.style[transitionPrefix] = transformProperty + ' 0.1s ease-out';
            target.node.style[transformPrefix] = 'translate(0,0) ' + hwLayerMagic + target.baseTransform.value;
            setTimeout(function(){
                target.node.style[transitionPrefix] = '';
                target.node.style[transformPrefix] = target.baseTransform.original;
                if (callback) callback.call(this, target);
            }.bind(this), 101);
        },

        animateSwipe: function(callback) {
            var target = this.target;
            var siblings = this.getSiblings(target);
            var emptySpaceTransform = 'translate(0,' + this.target.height + 'px) ' + hwLayerMagic + ' ';

            // FIXME: animate with real velocity
            target.node.style[transitionPrefix] = 'all 0.1s linear';
            target.node.style[transformPrefix] = ' translate(' + (this.getTotalMovement().x > 0 ? '' : '-') + '100%,0) ' + hwLayerMagic + target.baseTransform.value;

            setTimeout(function(){
                if (callback.call(this, target)) {
                    siblings.forEach(function(o){
                        o.node.style[transitionPrefix] = '';
                        o.node.style[transformPrefix] = emptySpaceTransform + o.baseTransform.value;
                    });
                    setTimeout(function(){
                        siblings.forEach(function(o){
                            o.node.style[transitionPrefix] = transformProperty + ' 0.1s ease-in-out';
                            o.node.style[transformPrefix] = 'translate(0,0) ' + hwLayerMagic + o.baseTransform.value;
                        });
                        setTimeout(function(){
                            siblings.forEach(function(o){
                                o.node.style[transitionPrefix] = '';
                                o.node.style[transformPrefix] = o.baseTransform.original;
                            });
                        },101);
                    }, 1);
                }
            }.bind(this), 101);
        },
    };

    // AMD
    if ('function' === typeof define && define.amd) {
        define(function(){
            return Slip;
        });
    }
    return Slip;
})();
/*
 * Snap.js
 *
 * Copyright 2013, Jacob Kelley - http://jakiestfu.com/
 * Released under the MIT Licence
 * http://opensource.org/licenses/MIT
 *
 * Github:  http://github.com/jakiestfu/Snap.js/
 * Version: 1.9.2
 */
 (function(c,b){var a=a||function(k){var f={element:null,dragger:null,disable:"none",addBodyClasses:true,hyperextensible:true,resistance:0.5,flickThreshold:50,transitionSpeed:0.3,easing:"ease",maxPosition:266,minPosition:-266,tapToClose:true,touchToDrag:true,slideIntent:40,minDragDistance:5},e={simpleStates:{opening:null,towards:null,hyperExtending:null,halfway:null,flick:null,translation:{absolute:0,relative:0,sinceDirectionChange:0,percentage:0}}},h={},d={hasTouch:(b.ontouchstart===null),eventType:function(m){var l={down:(d.hasTouch?"touchstart":"mousedown"),move:(d.hasTouch?"touchmove":"mousemove"),up:(d.hasTouch?"touchend":"mouseup"),out:(d.hasTouch?"touchcancel":"mouseout")};return l[m]},page:function(l,m){return(d.hasTouch&&m.touches.length&&m.touches[0])?m.touches[0]["page"+l]:m["page"+l]},klass:{has:function(m,l){return(m.className).indexOf(l)!==-1},add:function(m,l){if(!d.klass.has(m,l)&&f.addBodyClasses){m.className+=" "+l}},remove:function(m,l){if(f.addBodyClasses){m.className=(m.className).replace(l,"").replace(/^\s+|\s+$/g,"")}}},dispatchEvent:function(l){if(typeof h[l]==="function"){return h[l].call()}},vendor:function(){var m=b.createElement("div"),n="webkit Moz O ms".split(" "),l;for(l in n){if(typeof m.style[n[l]+"Transition"]!=="undefined"){return n[l]}}},transitionCallback:function(){return(e.vendor==="Moz"||e.vendor==="ms")?"transitionend":e.vendor+"TransitionEnd"},canTransform:function(){return typeof f.element.style[e.vendor+"Transform"]!=="undefined"},deepExtend:function(l,n){var m;for(m in n){if(n[m]&&n[m].constructor&&n[m].constructor===Object){l[m]=l[m]||{};d.deepExtend(l[m],n[m])}else{l[m]=n[m]}}return l},angleOfDrag:function(l,o){var n,m;m=Math.atan2(-(e.startDragY-o),(e.startDragX-l));if(m<0){m+=2*Math.PI}n=Math.floor(m*(180/Math.PI)-180);if(n<0&&n>-180){n=360-Math.abs(n)}return Math.abs(n)},events:{addEvent:function g(m,l,n){if(m.addEventListener){return m.addEventListener(l,n,false)}else{if(m.attachEvent){return m.attachEvent("on"+l,n)}}},removeEvent:function g(m,l,n){if(m.addEventListener){return m.removeEventListener(l,n,false)}else{if(m.attachEvent){return m.detachEvent("on"+l,n)}}},prevent:function(l){if(l.preventDefault){l.preventDefault()}else{l.returnValue=false}}},parentUntil:function(n,l){var m=typeof l==="string";while(n.parentNode){if(m&&n.getAttribute&&n.getAttribute(l)){return n}else{if(!m&&n===l){return n}}n=n.parentNode}return null}},i={translate:{get:{matrix:function(n){if(!d.canTransform()){return parseInt(f.element.style.left,10)}else{var m=c.getComputedStyle(f.element)[e.vendor+"Transform"].match(/\((.*)\)/),l=8;if(m){m=m[1].split(",");if(m.length===16){n+=l}return parseInt(m[n],10)}return 0}}},easeCallback:function(){f.element.style[e.vendor+"Transition"]="";e.translation=i.translate.get.matrix(4);e.easing=false;clearInterval(e.animatingInterval);if(e.easingTo===0){d.klass.remove(b.body,"snapjs-right");d.klass.remove(b.body,"snapjs-left")}d.dispatchEvent("animated");d.events.removeEvent(f.element,d.transitionCallback(),i.translate.easeCallback)},easeTo:function(l){if(!d.canTransform()){e.translation=l;i.translate.x(l)}else{e.easing=true;e.easingTo=l;f.element.style[e.vendor+"Transition"]="all "+f.transitionSpeed+"s "+f.easing;e.animatingInterval=setInterval(function(){d.dispatchEvent("animating")},1);d.events.addEvent(f.element,d.transitionCallback(),i.translate.easeCallback);i.translate.x(l)}if(l===0){f.element.style[e.vendor+"Transform"]=""}},x:function(m){if((f.disable==="left"&&m>0)||(f.disable==="right"&&m<0)){return}if(!f.hyperextensible){if(m===f.maxPosition||m>f.maxPosition){m=f.maxPosition}else{if(m===f.minPosition||m<f.minPosition){m=f.minPosition}}}m=parseInt(m,10);if(isNaN(m)){m=0}if(d.canTransform()){var l="translate3d("+m+"px, 0,0)";f.element.style[e.vendor+"Transform"]=l}else{f.element.style.width=(c.innerWidth||b.documentElement.clientWidth)+"px";f.element.style.left=m+"px";f.element.style.right=""}}},drag:{listen:function(){e.translation=0;e.easing=false;d.events.addEvent(f.element,d.eventType("down"),i.drag.startDrag);d.events.addEvent(f.element,d.eventType("move"),i.drag.dragging);d.events.addEvent(f.element,d.eventType("up"),i.drag.endDrag)},stopListening:function(){d.events.removeEvent(f.element,d.eventType("down"),i.drag.startDrag);d.events.removeEvent(f.element,d.eventType("move"),i.drag.dragging);d.events.removeEvent(f.element,d.eventType("up"),i.drag.endDrag)},startDrag:function(n){var m=n.target?n.target:n.srcElement,l=d.parentUntil(m,"data-snap-ignore");if(l){d.dispatchEvent("ignore");return}if(f.dragger){var o=d.parentUntil(m,f.dragger);if(!o&&(e.translation!==f.minPosition&&e.translation!==f.maxPosition)){return}}d.dispatchEvent("start");f.element.style[e.vendor+"Transition"]="";e.isDragging=true;e.hasIntent=null;e.intentChecked=false;e.startDragX=d.page("X",n);e.startDragY=d.page("Y",n);e.dragWatchers={current:0,last:0,hold:0,state:""};e.simpleStates={opening:null,towards:null,hyperExtending:null,halfway:null,flick:null,translation:{absolute:0,relative:0,sinceDirectionChange:0,percentage:0}}},dragging:function(s){if(e.isDragging&&f.touchToDrag){var v=d.page("X",s),u=d.page("Y",s),t=e.translation,o=i.translate.get.matrix(4),n=v-e.startDragX,p=o>0,q=n,w;if((e.intentChecked&&!e.hasIntent)){return}if(f.addBodyClasses){if((o)>0){d.klass.add(b.body,"snapjs-left");d.klass.remove(b.body,"snapjs-right")}else{if((o)<0){d.klass.add(b.body,"snapjs-right");d.klass.remove(b.body,"snapjs-left")}}}if(e.hasIntent===false||e.hasIntent===null){var m=d.angleOfDrag(v,u),l=(m>=0&&m<=f.slideIntent)||(m<=360&&m>(360-f.slideIntent)),r=(m>=180&&m<=(180+f.slideIntent))||(m<=180&&m>=(180-f.slideIntent));if(!r&&!l){e.hasIntent=false}else{e.hasIntent=true}e.intentChecked=true}if((f.minDragDistance>=Math.abs(v-e.startDragX))||(e.hasIntent===false)){return}d.events.prevent(s);d.dispatchEvent("drag");e.dragWatchers.current=v;if(e.dragWatchers.last>v){if(e.dragWatchers.state!=="left"){e.dragWatchers.state="left";e.dragWatchers.hold=v}e.dragWatchers.last=v}else{if(e.dragWatchers.last<v){if(e.dragWatchers.state!=="right"){e.dragWatchers.state="right";e.dragWatchers.hold=v}e.dragWatchers.last=v}}if(p){if(f.maxPosition<o){w=(o-f.maxPosition)*f.resistance;q=n-w}e.simpleStates={opening:"left",towards:e.dragWatchers.state,hyperExtending:f.maxPosition<o,halfway:o>(f.maxPosition/2),flick:Math.abs(e.dragWatchers.current-e.dragWatchers.hold)>f.flickThreshold,translation:{absolute:o,relative:n,sinceDirectionChange:(e.dragWatchers.current-e.dragWatchers.hold),percentage:(o/f.maxPosition)*100}}}else{if(f.minPosition>o){w=(o-f.minPosition)*f.resistance;q=n-w}e.simpleStates={opening:"right",towards:e.dragWatchers.state,hyperExtending:f.minPosition>o,halfway:o<(f.minPosition/2),flick:Math.abs(e.dragWatchers.current-e.dragWatchers.hold)>f.flickThreshold,translation:{absolute:o,relative:n,sinceDirectionChange:(e.dragWatchers.current-e.dragWatchers.hold),percentage:(o/f.minPosition)*100}}}i.translate.x(q+t)}},endDrag:function(m){if(e.isDragging){d.dispatchEvent("end");var l=i.translate.get.matrix(4);if(e.dragWatchers.current===0&&l!==0&&f.tapToClose){d.dispatchEvent("close");d.events.prevent(m);i.translate.easeTo(0);e.isDragging=false;e.startDragX=0;return}if(e.simpleStates.opening==="left"){if((e.simpleStates.halfway||e.simpleStates.hyperExtending||e.simpleStates.flick)){if(e.simpleStates.flick&&e.simpleStates.towards==="left"){i.translate.easeTo(0)}else{if((e.simpleStates.flick&&e.simpleStates.towards==="right")||(e.simpleStates.halfway||e.simpleStates.hyperExtending)){i.translate.easeTo(f.maxPosition)}}}else{i.translate.easeTo(0)}}else{if(e.simpleStates.opening==="right"){if((e.simpleStates.halfway||e.simpleStates.hyperExtending||e.simpleStates.flick)){if(e.simpleStates.flick&&e.simpleStates.towards==="right"){i.translate.easeTo(0)}else{if((e.simpleStates.flick&&e.simpleStates.towards==="left")||(e.simpleStates.halfway||e.simpleStates.hyperExtending)){i.translate.easeTo(f.minPosition)}}}else{i.translate.easeTo(0)}}}e.isDragging=false;e.startDragX=d.page("X",m)}}}},j=function(l){if(l.element){d.deepExtend(f,l);e.vendor=d.vendor();i.drag.listen()}};this.open=function(l){d.dispatchEvent("open");d.klass.remove(b.body,"snapjs-expand-left");d.klass.remove(b.body,"snapjs-expand-right");if(l==="left"){e.simpleStates.opening="left";e.simpleStates.towards="right";d.klass.add(b.body,"snapjs-left");d.klass.remove(b.body,"snapjs-right");i.translate.easeTo(f.maxPosition)}else{if(l==="right"){e.simpleStates.opening="right";e.simpleStates.towards="left";d.klass.remove(b.body,"snapjs-left");d.klass.add(b.body,"snapjs-right");i.translate.easeTo(f.minPosition)}}};this.close=function(){d.dispatchEvent("close");i.translate.easeTo(0)};this.expand=function(l){var m=c.innerWidth||b.documentElement.clientWidth;if(l==="left"){d.dispatchEvent("expandLeft");d.klass.add(b.body,"snapjs-expand-left");d.klass.remove(b.body,"snapjs-expand-right")}else{d.dispatchEvent("expandRight");d.klass.add(b.body,"snapjs-expand-right");d.klass.remove(b.body,"snapjs-expand-left");m*=-1}i.translate.easeTo(m)};this.on=function(l,m){h[l]=m;return this};this.off=function(l){if(h[l]){h[l]=false}};this.enable=function(){d.dispatchEvent("enable");i.drag.listen()};this.disable=function(){d.dispatchEvent("disable");i.drag.stopListening()};this.settings=function(l){d.deepExtend(f,l)};this.state=function(){var l,m=i.translate.get.matrix(4);if(m===f.maxPosition){l="left"}else{if(m===f.minPosition){l="right"}else{l="closed"}}return{state:l,info:e.simpleStates}};j(k)};if((typeof module!=="undefined")&&module.exports){module.exports=a}if(typeof ender==="undefined"){this.Snap=a}if((typeof define==="function")&&define.amd){define("snap",[],function(){return a})}}).call(this,window,document);(function(c){c.fn.stupidtable=function(b){return this.each(function(){var a=c(this);b=b||{};b=c.extend({},c.fn.stupidtable.default_sort_fns,b);a.on("click.stupidtable","th",function(){var d=c(this),f=0,g=c.fn.stupidtable.dir;a.find("th").slice(0,d.index()).each(function(){var a=c(this).attr("colspan")||1;f+=parseInt(a,10)});var e=d.data("sort-default")||g.ASC;d.data("sort-dir")&&(e=d.data("sort-dir")===g.ASC?g.DESC:g.ASC);var l=d.data("sort")||null;null!==l&&(a.trigger("beforetablesort",{column:f, direction:e}),a.css("display"),setTimeout(function(){var h=[],m=b[l],k=a.children("tbody").children("tr");k.each(function(a,b){var d=c(b).children().eq(f),e=d.data("sort-value"),d="undefined"!==typeof e?e:d.text();h.push([d,b])});h.sort(function(a,b){return m(a[0],b[0])});e!=g.ASC&&h.reverse();k=c.map(h,function(a){return a[1]});a.children("tbody").append(k);a.find("th").data("sort-dir",null).removeClass("sorting-desc sorting-asc");d.data("sort-dir",e).addClass("sorting-"+e);a.trigger("aftertablesort", {column:f,direction:e});a.css("display")},10))})})};c.fn.stupidtable.dir={ASC:"asc",DESC:"desc"};c.fn.stupidtable.default_sort_fns={"int":function(b,a){return parseInt(b,10)-parseInt(a,10)},"float":function(b,a){return parseFloat(b)-parseFloat(a)},string:function(b,a){return b<a?-1:b>a?1:0},"string-ins":function(b,a){b=b.toLowerCase();a=a.toLowerCase();return b<a?-1:b>a?1:0}}})(jQuery);